# -*- coding: utf-8 -*-
#    phasexmlcreator provides utilites to generate XML input for phasetransfer calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlcreator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for making solvent xml entries from the infomration read in.

@author: mark
"""

import logging
import os
import phasexmlcreator.solventconcentrationreader as solventconcentrationreader
import phasexmlcreator.solventxmlcreator as solventxmlcreator

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)

DEFAULT_SOLVENT_INFORMATION_FILE = solventconcentrationreader.DEFAULT_SOLVENT_INFORMATION_FILE


def read_solvent_information_write_to_xml(
    solvent_filename, ssip_filename_list, output_filename
):
    """This reads in the solvent information, then outputs the solvents as xml
    in the given output file.
    """
    solvent_information_list = read_solvent_info_to_list(
        solvent_filename, ssip_filename_list
    )
    write_solvent_information_to_file(solvent_information_list, output_filename)


def write_solvent_information_to_file(solvent_information_list, filename):
    """This creates a SolventList element for the given information, and
    then writes it to file.
    """
    solvent_list_element = generate_solvent_list_element(solvent_information_list)
    write_solvent_list_element_to_file(solvent_list_element, filename)


def write_solvent_list_element_to_file(solvent_list_element, filename):
    """This writes the solvent list element to file.
    """
    solvent_list_etree = solventxmlcreator.create_element_tree(solvent_list_element)
    solventxmlcreator.write_element_tree_to_file(solvent_list_etree, filename)


def generate_solvent_list_element(solvent_information_list):
    """This generates a SolventList element.
    """
    return solventxmlcreator.generate_solvent_list_element(solvent_information_list)


def read_solvent_info_to_list(solvent_filename, ssip_filename_list):
    """This reads in the solvent information, and returns a dictionary, then
    outputs a list of solvent entries.
    """
    solvent_information_dict = read_and_merge_solvent_information(
        solvent_filename, ssip_filename_list
    )
    return generate_solvent_info_list(solvent_information_dict)


def generate_solvent_info_list(solvent_information_dict):
    """This creates a list from the dictionary, where the entries are ordered
    lexicographically by InChIKey.
    """
    sorted_inchikey_list = sorted(solvent_information_dict.keys())
    solvent_information_list = []
    for inchikey in sorted_inchikey_list:
        solvent_information_list.append(solvent_information_dict[inchikey])
    return solvent_information_list


def read_and_merge_solvent_information(solvent_filename, ssip_filename_list):
    """This reads and merges the solvent concentration information with the ssip lists.
    """
    return solventconcentrationreader.read_and_merge_solvent_information(
        solvent_filename, ssip_filename_list
    )
