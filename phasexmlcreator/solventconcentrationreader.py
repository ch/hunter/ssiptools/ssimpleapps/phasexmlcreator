# -*- coding: utf-8 -*-
#    phasexmlcreator provides utilites to generate XML input for phasetransfer calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlcreator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for reading in concentrations for the solvent names, and generating the
inchikeys of the molecules in pure solvents.

@author: mark
"""

import logging
import pathlib
import phasexmlcreator.ssipfilereader as ssipfilereader
import phasexmlcreator.puresolventconcentrationfilereader as puresolventconcfilereader

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)

DEFAULT_SOLVENT_INFORMATION_FILE = (pathlib.Path(__file__).parent / "solvent_information/solvent_information.csv").absolute().as_posix()


def read_and_merge_solvent_information(solvent_filename, ssip_filename_list, name_inchikey_map=None, tempdependence=False):
    """Read solvent concentrations and merges with SSIP descriptions.

    Parameters
    ----------
    solvent_filename : str
        Filename for file containing 
    ssip_filename_list : list of str
        Solvent files

    Returns
    -------
    dict
        Dictionary containing solvent information including SSIPMolecule element.

    """
    solvent_information_dict = read_solvent_information(solvent_filename,
                                                        tempdependence)
    if name_inchikey_map:
        for name, inchikey in name_inchikey_map.items():
            if inchikey not in solvent_information_dict.keys():
                entry = {
                "concentration": "10.0", #Default value to act as a placeholder for calculations.
                "Solvent": name,
                "StdInChIKey": inchikey,
            }
                solvent_information_dict[inchikey] = entry
    ssip_list_dict = read_ssip_lists(ssip_filename_list)
    return append_ssip_list_to_concentrations(solvent_information_dict, ssip_list_dict)


def append_ssip_list_to_concentrations(solvent_information_dict, ssip_list_dict):
    """This returns a dictionary, where only entries with bothh SSIPLists and
    concentrations are returned.
    """
    merged_dictionary = {}
    for inchikey in solvent_information_dict.keys():
        if inchikey in ssip_list_dict.keys():
            if isinstance(solvent_information_dict[inchikey].get("concentration"), str):
                ssip_info = {
                "inchikey": inchikey,
                "ssip_molecule": ssip_list_dict[inchikey]["ssip_molecule"],
                "concentration_value": float(
                    solvent_information_dict[inchikey]["concentration"]
                ),
                "mole_fraction": 1.0,
            }
            else:
                concentration_dict = {}
                LOGGER.debug("solvent_information_dict entry: %s", solvent_information_dict[inchikey])
                for temperature in solvent_information_dict[inchikey].keys():
                    LOGGER.debug("temperature value: %s", temperature)
                    if temperature != "Solvent":
                        concentration_dict[temperature] = solvent_information_dict[inchikey][temperature]["concentration"]
                ssip_info = {
                "inchikey": inchikey,
                "ssip_molecule": ssip_list_dict[inchikey]["ssip_molecule"],
                "concentration_value": concentration_dict,
                
                "mole_fraction": 1.0,
            }
            solvent_entry = {
                "solvent_id": solvent_information_dict[inchikey]["Solvent"],
                "solvent_name": solvent_information_dict[inchikey]["Solvent"],
                "ssip_info_list": [ssip_info],
            }
            merged_dictionary[inchikey] = solvent_entry
    return merged_dictionary


def read_solvent_information(filename, tempdependence=False):
    """This reads in the solvent information to a dictionary.
    """
    return puresolventconcfilereader.read_file_to_information_dict(filename, tempdependence)


def read_ssip_lists(filename_list):
    """This reads in the ssip lists from the given files.
    """
    return ssipfilereader.extract_ssip_information_from_files(filename_list)
