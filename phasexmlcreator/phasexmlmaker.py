# -*- coding: utf-8 -*-
#    phasexmlcreator provides utilites to generate XML input for phasetransfer calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlcreator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for making phase XML from reading in the information from SSIP files.

Initial implementation is a bootstrapping of that used for phase generation.

@author: mark
"""

import logging
import os
import phasexmlcreator.solventconcentrationreader as solventconcentrationreader
import phasexmlcreator.phasexmlcreator as phasexmlcreator

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)

DEFAULT_SOLVENT_INFORMATION_FILE = os.path.join(
    os.path.dirname(__file__), "solvent_information/solvent_information.csv"
)


def read_phase_information_write_to_xml(
    phase_filename, ssip_filename_list, temperature_info, output_filename, tempdependence=False
):
    """This reads in the solvent information, then outputs the solvents as xml
    in the given output file.
    """
    phase_information_list = read_phase_solvent_info_to_list(
        phase_filename, ssip_filename_list, tempdependence
    )
    write_phase_information_to_file(
        phase_information_list, temperature_info, output_filename
    )


def write_phase_information_to_file_diff_temp(phase_info_by_temp_list, filename):
    mixture_collection_element = phasexmlcreator.generate_mixture_collection_element_diff_temp(phase_info_by_temp_list)
    element_tree = phasexmlcreator.create_element_tree(mixture_collection_element)
    LOGGER.debug("created mixture element. Writing to file:")
    phasexmlcreator.write_element_tree_to_file(element_tree, filename)
    LOGGER.debug("Written to file.")

def write_phase_information_to_file(phase_information_list, temperature_info, filename):
    """This creates a MixtureCollection element for the given information, and
    then writes it to file.
    """
    mixture_collection_element = generate_mixture_collection_element(
        phase_information_list, temperature_info
    )
    element_tree = phasexmlcreator.create_element_tree(mixture_collection_element)
    LOGGER.debug("created mixture element. Writing to file:")
    phasexmlcreator.write_element_tree_to_file(element_tree, filename)
    LOGGER.debug("Written to file.")


def generate_mixture_collection_element(phase_information, temperature_info):
    """This generates a mixture collection element.
    """
    return phasexmlcreator.generate_mixture_collection_element(
        phase_information, temperature_info
    )


def read_phase_solvent_info_to_list(solvent_filename, ssip_filename_list, tempdependence=False):
    """This reads in the solvent information, and returns a dictionary, then
    outputs a list of phase entries.
    """
    phase_information_dict = read_and_merge_solvent_information(
        solvent_filename, ssip_filename_list, tempdependence,
    )
    return generate_phase_info_list(phase_information_dict)


def generate_phase_info_list(phase_information_dict):
    """This creates a list form the dictionary, where the entries are ordered
    lexicographically by InChIKey.
    """
    sorted_inchikey_list = sorted(phase_information_dict.keys())
    phase_information_list = []
    for inchikey in sorted_inchikey_list:
        phase_information_list.append(phase_information_dict[inchikey])
    return phase_information_list


def read_and_merge_solvent_information(solvent_filename, ssip_filename_list, tempdependence=False):
    """This reads and merges the solvent concentration information with the ssip lists.
    """
    return solventconcentrationreader.read_and_merge_solvent_information(
        solvent_filename, ssip_filename_list, tempdependence=tempdependence
        )
