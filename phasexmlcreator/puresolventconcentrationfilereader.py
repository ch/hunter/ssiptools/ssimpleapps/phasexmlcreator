# -*- coding: utf-8 -*-
#    phasexmlcreator provides utilites to generate XML input for phasetransfer calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlcreator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for reading in the solvent information, including the name, concentration
and the inchikey. This returns

@author: mark
"""

import logging


logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


def read_file_to_information_dict(filename, tempdependence=False):
    """This reads a file to a dictionary of entries.
    """
    with open(filename, "r") as file_in:
        file_lines = file_in.readlines()
        title_dict = read_titleline(file_lines[0])
        LOGGER.debug("title dict:")
        LOGGER.debug(title_dict)
        return read_entry_lines_to_dictionary(title_dict, file_lines[1:], tempdependence)


def read_entry_lines_to_dictionary(title_dict, line_list, tempdependence):
    """This reads the lines to a list of dictionary entries.
    """
    entry_information_dict = {}
    for line in line_list:
        LOGGER.debug("line:")
        LOGGER.debug(line)
        entry_dict = read_entry_line_to_dict(title_dict, line)
        if tempdependence:
            dict_key = "{:.4f}".format(float(entry_dict["temperature"]))
            if entry_dict["StdInChIKey"] in entry_information_dict.keys():
                
                entry_information_dict[entry_dict["StdInChIKey"]][dict_key] = entry_dict
            else:
                entry_information_dict[entry_dict["StdInChIKey"]] = {dict_key: entry_dict,
                                                                     "Solvent":entry_dict["Solvent"]
                                                                     }
        else:
            entry_information_dict[entry_dict["StdInChIKey"]] = entry_dict
    return entry_information_dict


def read_entry_line_to_dict(title_dict, line):
    """This reads the line to a dict.
    """
    entry_information = {}
    line_list = line.strip().split("\t")
    LOGGER.debug("line_list")
    LOGGER.debug(line_list)
    for index, title in title_dict.items():
        entry_information[title] = line_list[index]
    return entry_information


def read_titleline(line):
    """This reads the file titles.
    """
    title_list = line.strip().split("\t")
    title_index_dict = {}
    for i in range(len(title_list)):
        title_index_dict[i] = title_list[i]
    return title_index_dict
