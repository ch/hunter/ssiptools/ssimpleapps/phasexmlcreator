# -*- coding: utf-8 -*-
#    phasexmlcreator provides utilites to generate XML input for phasetransfer calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlcreator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for reading in SSIPMolecules

@author: mark
"""

import logging
import xmlvalidator.xmlvalidation as xmlvalidation

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)

PHASE_NAMESPACE_DICT = {
    "phase": "http://www-hunter.ch.cam.ac.uk/PhaseSchema",
    "ssip": "http://www-hunter.ch.cam.ac.uk/SSIP",
    "cml": "http://www.xml-cml.org/schema",
}


def extract_ssip_information_from_files(filename_list):
    """This reads in the files from all the files in the list, and gets the
    inchikey for the molecule and the ssip list.
    """
    ssip_information_dict = {}
    for filename in filename_list:
        ssip_information = extract_ssip_molecule_and_inchikey(filename)
        ssip_information_dict[ssip_information["inchikey"]] = ssip_information
    return ssip_information_dict


def extract_ssip_molecule_and_inchikey(filename):
    """This extracts the inchikey from the filename, and getthe ssip_list from
    the file.
    """
    inchikey = extract_inchikey_from_filename(filename)
    ssip_molecule = extract_ssip_molecule_from_file(filename)
    return {"inchikey": inchikey, "ssip_molecule": ssip_molecule}


def extract_ssip_molecule_from_file(filename):
    """This extracts an SSIPList from a file.
    """
    xml_etree = xmlvalidation.validate_and_read_xml_file(
        filename, xmlvalidation.SSIP_SCHEMA
    )
    return extract_ssip_molecule_in_etree(xml_etree)


def extract_ssip_molecule_in_etree(etree):
    """This extracts the ssip list in the file to a list.
    """
    ssip_list_elements = etree.xpath(
        "//ssip:SSIPMolecule", namespaces=PHASE_NAMESPACE_DICT
    )
    if len(ssip_list_elements) == 1:
        return ssip_list_elements[0]
    else:
        LOGGER.debug("len:")
        LOGGER.debug(len(ssip_list_elements))
        LOGGER.debug(ssip_list_elements)
        raise ValueError("Wrong number of SSIP lists.")


def extract_inchikey_from_filename(filename):
    """This assumes the standard naming convention.
    """
    return filename.split("/")[-1].replace("_ssip.xml", "")
