# -*- coding: utf-8 -*-
#    phasexmlcreator provides utilites to generate XML input for phasetransfer calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlcreator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for creating solvent entries.

@author: mark
"""

import logging
import copy
from lxml import etree
import phasexmlcreator.moleculexmlcreator as moleculexmlcreator
import xmlvalidator.xmlvalidation as xmlvalidation

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)

PHASE_NAMESPACE_DICT = {
    "phase": "http://www-hunter.ch.cam.ac.uk/PhaseSchema",
    "ssip": "http://www-hunter.ch.cam.ac.uk/SSIP",
    "cml": "http://www.xml-cml.org/schema",
}
PHASE = "{{{}}}".format(PHASE_NAMESPACE_DICT["phase"])


def write_element_tree_to_file(element_tree, filename):
    """Writes element tree to file.
    """
    xmlvalidation.validate_xml(element_tree, xmlvalidation.PHASE_SCHEMA)
    element_tree.write(
        filename, encoding="UTF-8", xml_declaration=True, pretty_print=True
    )


def create_element_tree(element):
    """Returns the element tree for the element.
    """
    return etree.ElementTree(element)


def generate_solvent_list_element(solvent_info_dict_list):
    """This generates a solvent list element containing the solvent elements
    created from the inputted information.
    """
    solvent_list_element = create_solvent_list_element()
    solvent_element_list = generate_solvent_element_list_from_information(
        solvent_info_dict_list
    )
    append_solvent_element_list_to_solvent_list_element(
        solvent_list_element, solvent_element_list
    )
    return solvent_list_element


def append_solvent_element_list_to_solvent_list_element(
    solvent_list_element, solvent_element_list
):
    """This appends the solvent elements in the list to the solvent list element.
    """
    solvents_element = etree.Element(PHASE + "Solvents", nsmap=PHASE_NAMESPACE_DICT)
    for solvent_element in solvent_element_list:
        append_child_element_to_parent_element(solvents_element, solvent_element)
    append_child_element_to_parent_element(solvent_list_element, solvents_element)


def create_solvent_list_element():
    """This creates a solvent list element.
    """
    return etree.Element(PHASE + "SolventList", nsmap=PHASE_NAMESPACE_DICT)


def generate_solvent_element_list_from_information(solvent_info_dict_list):
    """This creates a list of solvent elements, from the information supplied.
    """
    solvent_element_list = []
    for solvent_info_dict in solvent_info_dict_list:
        solvent_element_list.append(
            generate_solvent_element_from_value_dictionary(solvent_info_dict)
        )
    return solvent_element_list


def generate_solvent_element_from_value_dictionary(solvent_info_dict):
    """This generates a solvent entry from the given information.
    """
    return generate_solvent_element_from_ssip_lists(
        solvent_info_dict["solvent_id"],
        solvent_info_dict["solvent_name"],
        solvent_info_dict["ssip_info_list"],
    )


def generate_solvent_element_from_ssip_lists(solvent_id, solvent_name, ssip_info_list):
    """This generates the solvent information from the given information,
    first creating the molecule elements.
    """
    molecule_element_list = generate_molecule_element_list(ssip_info_list)
    return generate_solvent_element(solvent_id, solvent_name, molecule_element_list)


def generate_solvent_element(solvent_id, solvent_name, molecule_element_list):
    """This generates a solvent element from the given information.
    """
    solvent_element = create_solvent_element()
    append_solvent_id(solvent_element, solvent_id)
    append_solvent_name(solvent_element, solvent_name)
    solvent_element.set(PHASE + "units", "MOLAR")
    append_molecule_element_list_to_solvent(solvent_element, molecule_element_list)
    return solvent_element


def generate_molecule_element_list(ssip_info_list):
    """This generates a list of molecule elements from the given information
    about the ssip lists and concentrations.
    """
    molecule_element_list = []
    for ssip_info in ssip_info_list:
        mole_fraction = ssip_info.get("mole_fraction", None)
        molecule_element_list.append(
            moleculexmlcreator.generate_phase_molecule(
                ssip_info["inchikey"],
                ssip_info["inchikey"],
                ssip_info["ssip_molecule"],
                ssip_info["concentration_value"],
                mole_fraction=mole_fraction,
            )
        )
    return molecule_element_list


def append_molecule_element_list_to_solvent(solvent_element, molecule_element_list):
    """This appends the molecule elements in the list to the solvent as a Molecules element.
    """
    molecules_element = etree.Element(PHASE + "Molecules", nsmap=PHASE_NAMESPACE_DICT)
    for molecule_element in molecule_element_list:
        append_child_element_to_parent_element(molecules_element, molecule_element)
    append_child_element_to_parent_element(solvent_element, molecules_element)


def append_child_element_to_parent_element(parent_element, child_element):
    """This appends the child element to the parent.
    """
    parent_element.append(copy.deepcopy(child_element))


def append_solvent_name(solvent_element, solvent_name):
    """This appends the solvent name to the solvent element.
    """
    solvent_element.set(PHASE + "solventName", solvent_name)


def append_solvent_id(solvent_element, solvent_id):
    """This appends the solvent id to the solvent element.
    """
    solvent_element.set(PHASE + "solventID", solvent_id)


def create_solvent_element():
    """This creates an empty solvent element.
    """
    return etree.Element(PHASE + "Solvent", nsmap=PHASE_NAMESPACE_DICT)
