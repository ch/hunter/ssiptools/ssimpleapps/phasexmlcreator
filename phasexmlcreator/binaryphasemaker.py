# -*- coding: utf-8 -*-
#    phasexmlcreator provides utilites to generate XML input for phasetransfer calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlcreator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for creating a binary phase. This sets molefractions. Total
concentrations will require calculation.

@author: mark
"""

import logging
import copy
import phasexmlcreator.solventxmlmaker as solventxmlmaker
import phasexmlcreator.phasexmlmaker as phasexmlmaker
from phasexmlcreator.formattingconstants import MOLEFRACTION_FORMAT, MOLEFRACTION_ERROR

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


def write_mixture_collection_element_to_file(solvent_list_element, filename):
    """This writes the solvent list element to file.
    """
    solventxmlmaker.write_solvent_list_element_to_file(solvent_list_element, filename)


def read_and_generate_mixture_collection_element(
    solvent_filename, ssip_filename_list, mole_fraction_one_list, temperature_info
):
    """This reads in the solvent information for the pure solvents, and then
    generates the solute list element for the different volume fractions.
    """
    pure_solvent_information = read_and_merge_solvent_information(
        solvent_filename, ssip_filename_list
    )
    binary_phase_information = generate_binary_phase_information(
        pure_solvent_information[0], pure_solvent_information[1], mole_fraction_one_list
    )
    return generate_mixture_collection_element(
        binary_phase_information, temperature_info
    )


def generate_mixture_collection_element(phase_information_list, temperature_info):
    """This generates a MixtureContainer element.
    """
    return phasexmlmaker.generate_mixture_collection_element(
        phase_information_list, temperature_info
    )


def read_and_merge_solvent_information(solvent_filename, ssip_filename_list):
    """This reads in the information about the two solvents that make up the binary system.
    """
    return list(
        solventxmlmaker.read_and_merge_solvent_information(
            solvent_filename, ssip_filename_list
        ).values()
    )


def generate_binary_phase_information(
    solvent_one_info, solvent_two_info, mole_fraction_one_list
):
    """This generates solvent information entries for each volume fraction.
    """
    phase_information_list = []
    for mole_frac_one in mole_fraction_one_list:
        phase_entry = create_binary_phase_information(
            solvent_one_info, solvent_two_info, mole_frac_one
        )
        phase_information_list.append(phase_entry)
    return phase_information_list


def create_binary_phase_information(solvent_one_info, solvent_two_info, mole_frac_one):
    """This creates a binary solvent entry for the two solvents using the volume fraction.
    """
    solvent_one_ssip_info = solvent_one_info["ssip_info_list"][0]
    solvent_two_ssip_info = solvent_two_info["ssip_info_list"][0]
    binary_ssip_info_list = generate_solvent_ssip_information(
        copy.deepcopy(solvent_one_ssip_info),
        copy.deepcopy(solvent_two_ssip_info),
        mole_frac_one,
        1.0 - mole_frac_one,
    )
    solvent_id = (
        solvent_one_info["solvent_id"]
        + MOLEFRACTION_FORMAT.format(mole_frac_one)
        + solvent_two_info["solvent_id"]
    )
    solvent_name = (
        solvent_one_info["solvent_name"]
        + MOLEFRACTION_FORMAT.format(mole_frac_one)
        + solvent_two_info["solvent_name"]
    )
    solvent_entry = {
        "solvent_id": solvent_id,
        "solvent_name": solvent_name,
        "ssip_info_list": binary_ssip_info_list,
    }
    return solvent_entry


def generate_solvent_ssip_information(
    solvent_one_ssip_info, solvent_two_ssip_info, mole_frac_one, mole_frac_two
):
    """This generates the molecule entries for a solvent for the two solvents using the volume fraction.
    """
    solvent_one_ssip_info["mole_fraction"] = mole_frac_one
    solvent_two_ssip_info["mole_fraction"] = mole_frac_two
    binary_solvent_info = []
    if mole_frac_one > MOLEFRACTION_ERROR:
        binary_solvent_info.append(solvent_one_ssip_info)
    if mole_frac_two > MOLEFRACTION_ERROR:
        binary_solvent_info.append(solvent_two_ssip_info)
    return binary_solvent_info
