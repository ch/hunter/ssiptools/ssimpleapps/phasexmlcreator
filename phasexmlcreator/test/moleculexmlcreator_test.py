# -*- coding: utf-8 -*-
#    phasexmlcreator provides utilites to generate XML input for phasetransfer calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlcreator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Contains moleculexmlreader test case

@author: mark
"""

import logging
import unittest
from lxml import etree
import phasexmlcreator.moleculexmlcreator as moleculexmlcreator

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)


class MoleculeXMLCreatorTestCase(unittest.TestCase):
    """Test case for the moleculexmlcreator script.
    """

    def setUp(self):
        """Set up for tests
        """
        self.concentration_value = 55.35
        self.inchikey = "XLYOFNOQVPJJNP-UHFFFAOYSA-N"
        self.phase_molecule_element = moleculexmlcreator.create_phase_molecule_element()
        self.ssip_molecule = """<?xml-stylesheet type="text/xsl" href="http://www-hunter.ch.cam.ac.uk/xlst/ssip.xsl"?><ssip:SSIPMolecule xmlns:cml="http://www.xml-cml.org/schema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ssip:ssipSoftwareVersion="2.2.0-SNAPSHOT" ssip:parameterVersion="1.0.0" xsi:schemaLocation="http://www.xml-cml.org/schema http://www-hunter.ch.cam.ac.uk/schema/cmlschema.xsd http://www-hunter.ch.cam.ac.uk/SSIP http://www-hunter.ch.cam.ac.uk/schema/SSIP.xsd">
    <cml:molecule ssip:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" cml:id="XLYOFNOQVPJJNP-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="O" cml:id="a1" cml:x3="0.0569236794019" cml:y3="0.53497425302" cml:z3="-9.99862395488E-5"/>
            <cml:atom cml:elementType="H" cml:id="a2" cml:x3="-0.735721920018" cml:y3="-0.0219270863225" cml:z3="-1.029173866E-4"/>
            <cml:atom cml:elementType="H" cml:id="a3" cml:x3="0.785198240616" cml:y3="-0.103847166698" cml:z3="-1.97096373822E-4"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a3" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    <ssip:SurfaceInformation>
        <ssip:TotalSurfaceArea>37.48483410829331</ssip:TotalSurfaceArea>
        <ssip:NegativeSurfaceArea>18.664358683092974</ssip:NegativeSurfaceArea>
        <ssip:PositiveSurfaceArea>18.82047542519977</ssip:PositiveSurfaceArea>
        <ssip:ElectronDensityIsosurface>0.002</ssip:ElectronDensityIsosurface>
        <ssip:NumberOFMEPSPoints>2286</ssip:NumberOFMEPSPoints>
    </ssip:SurfaceInformation>
    <ssip:SSIPs>
        <ssip:SSIP ssip:value="2.9387503903833574" ssip:nearestAtomID="a3" cml:x3="1.509794550767402" cml:y3="-0.8784982637871289" cml:z3="-1.6986589692899999E-4"/>
        <ssip:SSIP ssip:value="2.9275582734233065" ssip:nearestAtomID="a2" cml:x3="-1.4603207114038939" cml:y3="-0.790973405157027" cml:z3="0.12362427220238399"/>
        <ssip:SSIP ssip:value="-6.263074063139137" ssip:nearestAtomID="a1" cml:x3="0.156188781751346" cml:y3="1.6969169177862917" cml:z3="1.172598045109361"/>
        <ssip:SSIP ssip:value="-6.607684490829715" ssip:nearestAtomID="a1" cml:x3="0.06855491260794999" cml:y3="1.9788577934582507" cml:z3="-0.77762067563301"/>
    </ssip:SSIPs>
</ssip:SSIPMolecule>"""
        self.ssip_molecule_etree = etree.XML(self.ssip_molecule)
        self.ssip_ssip_etree = etree.XML(
            '<ssip:SSIP xmlns:cml="http://www.xml-cml.org/schema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" ssip:value="2.9275582734233065" ssip:nearestAtomID="a2" cml:x3="-1.4603207114038939" cml:y3="-0.790973405157027" cml:z3="0.12362427220238399"/>'
        )
        self.maxDiff = None

    def tearDown(self):
        """clean up after tests.
        """
        del self.concentration_value
        del self.inchikey
        del self.phase_molecule_element
        del self.ssip_molecule
        del self.ssip_molecule_etree

    def test_generate_phase_molecule_with_id(self):
        """Test to see if expected molecule is generated.
        """
        expected_xml = """<phase:Molecule xmlns:phase="http://www-hunter.ch.cam.ac.uk/PhaseSchema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:cml="http://www.xml-cml.org/schema" phase:stdInChIKey="AAAAAAAAAAAAAA-AAAAAAAANZ-N" phase:moleculeID="molid"><cml:molecule ssip:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" cml:id="XLYOFNOQVPJJNP-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="O" cml:id="a1" cml:x3="0.0569236794019" cml:y3="0.53497425302" cml:z3="-9.99862395488E-5"/>
            <cml:atom cml:elementType="H" cml:id="a2" cml:x3="-0.735721920018" cml:y3="-0.0219270863225" cml:z3="-1.029173866E-4"/>
            <cml:atom cml:elementType="H" cml:id="a3" cml:x3="0.785198240616" cml:y3="-0.103847166698" cml:z3="-1.97096373822E-4"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a3" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    <ssip:SurfaceInformation>
        <ssip:TotalSurfaceArea>37.48483410829331</ssip:TotalSurfaceArea>
        <ssip:NegativeSurfaceArea>18.664358683092974</ssip:NegativeSurfaceArea>
        <ssip:PositiveSurfaceArea>18.82047542519977</ssip:PositiveSurfaceArea>
        <ssip:ElectronDensityIsosurface>0.002</ssip:ElectronDensityIsosurface>
        <ssip:NumberOFMEPSPoints>2286</ssip:NumberOFMEPSPoints>
    </ssip:SurfaceInformation>
    <phase:SSIPs phase:units="MOLAR"><phase:SSIP ssip:value="2.9387503903833574" ssip:nearestAtomID="a3" cml:x3="1.509794550767402" cml:y3="-0.8784982637871289" cml:z3="-1.6986589692899999E-4" phase:moleculeID="molid" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="2.9275582734233065" ssip:nearestAtomID="a2" cml:x3="-1.4603207114038939" cml:y3="-0.790973405157027" cml:z3="0.12362427220238399" phase:moleculeID="molid" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.263074063139137" ssip:nearestAtomID="a1" cml:x3="0.156188781751346" cml:y3="1.6969169177862917" cml:z3="1.172598045109361" phase:moleculeID="molid" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.607684490829715" ssip:nearestAtomID="a1" cml:x3="0.06855491260794999" cml:y3="1.9788577934582507" cml:z3="-0.77762067563301" phase:moleculeID="molid" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP></phase:SSIPs></phase:Molecule>
"""
        actual_xml_element = moleculexmlcreator.generate_phase_molecule(
            "molid", "molid", self.ssip_molecule_etree, self.concentration_value
        )
        self.assertMultiLineEqual(
            expected_xml, etree.tounicode(actual_xml_element, pretty_print=True)
        )

    def test_generate_phase_molecule(self):
        """Test to see if expected molecule is generated.
        """
        expected_xml = """<phase:Molecule xmlns:phase="http://www-hunter.ch.cam.ac.uk/PhaseSchema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:cml="http://www.xml-cml.org/schema" phase:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N"><cml:molecule ssip:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" cml:id="XLYOFNOQVPJJNP-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="O" cml:id="a1" cml:x3="0.0569236794019" cml:y3="0.53497425302" cml:z3="-9.99862395488E-5"/>
            <cml:atom cml:elementType="H" cml:id="a2" cml:x3="-0.735721920018" cml:y3="-0.0219270863225" cml:z3="-1.029173866E-4"/>
            <cml:atom cml:elementType="H" cml:id="a3" cml:x3="0.785198240616" cml:y3="-0.103847166698" cml:z3="-1.97096373822E-4"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a3" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    <ssip:SurfaceInformation>
        <ssip:TotalSurfaceArea>37.48483410829331</ssip:TotalSurfaceArea>
        <ssip:NegativeSurfaceArea>18.664358683092974</ssip:NegativeSurfaceArea>
        <ssip:PositiveSurfaceArea>18.82047542519977</ssip:PositiveSurfaceArea>
        <ssip:ElectronDensityIsosurface>0.002</ssip:ElectronDensityIsosurface>
        <ssip:NumberOFMEPSPoints>2286</ssip:NumberOFMEPSPoints>
    </ssip:SurfaceInformation>
    <phase:SSIPs phase:units="MOLAR"><phase:SSIP ssip:value="2.9387503903833574" ssip:nearestAtomID="a3" cml:x3="1.509794550767402" cml:y3="-0.8784982637871289" cml:z3="-1.6986589692899999E-4" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="2.9275582734233065" ssip:nearestAtomID="a2" cml:x3="-1.4603207114038939" cml:y3="-0.790973405157027" cml:z3="0.12362427220238399" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.263074063139137" ssip:nearestAtomID="a1" cml:x3="0.156188781751346" cml:y3="1.6969169177862917" cml:z3="1.172598045109361" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.607684490829715" ssip:nearestAtomID="a1" cml:x3="0.06855491260794999" cml:y3="1.9788577934582507" cml:z3="-0.77762067563301" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP></phase:SSIPs></phase:Molecule>
"""
        actual_xml_element = moleculexmlcreator.generate_phase_molecule(
            self.inchikey,
            self.inchikey,
            self.ssip_molecule_etree,
            self.concentration_value,
        )
        self.assertMultiLineEqual(
            expected_xml, etree.tounicode(actual_xml_element, pretty_print=True)
        )

    def test_extract_cml_molecule(self):
        """Test to see if expected cml is extracted.
        """
        expected_cml = """<cml:molecule xmlns:cml="http://www.xml-cml.org/schema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" ssip:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" cml:id="XLYOFNOQVPJJNP-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="O" cml:id="a1" cml:x3="0.0569236794019" cml:y3="0.53497425302" cml:z3="-9.99862395488E-5"/>
            <cml:atom cml:elementType="H" cml:id="a2" cml:x3="-0.735721920018" cml:y3="-0.0219270863225" cml:z3="-1.029173866E-4"/>
            <cml:atom cml:elementType="H" cml:id="a3" cml:x3="0.785198240616" cml:y3="-0.103847166698" cml:z3="-1.97096373822E-4"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a3" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    """
        actual_cml = moleculexmlcreator.extract_cml_molecule(self.ssip_molecule_etree)
        self.assertMultiLineEqual(expected_cml, etree.tounicode(actual_cml))

    def test_extract_surface_information(self):
        """Test to see if surface information is extracted.
        """
        expected_surf_inf = """<ssip:SurfaceInformation xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP">
        <ssip:TotalSurfaceArea>37.48483410829331</ssip:TotalSurfaceArea>
        <ssip:NegativeSurfaceArea>18.664358683092974</ssip:NegativeSurfaceArea>
        <ssip:PositiveSurfaceArea>18.82047542519977</ssip:PositiveSurfaceArea>
        <ssip:ElectronDensityIsosurface>0.002</ssip:ElectronDensityIsosurface>
        <ssip:NumberOFMEPSPoints>2286</ssip:NumberOFMEPSPoints>
    </ssip:SurfaceInformation>
    """
        actual_surf_inf = moleculexmlcreator.extract_surface_information(
            self.ssip_molecule_etree
        )
        self.assertMultiLineEqual(expected_surf_inf, etree.tounicode(actual_surf_inf))

    def test_extract_ssips(self):
        """Test to see if SSIPs are extracted.
        """
        expected_ssips = """<ssip:SSIPs xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:cml="http://www.xml-cml.org/schema">
        <ssip:SSIP ssip:value="2.9387503903833574" ssip:nearestAtomID="a3" cml:x3="1.509794550767402" cml:y3="-0.8784982637871289" cml:z3="-1.6986589692899999E-4"/>
        <ssip:SSIP ssip:value="2.9275582734233065" ssip:nearestAtomID="a2" cml:x3="-1.4603207114038939" cml:y3="-0.790973405157027" cml:z3="0.12362427220238399"/>
        <ssip:SSIP ssip:value="-6.263074063139137" ssip:nearestAtomID="a1" cml:x3="0.156188781751346" cml:y3="1.6969169177862917" cml:z3="1.172598045109361"/>
        <ssip:SSIP ssip:value="-6.607684490829715" ssip:nearestAtomID="a1" cml:x3="0.06855491260794999" cml:y3="1.9788577934582507" cml:z3="-0.77762067563301"/>
    </ssip:SSIPs>
"""
        actual_ssips = moleculexmlcreator.extract_ssips(self.ssip_molecule_etree)
        self.assertMultiLineEqual(expected_ssips, etree.tounicode(actual_ssips))

    def test_create_phase_ssips(self):
        """Test to see if expected SSIPs element is created.
        """
        expected_ssips = """<phase:SSIPs xmlns:phase="http://www-hunter.ch.cam.ac.uk/PhaseSchema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:cml="http://www.xml-cml.org/schema" phase:units="MOLAR">
  <phase:SSIP ssip:value="2.9387503903833574" ssip:nearestAtomID="a3" cml:x3="1.509794550767402" cml:y3="-0.8784982637871289" cml:z3="-1.6986589692899999E-4" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR">
    <phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration>
  </phase:SSIP>
  <phase:SSIP ssip:value="2.9275582734233065" ssip:nearestAtomID="a2" cml:x3="-1.4603207114038939" cml:y3="-0.790973405157027" cml:z3="0.12362427220238399" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR">
    <phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration>
  </phase:SSIP>
  <phase:SSIP ssip:value="-6.263074063139137" ssip:nearestAtomID="a1" cml:x3="0.156188781751346" cml:y3="1.6969169177862917" cml:z3="1.172598045109361" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR">
    <phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration>
  </phase:SSIP>
  <phase:SSIP ssip:value="-6.607684490829715" ssip:nearestAtomID="a1" cml:x3="0.06855491260794999" cml:y3="1.9788577934582507" cml:z3="-0.77762067563301" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR">
    <phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration>
  </phase:SSIP>
</phase:SSIPs>
"""
        ssip_ssips_etree = moleculexmlcreator.extract_ssips(self.ssip_molecule_etree)
        actual_ssips = moleculexmlcreator.create_phase_ssips(
            ssip_ssips_etree, self.concentration_value, self.inchikey
        )
        self.assertMultiLineEqual(
            expected_ssips, etree.tounicode(actual_ssips, pretty_print=True)
        )

    def test_create_phase_ssip_with_concentration(self):
        """Test to see if expected SSIP element is created with correct TotalConcentration.
        """
        expected_ssip = """<phase:SSIP xmlns:phase="http://www-hunter.ch.cam.ac.uk/PhaseSchema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:cml="http://www.xml-cml.org/schema" ssip:value="2.9275582734233065" ssip:nearestAtomID="a2" cml:x3="-1.4603207114038939" cml:y3="-0.790973405157027" cml:z3="0.12362427220238399" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR">
  <phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration>
</phase:SSIP>
"""
        actual_ssip = moleculexmlcreator.create_phase_ssip_with_concentration(
            self.ssip_ssip_etree, self.concentration_value, self.inchikey
        )
        self.assertMultiLineEqual(
            expected_ssip, etree.tounicode(actual_ssip, pretty_print=True)
        )

    def test_create_phase_ssip(self):
        """Test to see if expected SSIP is created
        """
        expected_ssip = """<phase:SSIP xmlns:phase="http://www-hunter.ch.cam.ac.uk/PhaseSchema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:cml="http://www.xml-cml.org/schema" ssip:value="2.9275582734233065" ssip:nearestAtomID="a2" cml:x3="-1.4603207114038939" cml:y3="-0.790973405157027" cml:z3="0.12362427220238399" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"/>"""
        actual_ssip = moleculexmlcreator.create_phase_ssip(
            self.ssip_ssip_etree, self.inchikey
        )
        self.assertEqual(expected_ssip, etree.tounicode(actual_ssip))

    def test_append_to_element(self):
        """Test to see if actual element matches the expected xml element.
        """
        expected_xml = """<phase:Molecule xmlns:phase="http://www-hunter.ch.cam.ac.uk/PhaseSchema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:cml="http://www.xml-cml.org/schema"><ssip:SurfaceInformation>
        <ssip:TotalSurfaceArea>37.48483410829331</ssip:TotalSurfaceArea>
        <ssip:NegativeSurfaceArea>18.664358683092974</ssip:NegativeSurfaceArea>
        <ssip:PositiveSurfaceArea>18.82047542519977</ssip:PositiveSurfaceArea>
        <ssip:ElectronDensityIsosurface>0.002</ssip:ElectronDensityIsosurface>
        <ssip:NumberOFMEPSPoints>2286</ssip:NumberOFMEPSPoints>
    </ssip:SurfaceInformation>
    </phase:Molecule>"""
        surface_information = moleculexmlcreator.extract_surface_information(
            self.ssip_molecule_etree
        )
        moleculexmlcreator.append_to_element(
            self.phase_molecule_element, surface_information
        )
        self.assertMultiLineEqual(
            expected_xml, etree.tounicode(self.phase_molecule_element)
        )

    def test_append_inchikey_attribute(self):
        """Test to see if inchikey is added.
        """
        expected_xml = '<phase:Molecule xmlns:phase="http://www-hunter.ch.cam.ac.uk/PhaseSchema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:cml="http://www.xml-cml.org/schema" phase:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N"/>'
        actual_xml_element = moleculexmlcreator.append_inchikey_attribute(
            self.phase_molecule_element, self.inchikey
        )
        self.assertEqual(expected_xml, etree.tounicode(actual_xml_element))

    def test_append_molecule_id(self):
        """Test to see if moleculeID is added.
        """
        expected_xml = '<phase:Molecule xmlns:phase="http://www-hunter.ch.cam.ac.uk/PhaseSchema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:cml="http://www.xml-cml.org/schema" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N"/>'
        moleculexmlcreator.append_molecule_id(
            self.phase_molecule_element, self.inchikey
        )
        self.assertEqual(expected_xml, etree.tounicode(self.phase_molecule_element))

    def test_create_phase_molecule_element(self):
        """Test to seeif expected element is created.
        """
        expected_xml = '<phase:Molecule xmlns:phase="http://www-hunter.ch.cam.ac.uk/PhaseSchema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:cml="http://www.xml-cml.org/schema"/>'
        actual_xml_element = moleculexmlcreator.create_phase_molecule_element()
        self.assertEqual(expected_xml, etree.tounicode(actual_xml_element))

    def test_create_concentration_element(self):
        """Test to see if expected element is created
        """
        expected_xml = (
            '<phase:TotalConcentration xmlns:phase="http://www-hunter.ch.cam'
            + '.ac.uk/PhaseSchema" xmlns:ssip="http://www-hunter.'
            + 'ch.cam.ac.uk/SSIP" xmlns:cml="http://www.xml-cml.org/schema" '
            + 'phase:units="MOLAR">55.3500000000</'
            + "phase:TotalConcentration>"
        )
        actual_xml_element = moleculexmlcreator.create_total_concentration_element(
            self.concentration_value
        )
        self.assertEqual(expected_xml, etree.tounicode(actual_xml_element))
