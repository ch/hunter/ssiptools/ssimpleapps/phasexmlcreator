# -*- coding: utf-8 -*-
#    phasexmlcreator provides utilites to generate XML input for phasetransfer calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlcreator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for testing teh phase XML maker script.

@author: mark
"""

import logging
import unittest
import os
import pathlib
from lxml import etree
import phasexmlcreator.phasexmlmaker as phasexmlmaker

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)


class PhaseXMLMakerTestCase(unittest.TestCase):
    """Test case for phasexmlmaker methods.
    """

    def setUp(self):
        """set up before the tests.
        """
        self.maxDiff = None
        self.temperature_info = [
            {"temperature_value": 298.0, "temperature_units": "KELVIN"}
        ]
        self.high_temperature_info = [
            {"temperature_value": 313.15, "temperature_units": "KELVIN"}
        ]
        parent_directory = pathlib.Path(__file__).parents[0]
        self.solvent_filename = (
            (parent_directory / "resources" / "concentration_examples.csv")
            .absolute()
            .as_posix()
        )
        self.solvent_temp_filename = (
            (parent_directory / "resources" / "concentration_tempexamples.csv")
            .absolute()
            .as_posix()
        )
        self.ssip_filename_list = [
            (parent_directory / "resources/XLYOFNOQVPJJNP-UHFFFAOYSA-N_ssip.xml")
            .absolute()
            .as_posix()
        ]
        self.water_phase_file = (
            (parent_directory / "resources/waterphase.xml").absolute().as_posix()
        )
        self.water_phase_temp_file = (
            (parent_directory / "resources/waterphasehightemp.xml").absolute().as_posix()
        )
        self.actual_filename = "actual_phase.xml"

    def tearDown(self):
        """clean up after tests.
        """
        del self.temperature_info
        del self.solvent_filename
        del self.ssip_filename_list
        if os.path.isfile(self.actual_filename):
            os.remove(self.actual_filename)
        del self.actual_filename

    def test_read_phase_information_write_to_xml(self):
        """Test to see if expected file is written.
        """
        phasexmlmaker.read_phase_information_write_to_xml(
            self.solvent_filename,
            self.ssip_filename_list,
            self.temperature_info,
            self.actual_filename,
        )
        with open(self.actual_filename, "r") as actual_file:
            actual_xml = actual_file.read()
            with open(self.water_phase_file) as expected_file:
                expected_xml = expected_file.read()
                self.assertMultiLineEqual(expected_xml, actual_xml)
        
        phasexmlmaker.read_phase_information_write_to_xml(
            self.solvent_temp_filename,
            self.ssip_filename_list,
            self.high_temperature_info,
            self.actual_filename,
            tempdependence=True
        )
        with open(self.actual_filename, "r") as actual_file:
            actual_xml = actual_file.read()
            with open(self.water_phase_temp_file) as expected_file:
                expected_xml = expected_file.read()
                self.assertMultiLineEqual(expected_xml, actual_xml)
        
    def test_write_phase_information_to_file_diff_temp(self):
        """Test expected file is created.

        Returns
        -------
        None.

        """
        actual_list = phasexmlmaker.read_phase_solvent_info_to_list(
            self.solvent_filename, self.ssip_filename_list
        )
        phasexmlmaker.write_phase_information_to_file_diff_temp([[self.temperature_info, actual_list]], self.actual_filename)
        with open(self.actual_filename, "r") as actual_file:
            actual_xml = actual_file.read()
            with open(self.water_phase_file) as expected_file:
                expected_xml = expected_file.read()
                self.assertMultiLineEqual(expected_xml, actual_xml)
    def test_write_phase_information_to_file(self):
        """Test to see if expected file is written.
        """
        actual_list = phasexmlmaker.read_phase_solvent_info_to_list(
            self.solvent_filename, self.ssip_filename_list
        )
        phasexmlmaker.write_phase_information_to_file(
            actual_list, self.temperature_info, self.actual_filename
        )
        with open(self.actual_filename, "r") as actual_file:
            actual_xml = actual_file.read()
            with open(self.water_phase_file) as expected_file:
                expected_xml = expected_file.read()
                self.assertMultiLineEqual(expected_xml, actual_xml)

    def test_generate_mixture_collection_element(self):
        """Test to seeif expected element is created
        """
        expected_xml = """<phase:MixtureCollection xmlns:phase="http://www-hunter.ch.cam.ac.uk/PhaseSchema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:cml="http://www.xml-cml.org/schema">
  <phase:Mixtures>
    <phase:Mixture phase:soluteID="298.000KELVIN">
      <phase:Phases>
        <phase:Phase phase:solventID="water298.000KELVIN" phase:units="MOLAR">
          <phase:Molecules>
            <phase:Molecule phase:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:moleFraction="1.000000000"><cml:molecule ssip:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" cml:id="XLYOFNOQVPJJNP-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="O" cml:id="a1" cml:x3="0.0569236794019" cml:y3="0.53497425302" cml:z3="-9.99862395488E-5"/>
            <cml:atom cml:elementType="H" cml:id="a2" cml:x3="-0.735721920018" cml:y3="-0.0219270863225" cml:z3="-1.029173866E-4"/>
            <cml:atom cml:elementType="H" cml:id="a3" cml:x3="0.785198240616" cml:y3="-0.103847166698" cml:z3="-1.97096373822E-4"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a3" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    <ssip:SurfaceInformation>
        <ssip:TotalSurfaceArea>37.48483410829331</ssip:TotalSurfaceArea>
        <ssip:NegativeSurfaceArea>18.664358683092974</ssip:NegativeSurfaceArea>
        <ssip:PositiveSurfaceArea>18.82047542519977</ssip:PositiveSurfaceArea>
        <ssip:ElectronDensityIsosurface>0.002</ssip:ElectronDensityIsosurface>
        <ssip:NumberOFMEPSPoints>2286</ssip:NumberOFMEPSPoints>
    </ssip:SurfaceInformation>
    <phase:SSIPs phase:units="MOLAR"><phase:SSIP ssip:value="2.9387503903833574" ssip:nearestAtomID="a3" cml:x3="1.509794550767402" cml:y3="-0.8784982637871289" cml:z3="-1.6986589692899999E-4" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="2.9275582734233065" ssip:nearestAtomID="a2" cml:x3="-1.4603207114038939" cml:y3="-0.790973405157027" cml:z3="0.12362427220238399" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.263074063139137" ssip:nearestAtomID="a1" cml:x3="0.156188781751346" cml:y3="1.6969169177862917" cml:z3="1.172598045109361" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.607684490829715" ssip:nearestAtomID="a1" cml:x3="0.06855491260794999" cml:y3="1.9788577934582507" cml:z3="-0.77762067563301" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP></phase:SSIPs></phase:Molecule>
          </phase:Molecules>
          <phase:Temperature phase:units="KELVIN">298.000</phase:Temperature>
        </phase:Phase>
      </phase:Phases>
    </phase:Mixture>
  </phase:Mixtures>
</phase:MixtureCollection>
"""
        actual_list = phasexmlmaker.read_phase_solvent_info_to_list(
            self.solvent_filename, self.ssip_filename_list
        )
        actual_element = phasexmlmaker.generate_mixture_collection_element(
            actual_list, self.temperature_info
        )
        self.assertMultiLineEqual(
            expected_xml, etree.tounicode(actual_element, pretty_print=True)
        )

    def test_read_phase_solvent_info_to_list(self):
        """Test to see if expected list is created.
        """
        expected_xml = """<ssip:SSIPMolecule xmlns:cml="http://www.xml-cml.org/schema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ssip:ssipSoftwareVersion="2.2.0-SNAPSHOT" ssip:parameterVersion="1.0.0" xsi:schemaLocation="http://www.xml-cml.org/schema http://www-hunter.ch.cam.ac.uk/schema/cmlschema.xsd http://www-hunter.ch.cam.ac.uk/SSIP http://www-hunter.ch.cam.ac.uk/schema/SSIP.xsd">
    <cml:molecule ssip:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" cml:id="XLYOFNOQVPJJNP-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="O" cml:id="a1" cml:x3="0.0569236794019" cml:y3="0.53497425302" cml:z3="-9.99862395488E-5"/>
            <cml:atom cml:elementType="H" cml:id="a2" cml:x3="-0.735721920018" cml:y3="-0.0219270863225" cml:z3="-1.029173866E-4"/>
            <cml:atom cml:elementType="H" cml:id="a3" cml:x3="0.785198240616" cml:y3="-0.103847166698" cml:z3="-1.97096373822E-4"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a3" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    <ssip:SurfaceInformation>
        <ssip:TotalSurfaceArea>37.48483410829331</ssip:TotalSurfaceArea>
        <ssip:NegativeSurfaceArea>18.664358683092974</ssip:NegativeSurfaceArea>
        <ssip:PositiveSurfaceArea>18.82047542519977</ssip:PositiveSurfaceArea>
        <ssip:ElectronDensityIsosurface>0.002</ssip:ElectronDensityIsosurface>
        <ssip:NumberOFMEPSPoints>2286</ssip:NumberOFMEPSPoints>
    </ssip:SurfaceInformation>
    <ssip:SSIPs>
        <ssip:SSIP ssip:value="2.9387503903833574" ssip:nearestAtomID="a3" cml:x3="1.509794550767402" cml:y3="-0.8784982637871289" cml:z3="-1.6986589692899999E-4"/>
        <ssip:SSIP ssip:value="2.9275582734233065" ssip:nearestAtomID="a2" cml:x3="-1.4603207114038939" cml:y3="-0.790973405157027" cml:z3="0.12362427220238399"/>
        <ssip:SSIP ssip:value="-6.263074063139137" ssip:nearestAtomID="a1" cml:x3="0.156188781751346" cml:y3="1.6969169177862917" cml:z3="1.172598045109361"/>
        <ssip:SSIP ssip:value="-6.607684490829715" ssip:nearestAtomID="a1" cml:x3="0.06855491260794999" cml:y3="1.9788577934582507" cml:z3="-0.77762067563301"/>
    </ssip:SSIPs>
</ssip:SSIPMolecule>"""
        expected_key_list = ["solvent_id", "solvent_name", "ssip_info_list"]
        expected_keys = [
            "concentration_value",
            "inchikey",
            "mole_fraction",
            "ssip_molecule",
        ]
        actual_list = phasexmlmaker.read_phase_solvent_info_to_list(
            self.solvent_filename, self.ssip_filename_list
        )
        self.assertEqual(1, len(actual_list))
        for solvent_dict in actual_list:
            self.assertListEqual(expected_key_list, sorted(solvent_dict.keys()))
            self.assertEqual("water", solvent_dict["solvent_id"])
            self.assertEqual("water", solvent_dict["solvent_name"])
            self.assertEqual(1, len(solvent_dict["ssip_info_list"]))
            for information_dict in solvent_dict["ssip_info_list"]:
                self.assertListEqual(expected_keys, sorted(information_dict.keys()))
                self.assertEqual(
                    "XLYOFNOQVPJJNP-UHFFFAOYSA-N", information_dict["inchikey"]
                )
                self.assertAlmostEqual(55.35, information_dict["concentration_value"])
                self.assertAlmostEqual(1.0, information_dict["mole_fraction"])
                self.assertMultiLineEqual(
                    expected_xml, etree.tounicode(information_dict["ssip_molecule"])
                )

    def test_generate_phase_info_list(self):
        """Test to see if expected list is produced.
        """
        expected_xml = """<ssip:SSIPMolecule xmlns:cml="http://www.xml-cml.org/schema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ssip:ssipSoftwareVersion="2.2.0-SNAPSHOT" ssip:parameterVersion="1.0.0" xsi:schemaLocation="http://www.xml-cml.org/schema http://www-hunter.ch.cam.ac.uk/schema/cmlschema.xsd http://www-hunter.ch.cam.ac.uk/SSIP http://www-hunter.ch.cam.ac.uk/schema/SSIP.xsd">
    <cml:molecule ssip:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" cml:id="XLYOFNOQVPJJNP-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="O" cml:id="a1" cml:x3="0.0569236794019" cml:y3="0.53497425302" cml:z3="-9.99862395488E-5"/>
            <cml:atom cml:elementType="H" cml:id="a2" cml:x3="-0.735721920018" cml:y3="-0.0219270863225" cml:z3="-1.029173866E-4"/>
            <cml:atom cml:elementType="H" cml:id="a3" cml:x3="0.785198240616" cml:y3="-0.103847166698" cml:z3="-1.97096373822E-4"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a3" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    <ssip:SurfaceInformation>
        <ssip:TotalSurfaceArea>37.48483410829331</ssip:TotalSurfaceArea>
        <ssip:NegativeSurfaceArea>18.664358683092974</ssip:NegativeSurfaceArea>
        <ssip:PositiveSurfaceArea>18.82047542519977</ssip:PositiveSurfaceArea>
        <ssip:ElectronDensityIsosurface>0.002</ssip:ElectronDensityIsosurface>
        <ssip:NumberOFMEPSPoints>2286</ssip:NumberOFMEPSPoints>
    </ssip:SurfaceInformation>
    <ssip:SSIPs>
        <ssip:SSIP ssip:value="2.9387503903833574" ssip:nearestAtomID="a3" cml:x3="1.509794550767402" cml:y3="-0.8784982637871289" cml:z3="-1.6986589692899999E-4"/>
        <ssip:SSIP ssip:value="2.9275582734233065" ssip:nearestAtomID="a2" cml:x3="-1.4603207114038939" cml:y3="-0.790973405157027" cml:z3="0.12362427220238399"/>
        <ssip:SSIP ssip:value="-6.263074063139137" ssip:nearestAtomID="a1" cml:x3="0.156188781751346" cml:y3="1.6969169177862917" cml:z3="1.172598045109361"/>
        <ssip:SSIP ssip:value="-6.607684490829715" ssip:nearestAtomID="a1" cml:x3="0.06855491260794999" cml:y3="1.9788577934582507" cml:z3="-0.77762067563301"/>
    </ssip:SSIPs>
</ssip:SSIPMolecule>"""
        expected_key_list = ["solvent_id", "solvent_name", "ssip_info_list"]
        expected_keys = [
            "concentration_value",
            "inchikey",
            "mole_fraction",
            "ssip_molecule",
        ]
        actual_dictionary = phasexmlmaker.read_and_merge_solvent_information(
            self.solvent_filename, self.ssip_filename_list
        )
        actual_list = phasexmlmaker.generate_phase_info_list(actual_dictionary)
        self.assertEqual(1, len(actual_list))
        for solvent_dict in actual_list:
            self.assertListEqual(expected_key_list, sorted(solvent_dict.keys()))
            self.assertEqual("water", solvent_dict["solvent_id"])
            self.assertEqual("water", solvent_dict["solvent_name"])
            self.assertEqual(1, len(solvent_dict["ssip_info_list"]))
            for information_dict in solvent_dict["ssip_info_list"]:
                self.assertListEqual(expected_keys, sorted(information_dict.keys()))
                self.assertEqual(
                    "XLYOFNOQVPJJNP-UHFFFAOYSA-N", information_dict["inchikey"]
                )
                self.assertAlmostEqual(55.35, information_dict["concentration_value"])
                self.assertAlmostEqual(1.0, information_dict["mole_fraction"])
                self.assertMultiLineEqual(
                    expected_xml, etree.tounicode(information_dict["ssip_molecule"])
                )

    def test_read_and_merge_solvent_information(self):
        """Test to see if expected information is read in.
        """
        expected_xml = """<ssip:SSIPMolecule xmlns:cml="http://www.xml-cml.org/schema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ssip:ssipSoftwareVersion="2.2.0-SNAPSHOT" ssip:parameterVersion="1.0.0" xsi:schemaLocation="http://www.xml-cml.org/schema http://www-hunter.ch.cam.ac.uk/schema/cmlschema.xsd http://www-hunter.ch.cam.ac.uk/SSIP http://www-hunter.ch.cam.ac.uk/schema/SSIP.xsd">
    <cml:molecule ssip:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" cml:id="XLYOFNOQVPJJNP-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="O" cml:id="a1" cml:x3="0.0569236794019" cml:y3="0.53497425302" cml:z3="-9.99862395488E-5"/>
            <cml:atom cml:elementType="H" cml:id="a2" cml:x3="-0.735721920018" cml:y3="-0.0219270863225" cml:z3="-1.029173866E-4"/>
            <cml:atom cml:elementType="H" cml:id="a3" cml:x3="0.785198240616" cml:y3="-0.103847166698" cml:z3="-1.97096373822E-4"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a3" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    <ssip:SurfaceInformation>
        <ssip:TotalSurfaceArea>37.48483410829331</ssip:TotalSurfaceArea>
        <ssip:NegativeSurfaceArea>18.664358683092974</ssip:NegativeSurfaceArea>
        <ssip:PositiveSurfaceArea>18.82047542519977</ssip:PositiveSurfaceArea>
        <ssip:ElectronDensityIsosurface>0.002</ssip:ElectronDensityIsosurface>
        <ssip:NumberOFMEPSPoints>2286</ssip:NumberOFMEPSPoints>
    </ssip:SurfaceInformation>
    <ssip:SSIPs>
        <ssip:SSIP ssip:value="2.9387503903833574" ssip:nearestAtomID="a3" cml:x3="1.509794550767402" cml:y3="-0.8784982637871289" cml:z3="-1.6986589692899999E-4"/>
        <ssip:SSIP ssip:value="2.9275582734233065" ssip:nearestAtomID="a2" cml:x3="-1.4603207114038939" cml:y3="-0.790973405157027" cml:z3="0.12362427220238399"/>
        <ssip:SSIP ssip:value="-6.263074063139137" ssip:nearestAtomID="a1" cml:x3="0.156188781751346" cml:y3="1.6969169177862917" cml:z3="1.172598045109361"/>
        <ssip:SSIP ssip:value="-6.607684490829715" ssip:nearestAtomID="a1" cml:x3="0.06855491260794999" cml:y3="1.9788577934582507" cml:z3="-0.77762067563301"/>
    </ssip:SSIPs>
</ssip:SSIPMolecule>"""
        expected_key_list = ["solvent_id", "solvent_name", "ssip_info_list"]
        expected_keys = [
            "concentration_value",
            "inchikey",
            "mole_fraction",
            "ssip_molecule",
        ]
        actual_dictionary = phasexmlmaker.read_and_merge_solvent_information(
            self.solvent_filename, self.ssip_filename_list
        )
        self.assertEqual(1, len(actual_dictionary))
        for inchikey, solvent_dict in actual_dictionary.items():
            self.assertEqual("XLYOFNOQVPJJNP-UHFFFAOYSA-N", inchikey)
            self.assertListEqual(expected_key_list, sorted(solvent_dict.keys()))
            self.assertEqual("water", solvent_dict["solvent_id"])
            self.assertEqual("water", solvent_dict["solvent_name"])
            self.assertEqual(1, len(solvent_dict["ssip_info_list"]))
            for information_dict in solvent_dict["ssip_info_list"]:
                self.assertListEqual(expected_keys, sorted(information_dict.keys()))
                self.assertEqual(
                    "XLYOFNOQVPJJNP-UHFFFAOYSA-N", information_dict["inchikey"]
                )
                self.assertAlmostEqual(55.35, information_dict["concentration_value"])
                self.assertAlmostEqual(1.0, information_dict["mole_fraction"])
                self.assertMultiLineEqual(
                    expected_xml, etree.tounicode(information_dict["ssip_molecule"])
                )
