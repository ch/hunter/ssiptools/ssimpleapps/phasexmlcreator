# -*- coding: utf-8 -*-
#    phasexmlcreator provides utilites to generate XML input for phasetransfer calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlcreator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
tests for the solventxmlcreator script methods.

@author: mark
"""

import logging
import unittest
import os
import pathlib
from lxml import etree
import phasexmlcreator.moleculexmlcreator as moleculexmlcreator
import phasexmlcreator.solventxmlcreator as solventxmlcreator

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class SolventXMLCreatorTestCase(unittest.TestCase):
    """Test case for solutexmlcreator script.
    """

    def setUp(self):
        """Set up for tests.
        """
        self.maxDiff = None
        self.solvent_list_element = solventxmlcreator.create_solvent_list_element()
        self.solvent_element = solventxmlcreator.create_solvent_element()
        self.concentration_value = 55.35
        self.inchikey = "XLYOFNOQVPJJNP-UHFFFAOYSA-N"
        self.ssipList = """<ssip:SSIPMolecule xmlns:cml="http://www.xml-cml.org/schema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ssip:ssipSoftwareVersion="2.2.0-SNAPSHOT" ssip:parameterVersion="1.0.0" xsi:schemaLocation="http://www.xml-cml.org/schema http://www-hunter.ch.cam.ac.uk/schema/cmlschema.xsd http://www-hunter.ch.cam.ac.uk/SSIP http://www-hunter.ch.cam.ac.uk/schema/SSIP.xsd">
    <cml:molecule ssip:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" cml:id="XLYOFNOQVPJJNP-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="O" cml:id="a1" cml:x3="0.0569236794019" cml:y3="0.53497425302" cml:z3="-9.99862395488E-5"/>
            <cml:atom cml:elementType="H" cml:id="a2" cml:x3="-0.735721920018" cml:y3="-0.0219270863225" cml:z3="-1.029173866E-4"/>
            <cml:atom cml:elementType="H" cml:id="a3" cml:x3="0.785198240616" cml:y3="-0.103847166698" cml:z3="-1.97096373822E-4"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a3" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    <ssip:SurfaceInformation>
        <ssip:TotalSurfaceArea>37.48483410829331</ssip:TotalSurfaceArea>
        <ssip:NegativeSurfaceArea>18.664358683092974</ssip:NegativeSurfaceArea>
        <ssip:PositiveSurfaceArea>18.82047542519977</ssip:PositiveSurfaceArea>
        <ssip:ElectronDensityIsosurface>0.002</ssip:ElectronDensityIsosurface>
        <ssip:NumberOFMEPSPoints>2286</ssip:NumberOFMEPSPoints>
    </ssip:SurfaceInformation>
    <ssip:SSIPs>
        <ssip:SSIP ssip:value="2.9387503903833574" ssip:nearestAtomID="a3" cml:x3="1.509794550767402" cml:y3="-0.8784982637871289" cml:z3="-1.6986589692899999E-4"/>
        <ssip:SSIP ssip:value="2.9275582734233065" ssip:nearestAtomID="a2" cml:x3="-1.4603207114038939" cml:y3="-0.790973405157027" cml:z3="0.12362427220238399"/>
        <ssip:SSIP ssip:value="-6.263074063139137" ssip:nearestAtomID="a1" cml:x3="0.156188781751346" cml:y3="1.6969169177862917" cml:z3="1.172598045109361"/>
        <ssip:SSIP ssip:value="-6.607684490829715" ssip:nearestAtomID="a1" cml:x3="0.06855491260794999" cml:y3="1.9788577934582507" cml:z3="-0.77762067563301"/>
    </ssip:SSIPs>
</ssip:SSIPMolecule>"""
        self.ssip_etree = etree.XML(self.ssipList)
        self.molecule_element = moleculexmlcreator.generate_phase_molecule(
            self.inchikey, self.inchikey, self.ssip_etree, self.concentration_value
        )
        self.ssip_list_dict = {
            "inchikey": self.inchikey,
            "concentration_value": self.concentration_value,
            "ssip_molecule": self.ssip_etree,
        }
        self.solvent_info_dict = {
            "solvent_id": "water",
            "solvent_name": "water",
            "ssip_info_list": [self.ssip_list_dict],
        }
        parent_directory = pathlib.Path(__file__).parents[0]
        self.water_solvent_file = (
            (parent_directory / "resources/watersolvent.xml").absolute().as_posix()
        )
        self.actual_filename = "actual_solvent.xml"

    def tearDown(self):
        """clean up after tests.
        """
        del self.solvent_list_element
        del self.solvent_element
        del self.concentration_value
        del self.inchikey
        del self.ssipList
        del self.ssip_etree
        del self.molecule_element
        del self.ssip_list_dict
        del self.solvent_info_dict
        if os.path.isfile(self.actual_filename):
            os.remove(self.actual_filename)
        del self.actual_filename

    def test_write_element_tree_to_file(self):
        """Test to see if file is written out as expected.
        """
        solvent_list_element = solventxmlcreator.generate_solvent_list_element(
            [self.solvent_info_dict]
        )
        solvent_list_etree = solventxmlcreator.create_element_tree(solvent_list_element)
        solventxmlcreator.write_element_tree_to_file(
            solvent_list_etree, self.actual_filename
        )
        with open(self.actual_filename, "r") as actual_file:
            actual_xml = actual_file.read()
            with open(self.water_solvent_file) as expected_file:
                expected_xml = expected_file.read()
                self.assertMultiLineEqual(expected_xml, actual_xml)

    def test_create_element_tree(self):
        """Test to see if expected element tree is produced.
        """
        expected_xml = """<phase:SolventList xmlns:phase="http://www-hunter.ch.cam.ac.uk/PhaseSchema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:cml="http://www.xml-cml.org/schema">
  <phase:Solvents>
    <phase:Solvent phase:solventID="water" phase:solventName="water" phase:units="MOLAR">
      <phase:Molecules>
        <phase:Molecule phase:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N"><cml:molecule ssip:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" cml:id="XLYOFNOQVPJJNP-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="O" cml:id="a1" cml:x3="0.0569236794019" cml:y3="0.53497425302" cml:z3="-9.99862395488E-5"/>
            <cml:atom cml:elementType="H" cml:id="a2" cml:x3="-0.735721920018" cml:y3="-0.0219270863225" cml:z3="-1.029173866E-4"/>
            <cml:atom cml:elementType="H" cml:id="a3" cml:x3="0.785198240616" cml:y3="-0.103847166698" cml:z3="-1.97096373822E-4"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a3" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    <ssip:SurfaceInformation>
        <ssip:TotalSurfaceArea>37.48483410829331</ssip:TotalSurfaceArea>
        <ssip:NegativeSurfaceArea>18.664358683092974</ssip:NegativeSurfaceArea>
        <ssip:PositiveSurfaceArea>18.82047542519977</ssip:PositiveSurfaceArea>
        <ssip:ElectronDensityIsosurface>0.002</ssip:ElectronDensityIsosurface>
        <ssip:NumberOFMEPSPoints>2286</ssip:NumberOFMEPSPoints>
    </ssip:SurfaceInformation>
    <phase:SSIPs phase:units="MOLAR"><phase:SSIP ssip:value="2.9387503903833574" ssip:nearestAtomID="a3" cml:x3="1.509794550767402" cml:y3="-0.8784982637871289" cml:z3="-1.6986589692899999E-4" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="2.9275582734233065" ssip:nearestAtomID="a2" cml:x3="-1.4603207114038939" cml:y3="-0.790973405157027" cml:z3="0.12362427220238399" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.263074063139137" ssip:nearestAtomID="a1" cml:x3="0.156188781751346" cml:y3="1.6969169177862917" cml:z3="1.172598045109361" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.607684490829715" ssip:nearestAtomID="a1" cml:x3="0.06855491260794999" cml:y3="1.9788577934582507" cml:z3="-0.77762067563301" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP></phase:SSIPs></phase:Molecule>
      </phase:Molecules>
    </phase:Solvent>
  </phase:Solvents>
</phase:SolventList>
"""
        solvent_list_element = solventxmlcreator.generate_solvent_list_element(
            [self.solvent_info_dict]
        )
        solvent_list_etree = solventxmlcreator.create_element_tree(solvent_list_element)
        self.assertMultiLineEqual(
            expected_xml, etree.tounicode(solvent_list_etree, pretty_print=True)
        )

    def test_generate_solvent_list_element(self):
        """Test to see if expected SolventList is generated.
        """
        expected_xml = """<phase:SolventList xmlns:phase="http://www-hunter.ch.cam.ac.uk/PhaseSchema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:cml="http://www.xml-cml.org/schema">
  <phase:Solvents>
    <phase:Solvent phase:solventID="water" phase:solventName="water" phase:units="MOLAR">
      <phase:Molecules>
        <phase:Molecule phase:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N"><cml:molecule ssip:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" cml:id="XLYOFNOQVPJJNP-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="O" cml:id="a1" cml:x3="0.0569236794019" cml:y3="0.53497425302" cml:z3="-9.99862395488E-5"/>
            <cml:atom cml:elementType="H" cml:id="a2" cml:x3="-0.735721920018" cml:y3="-0.0219270863225" cml:z3="-1.029173866E-4"/>
            <cml:atom cml:elementType="H" cml:id="a3" cml:x3="0.785198240616" cml:y3="-0.103847166698" cml:z3="-1.97096373822E-4"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a3" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    <ssip:SurfaceInformation>
        <ssip:TotalSurfaceArea>37.48483410829331</ssip:TotalSurfaceArea>
        <ssip:NegativeSurfaceArea>18.664358683092974</ssip:NegativeSurfaceArea>
        <ssip:PositiveSurfaceArea>18.82047542519977</ssip:PositiveSurfaceArea>
        <ssip:ElectronDensityIsosurface>0.002</ssip:ElectronDensityIsosurface>
        <ssip:NumberOFMEPSPoints>2286</ssip:NumberOFMEPSPoints>
    </ssip:SurfaceInformation>
    <phase:SSIPs phase:units="MOLAR"><phase:SSIP ssip:value="2.9387503903833574" ssip:nearestAtomID="a3" cml:x3="1.509794550767402" cml:y3="-0.8784982637871289" cml:z3="-1.6986589692899999E-4" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="2.9275582734233065" ssip:nearestAtomID="a2" cml:x3="-1.4603207114038939" cml:y3="-0.790973405157027" cml:z3="0.12362427220238399" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.263074063139137" ssip:nearestAtomID="a1" cml:x3="0.156188781751346" cml:y3="1.6969169177862917" cml:z3="1.172598045109361" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.607684490829715" ssip:nearestAtomID="a1" cml:x3="0.06855491260794999" cml:y3="1.9788577934582507" cml:z3="-0.77762067563301" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP></phase:SSIPs></phase:Molecule>
      </phase:Molecules>
    </phase:Solvent>
  </phase:Solvents>
</phase:SolventList>
"""
        solvent_list_element = solventxmlcreator.generate_solvent_list_element(
            [self.solvent_info_dict]
        )
        self.assertMultiLineEqual(
            expected_xml, etree.tounicode(solvent_list_element, pretty_print=True)
        )

    def test_append_solvent_element_list_to_solvent_list_element(self):
        """Test to see if solvent elements are appended.
        """
        expected_xml = """<phase:SolventList xmlns:phase="http://www-hunter.ch.cam.ac.uk/PhaseSchema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:cml="http://www.xml-cml.org/schema">
  <phase:Solvents>
    <phase:Solvent phase:solventID="water" phase:solventName="water" phase:units="MOLAR">
      <phase:Molecules>
        <phase:Molecule phase:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N"><cml:molecule ssip:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" cml:id="XLYOFNOQVPJJNP-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="O" cml:id="a1" cml:x3="0.0569236794019" cml:y3="0.53497425302" cml:z3="-9.99862395488E-5"/>
            <cml:atom cml:elementType="H" cml:id="a2" cml:x3="-0.735721920018" cml:y3="-0.0219270863225" cml:z3="-1.029173866E-4"/>
            <cml:atom cml:elementType="H" cml:id="a3" cml:x3="0.785198240616" cml:y3="-0.103847166698" cml:z3="-1.97096373822E-4"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a3" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    <ssip:SurfaceInformation>
        <ssip:TotalSurfaceArea>37.48483410829331</ssip:TotalSurfaceArea>
        <ssip:NegativeSurfaceArea>18.664358683092974</ssip:NegativeSurfaceArea>
        <ssip:PositiveSurfaceArea>18.82047542519977</ssip:PositiveSurfaceArea>
        <ssip:ElectronDensityIsosurface>0.002</ssip:ElectronDensityIsosurface>
        <ssip:NumberOFMEPSPoints>2286</ssip:NumberOFMEPSPoints>
    </ssip:SurfaceInformation>
    <phase:SSIPs phase:units="MOLAR"><phase:SSIP ssip:value="2.9387503903833574" ssip:nearestAtomID="a3" cml:x3="1.509794550767402" cml:y3="-0.8784982637871289" cml:z3="-1.6986589692899999E-4" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="2.9275582734233065" ssip:nearestAtomID="a2" cml:x3="-1.4603207114038939" cml:y3="-0.790973405157027" cml:z3="0.12362427220238399" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.263074063139137" ssip:nearestAtomID="a1" cml:x3="0.156188781751346" cml:y3="1.6969169177862917" cml:z3="1.172598045109361" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.607684490829715" ssip:nearestAtomID="a1" cml:x3="0.06855491260794999" cml:y3="1.9788577934582507" cml:z3="-0.77762067563301" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP></phase:SSIPs></phase:Molecule>
      </phase:Molecules>
    </phase:Solvent>
  </phase:Solvents>
</phase:SolventList>
"""
        solvent_element = solventxmlcreator.generate_solvent_element_from_value_dictionary(
            self.solvent_info_dict
        )
        solventxmlcreator.append_solvent_element_list_to_solvent_list_element(
            self.solvent_list_element, [solvent_element]
        )
        self.assertMultiLineEqual(
            expected_xml, etree.tounicode(self.solvent_list_element, pretty_print=True)
        )

    def test_append_solvent_element_to_solvent_list_element(self):
        """Test to see if solvent element is appended as expected.
        """
        expected_xml = """<phase:SolventList xmlns:phase="http://www-hunter.ch.cam.ac.uk/PhaseSchema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:cml="http://www.xml-cml.org/schema">
  <phase:Solvent phase:solventID="water" phase:solventName="water" phase:units="MOLAR">
    <phase:Molecules>
      <phase:Molecule phase:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N"><cml:molecule ssip:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" cml:id="XLYOFNOQVPJJNP-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="O" cml:id="a1" cml:x3="0.0569236794019" cml:y3="0.53497425302" cml:z3="-9.99862395488E-5"/>
            <cml:atom cml:elementType="H" cml:id="a2" cml:x3="-0.735721920018" cml:y3="-0.0219270863225" cml:z3="-1.029173866E-4"/>
            <cml:atom cml:elementType="H" cml:id="a3" cml:x3="0.785198240616" cml:y3="-0.103847166698" cml:z3="-1.97096373822E-4"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a3" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    <ssip:SurfaceInformation>
        <ssip:TotalSurfaceArea>37.48483410829331</ssip:TotalSurfaceArea>
        <ssip:NegativeSurfaceArea>18.664358683092974</ssip:NegativeSurfaceArea>
        <ssip:PositiveSurfaceArea>18.82047542519977</ssip:PositiveSurfaceArea>
        <ssip:ElectronDensityIsosurface>0.002</ssip:ElectronDensityIsosurface>
        <ssip:NumberOFMEPSPoints>2286</ssip:NumberOFMEPSPoints>
    </ssip:SurfaceInformation>
    <phase:SSIPs phase:units="MOLAR"><phase:SSIP ssip:value="2.9387503903833574" ssip:nearestAtomID="a3" cml:x3="1.509794550767402" cml:y3="-0.8784982637871289" cml:z3="-1.6986589692899999E-4" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="2.9275582734233065" ssip:nearestAtomID="a2" cml:x3="-1.4603207114038939" cml:y3="-0.790973405157027" cml:z3="0.12362427220238399" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.263074063139137" ssip:nearestAtomID="a1" cml:x3="0.156188781751346" cml:y3="1.6969169177862917" cml:z3="1.172598045109361" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.607684490829715" ssip:nearestAtomID="a1" cml:x3="0.06855491260794999" cml:y3="1.9788577934582507" cml:z3="-0.77762067563301" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP></phase:SSIPs></phase:Molecule>
    </phase:Molecules>
  </phase:Solvent>
</phase:SolventList>
"""
        solvent_element = solventxmlcreator.generate_solvent_element_from_value_dictionary(
            self.solvent_info_dict
        )
        solventxmlcreator.append_child_element_to_parent_element(
            self.solvent_list_element, solvent_element
        )
        self.assertMultiLineEqual(
            expected_xml, etree.tounicode(self.solvent_list_element, pretty_print=True)
        )

    def test_create_solvent_list_element(self):
        """Test to see if expected element is created.
        """
        expected_xml = '<phase:SolventList xmlns:phase="http://www-hunter.ch.cam.ac.uk/PhaseSchema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:cml="http://www.xml-cml.org/schema"/>'
        actual_xml_element = solventxmlcreator.create_solvent_list_element()
        self.assertEqual(expected_xml, etree.tounicode(actual_xml_element))

    def test_generate_solvent_element_list_from_information(self):
        """Test to see if expected elements are created.
        """
        expected_xml = """<phase:Solvent xmlns:phase="http://www-hunter.ch.cam.ac.uk/PhaseSchema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:cml="http://www.xml-cml.org/schema" phase:solventID="water" phase:solventName="water" phase:units="MOLAR">
  <phase:Molecules>
    <phase:Molecule phase:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N"><cml:molecule ssip:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" cml:id="XLYOFNOQVPJJNP-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="O" cml:id="a1" cml:x3="0.0569236794019" cml:y3="0.53497425302" cml:z3="-9.99862395488E-5"/>
            <cml:atom cml:elementType="H" cml:id="a2" cml:x3="-0.735721920018" cml:y3="-0.0219270863225" cml:z3="-1.029173866E-4"/>
            <cml:atom cml:elementType="H" cml:id="a3" cml:x3="0.785198240616" cml:y3="-0.103847166698" cml:z3="-1.97096373822E-4"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a3" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    <ssip:SurfaceInformation>
        <ssip:TotalSurfaceArea>37.48483410829331</ssip:TotalSurfaceArea>
        <ssip:NegativeSurfaceArea>18.664358683092974</ssip:NegativeSurfaceArea>
        <ssip:PositiveSurfaceArea>18.82047542519977</ssip:PositiveSurfaceArea>
        <ssip:ElectronDensityIsosurface>0.002</ssip:ElectronDensityIsosurface>
        <ssip:NumberOFMEPSPoints>2286</ssip:NumberOFMEPSPoints>
    </ssip:SurfaceInformation>
    <phase:SSIPs phase:units="MOLAR"><phase:SSIP ssip:value="2.9387503903833574" ssip:nearestAtomID="a3" cml:x3="1.509794550767402" cml:y3="-0.8784982637871289" cml:z3="-1.6986589692899999E-4" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="2.9275582734233065" ssip:nearestAtomID="a2" cml:x3="-1.4603207114038939" cml:y3="-0.790973405157027" cml:z3="0.12362427220238399" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.263074063139137" ssip:nearestAtomID="a1" cml:x3="0.156188781751346" cml:y3="1.6969169177862917" cml:z3="1.172598045109361" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.607684490829715" ssip:nearestAtomID="a1" cml:x3="0.06855491260794999" cml:y3="1.9788577934582507" cml:z3="-0.77762067563301" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP></phase:SSIPs></phase:Molecule>
  </phase:Molecules>
</phase:Solvent>
"""
        actual_xml_element_list = solventxmlcreator.generate_solvent_element_list_from_information(
            [self.solvent_info_dict]
        )
        self.assertEqual(1, len(actual_xml_element_list))
        for actual_xml_element in actual_xml_element_list:
            self.assertMultiLineEqual(
                expected_xml, etree.tounicode(actual_xml_element, pretty_print=True)
            )

    def test_generate_solvent_element_from_value_dictionary(self):
        """Test to see if the expected solvent element is created.
        """
        expected_xml = """<phase:Solvent xmlns:phase="http://www-hunter.ch.cam.ac.uk/PhaseSchema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:cml="http://www.xml-cml.org/schema" phase:solventID="water" phase:solventName="water" phase:units="MOLAR">
  <phase:Molecules>
    <phase:Molecule phase:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N"><cml:molecule ssip:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" cml:id="XLYOFNOQVPJJNP-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="O" cml:id="a1" cml:x3="0.0569236794019" cml:y3="0.53497425302" cml:z3="-9.99862395488E-5"/>
            <cml:atom cml:elementType="H" cml:id="a2" cml:x3="-0.735721920018" cml:y3="-0.0219270863225" cml:z3="-1.029173866E-4"/>
            <cml:atom cml:elementType="H" cml:id="a3" cml:x3="0.785198240616" cml:y3="-0.103847166698" cml:z3="-1.97096373822E-4"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a3" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    <ssip:SurfaceInformation>
        <ssip:TotalSurfaceArea>37.48483410829331</ssip:TotalSurfaceArea>
        <ssip:NegativeSurfaceArea>18.664358683092974</ssip:NegativeSurfaceArea>
        <ssip:PositiveSurfaceArea>18.82047542519977</ssip:PositiveSurfaceArea>
        <ssip:ElectronDensityIsosurface>0.002</ssip:ElectronDensityIsosurface>
        <ssip:NumberOFMEPSPoints>2286</ssip:NumberOFMEPSPoints>
    </ssip:SurfaceInformation>
    <phase:SSIPs phase:units="MOLAR"><phase:SSIP ssip:value="2.9387503903833574" ssip:nearestAtomID="a3" cml:x3="1.509794550767402" cml:y3="-0.8784982637871289" cml:z3="-1.6986589692899999E-4" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="2.9275582734233065" ssip:nearestAtomID="a2" cml:x3="-1.4603207114038939" cml:y3="-0.790973405157027" cml:z3="0.12362427220238399" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.263074063139137" ssip:nearestAtomID="a1" cml:x3="0.156188781751346" cml:y3="1.6969169177862917" cml:z3="1.172598045109361" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.607684490829715" ssip:nearestAtomID="a1" cml:x3="0.06855491260794999" cml:y3="1.9788577934582507" cml:z3="-0.77762067563301" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP></phase:SSIPs></phase:Molecule>
  </phase:Molecules>
</phase:Solvent>
"""
        actual_xml_element = solventxmlcreator.generate_solvent_element_from_value_dictionary(
            self.solvent_info_dict
        )
        self.assertMultiLineEqual(
            expected_xml, etree.tounicode(actual_xml_element, pretty_print=True)
        )

    def test_generate_solvent_element_from_ssip_lists(self):
        """Test to see if expected solvent element is created.
        """
        expected_xml = """<phase:Solvent xmlns:phase="http://www-hunter.ch.cam.ac.uk/PhaseSchema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:cml="http://www.xml-cml.org/schema" phase:solventID="water" phase:solventName="water" phase:units="MOLAR">
  <phase:Molecules>
    <phase:Molecule phase:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N"><cml:molecule ssip:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" cml:id="XLYOFNOQVPJJNP-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="O" cml:id="a1" cml:x3="0.0569236794019" cml:y3="0.53497425302" cml:z3="-9.99862395488E-5"/>
            <cml:atom cml:elementType="H" cml:id="a2" cml:x3="-0.735721920018" cml:y3="-0.0219270863225" cml:z3="-1.029173866E-4"/>
            <cml:atom cml:elementType="H" cml:id="a3" cml:x3="0.785198240616" cml:y3="-0.103847166698" cml:z3="-1.97096373822E-4"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a3" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    <ssip:SurfaceInformation>
        <ssip:TotalSurfaceArea>37.48483410829331</ssip:TotalSurfaceArea>
        <ssip:NegativeSurfaceArea>18.664358683092974</ssip:NegativeSurfaceArea>
        <ssip:PositiveSurfaceArea>18.82047542519977</ssip:PositiveSurfaceArea>
        <ssip:ElectronDensityIsosurface>0.002</ssip:ElectronDensityIsosurface>
        <ssip:NumberOFMEPSPoints>2286</ssip:NumberOFMEPSPoints>
    </ssip:SurfaceInformation>
    <phase:SSIPs phase:units="MOLAR"><phase:SSIP ssip:value="2.9387503903833574" ssip:nearestAtomID="a3" cml:x3="1.509794550767402" cml:y3="-0.8784982637871289" cml:z3="-1.6986589692899999E-4" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="2.9275582734233065" ssip:nearestAtomID="a2" cml:x3="-1.4603207114038939" cml:y3="-0.790973405157027" cml:z3="0.12362427220238399" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.263074063139137" ssip:nearestAtomID="a1" cml:x3="0.156188781751346" cml:y3="1.6969169177862917" cml:z3="1.172598045109361" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.607684490829715" ssip:nearestAtomID="a1" cml:x3="0.06855491260794999" cml:y3="1.9788577934582507" cml:z3="-0.77762067563301" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP></phase:SSIPs></phase:Molecule>
  </phase:Molecules>
</phase:Solvent>
"""
        actual_xml_element = solventxmlcreator.generate_solvent_element_from_ssip_lists(
            "water", "water", [self.ssip_list_dict]
        )
        self.assertMultiLineEqual(
            expected_xml, etree.tounicode(actual_xml_element, pretty_print=True)
        )

    def test_generate_solvent_element(self):
        """Test to see if expected solvent element is generated.
        """
        expected_xml = """<phase:Solvent xmlns:phase="http://www-hunter.ch.cam.ac.uk/PhaseSchema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:cml="http://www.xml-cml.org/schema" phase:solventID="water" phase:solventName="water" phase:units="MOLAR">
  <phase:Molecules>
    <phase:Molecule phase:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N"><cml:molecule ssip:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" cml:id="XLYOFNOQVPJJNP-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="O" cml:id="a1" cml:x3="0.0569236794019" cml:y3="0.53497425302" cml:z3="-9.99862395488E-5"/>
            <cml:atom cml:elementType="H" cml:id="a2" cml:x3="-0.735721920018" cml:y3="-0.0219270863225" cml:z3="-1.029173866E-4"/>
            <cml:atom cml:elementType="H" cml:id="a3" cml:x3="0.785198240616" cml:y3="-0.103847166698" cml:z3="-1.97096373822E-4"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a3" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    <ssip:SurfaceInformation>
        <ssip:TotalSurfaceArea>37.48483410829331</ssip:TotalSurfaceArea>
        <ssip:NegativeSurfaceArea>18.664358683092974</ssip:NegativeSurfaceArea>
        <ssip:PositiveSurfaceArea>18.82047542519977</ssip:PositiveSurfaceArea>
        <ssip:ElectronDensityIsosurface>0.002</ssip:ElectronDensityIsosurface>
        <ssip:NumberOFMEPSPoints>2286</ssip:NumberOFMEPSPoints>
    </ssip:SurfaceInformation>
    <phase:SSIPs phase:units="MOLAR"><phase:SSIP ssip:value="2.9387503903833574" ssip:nearestAtomID="a3" cml:x3="1.509794550767402" cml:y3="-0.8784982637871289" cml:z3="-1.6986589692899999E-4" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="2.9275582734233065" ssip:nearestAtomID="a2" cml:x3="-1.4603207114038939" cml:y3="-0.790973405157027" cml:z3="0.12362427220238399" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.263074063139137" ssip:nearestAtomID="a1" cml:x3="0.156188781751346" cml:y3="1.6969169177862917" cml:z3="1.172598045109361" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.607684490829715" ssip:nearestAtomID="a1" cml:x3="0.06855491260794999" cml:y3="1.9788577934582507" cml:z3="-0.77762067563301" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP></phase:SSIPs></phase:Molecule>
  </phase:Molecules>
</phase:Solvent>
"""
        actual_xml_element = solventxmlcreator.generate_solvent_element(
            "water", "water", [self.molecule_element]
        )
        self.assertMultiLineEqual(
            expected_xml, etree.tounicode(actual_xml_element, pretty_print=True)
        )

    def test_generate_molecule_element_list(self):
        """test to see if expected molecule list is created.
        """
        expected_xml = """<phase:Molecule xmlns:phase="http://www-hunter.ch.cam.ac.uk/PhaseSchema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:cml="http://www.xml-cml.org/schema" phase:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N"><cml:molecule ssip:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" cml:id="XLYOFNOQVPJJNP-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="O" cml:id="a1" cml:x3="0.0569236794019" cml:y3="0.53497425302" cml:z3="-9.99862395488E-5"/>
            <cml:atom cml:elementType="H" cml:id="a2" cml:x3="-0.735721920018" cml:y3="-0.0219270863225" cml:z3="-1.029173866E-4"/>
            <cml:atom cml:elementType="H" cml:id="a3" cml:x3="0.785198240616" cml:y3="-0.103847166698" cml:z3="-1.97096373822E-4"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a3" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    <ssip:SurfaceInformation>
        <ssip:TotalSurfaceArea>37.48483410829331</ssip:TotalSurfaceArea>
        <ssip:NegativeSurfaceArea>18.664358683092974</ssip:NegativeSurfaceArea>
        <ssip:PositiveSurfaceArea>18.82047542519977</ssip:PositiveSurfaceArea>
        <ssip:ElectronDensityIsosurface>0.002</ssip:ElectronDensityIsosurface>
        <ssip:NumberOFMEPSPoints>2286</ssip:NumberOFMEPSPoints>
    </ssip:SurfaceInformation>
    <phase:SSIPs phase:units="MOLAR"><phase:SSIP ssip:value="2.9387503903833574" ssip:nearestAtomID="a3" cml:x3="1.509794550767402" cml:y3="-0.8784982637871289" cml:z3="-1.6986589692899999E-4" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="2.9275582734233065" ssip:nearestAtomID="a2" cml:x3="-1.4603207114038939" cml:y3="-0.790973405157027" cml:z3="0.12362427220238399" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.263074063139137" ssip:nearestAtomID="a1" cml:x3="0.156188781751346" cml:y3="1.6969169177862917" cml:z3="1.172598045109361" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.607684490829715" ssip:nearestAtomID="a1" cml:x3="0.06855491260794999" cml:y3="1.9788577934582507" cml:z3="-0.77762067563301" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP></phase:SSIPs></phase:Molecule>
"""
        molecule_element_list = solventxmlcreator.generate_molecule_element_list(
            [self.ssip_list_dict]
        )
        self.assertEqual(1, len(molecule_element_list))
        for molecule_element in molecule_element_list:
            self.assertMultiLineEqual(
                expected_xml, etree.tounicode(molecule_element, pretty_print=True)
            )

    def test_append_molecule_element_list_to_solvent(self):
        """Test to see if molecule list is appended.
        """
        expected_xml = """<phase:Solvent xmlns:phase="http://www-hunter.ch.cam.ac.uk/PhaseSchema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:cml="http://www.xml-cml.org/schema">
  <phase:Molecules>
    <phase:Molecule phase:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N"><cml:molecule ssip:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" cml:id="XLYOFNOQVPJJNP-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="O" cml:id="a1" cml:x3="0.0569236794019" cml:y3="0.53497425302" cml:z3="-9.99862395488E-5"/>
            <cml:atom cml:elementType="H" cml:id="a2" cml:x3="-0.735721920018" cml:y3="-0.0219270863225" cml:z3="-1.029173866E-4"/>
            <cml:atom cml:elementType="H" cml:id="a3" cml:x3="0.785198240616" cml:y3="-0.103847166698" cml:z3="-1.97096373822E-4"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a3" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    <ssip:SurfaceInformation>
        <ssip:TotalSurfaceArea>37.48483410829331</ssip:TotalSurfaceArea>
        <ssip:NegativeSurfaceArea>18.664358683092974</ssip:NegativeSurfaceArea>
        <ssip:PositiveSurfaceArea>18.82047542519977</ssip:PositiveSurfaceArea>
        <ssip:ElectronDensityIsosurface>0.002</ssip:ElectronDensityIsosurface>
        <ssip:NumberOFMEPSPoints>2286</ssip:NumberOFMEPSPoints>
    </ssip:SurfaceInformation>
    <phase:SSIPs phase:units="MOLAR"><phase:SSIP ssip:value="2.9387503903833574" ssip:nearestAtomID="a3" cml:x3="1.509794550767402" cml:y3="-0.8784982637871289" cml:z3="-1.6986589692899999E-4" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="2.9275582734233065" ssip:nearestAtomID="a2" cml:x3="-1.4603207114038939" cml:y3="-0.790973405157027" cml:z3="0.12362427220238399" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.263074063139137" ssip:nearestAtomID="a1" cml:x3="0.156188781751346" cml:y3="1.6969169177862917" cml:z3="1.172598045109361" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.607684490829715" ssip:nearestAtomID="a1" cml:x3="0.06855491260794999" cml:y3="1.9788577934582507" cml:z3="-0.77762067563301" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP></phase:SSIPs></phase:Molecule>
  </phase:Molecules>
</phase:Solvent>
"""
        solventxmlcreator.append_molecule_element_list_to_solvent(
            self.solvent_element, [self.molecule_element]
        )
        self.assertMultiLineEqual(
            expected_xml, etree.tounicode(self.solvent_element, pretty_print=True)
        )

    def test_append_molecule_element_to_solvent(self):
        """test to see if molecule is appended to solvent.
        """
        expected_xml = """<phase:Solvent xmlns:phase="http://www-hunter.ch.cam.ac.uk/PhaseSchema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:cml="http://www.xml-cml.org/schema">
  <phase:Molecule phase:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N"><cml:molecule ssip:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" cml:id="XLYOFNOQVPJJNP-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="O" cml:id="a1" cml:x3="0.0569236794019" cml:y3="0.53497425302" cml:z3="-9.99862395488E-5"/>
            <cml:atom cml:elementType="H" cml:id="a2" cml:x3="-0.735721920018" cml:y3="-0.0219270863225" cml:z3="-1.029173866E-4"/>
            <cml:atom cml:elementType="H" cml:id="a3" cml:x3="0.785198240616" cml:y3="-0.103847166698" cml:z3="-1.97096373822E-4"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a3" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    <ssip:SurfaceInformation>
        <ssip:TotalSurfaceArea>37.48483410829331</ssip:TotalSurfaceArea>
        <ssip:NegativeSurfaceArea>18.664358683092974</ssip:NegativeSurfaceArea>
        <ssip:PositiveSurfaceArea>18.82047542519977</ssip:PositiveSurfaceArea>
        <ssip:ElectronDensityIsosurface>0.002</ssip:ElectronDensityIsosurface>
        <ssip:NumberOFMEPSPoints>2286</ssip:NumberOFMEPSPoints>
    </ssip:SurfaceInformation>
    <phase:SSIPs phase:units="MOLAR"><phase:SSIP ssip:value="2.9387503903833574" ssip:nearestAtomID="a3" cml:x3="1.509794550767402" cml:y3="-0.8784982637871289" cml:z3="-1.6986589692899999E-4" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="2.9275582734233065" ssip:nearestAtomID="a2" cml:x3="-1.4603207114038939" cml:y3="-0.790973405157027" cml:z3="0.12362427220238399" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.263074063139137" ssip:nearestAtomID="a1" cml:x3="0.156188781751346" cml:y3="1.6969169177862917" cml:z3="1.172598045109361" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.607684490829715" ssip:nearestAtomID="a1" cml:x3="0.06855491260794999" cml:y3="1.9788577934582507" cml:z3="-0.77762067563301" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP></phase:SSIPs></phase:Molecule>
</phase:Solvent>
"""
        solventxmlcreator.append_child_element_to_parent_element(
            self.solvent_element, self.molecule_element
        )
        self.assertMultiLineEqual(
            expected_xml, etree.tounicode(self.solvent_element, pretty_print=True)
        )

    def test_append_solvent_name(self):
        """Test to see if solvent name is appended.
        """
        expected_xml = (
            '<phase:Solvent xmlns:phase="http://www-hunter.ch.cam.ac.uk/Pha'
            + 'seSchema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk'
            + '/SSIP" xmlns:cml="http://www.xml-cml.org/schema" phase:solventName="water"/>'
        )
        solventxmlcreator.append_solvent_name(self.solvent_element, "water")
        self.assertEqual(expected_xml, etree.tounicode(self.solvent_element))

    def test_append_solvent_id(self):
        """Test to see if solvent id is appended.
        """
        expected_xml = (
            '<phase:Solvent xmlns:phase="http://www-hunter.ch.cam.ac.uk/Pha'
            + 'seSchema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk'
            + '/SSIP" xmlns:cml="http://www.xml-cml.org/schema" phase:solventID="water"/>'
        )
        solventxmlcreator.append_solvent_id(self.solvent_element, "water")
        self.assertEqual(expected_xml, etree.tounicode(self.solvent_element))

    def test_create_solvent_element(self):
        """Test to see if solvent element is created as expected.
        """
        expected_xml = (
            '<phase:Solvent xmlns:phase="http://www-hunter.ch.cam.ac.uk/Phase'
            + 'Schema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/'
            + 'SSIP" xmlns:cml="http://www.xml-cml.org/schema"/>'
        )
        actual_xml_element = solventxmlcreator.create_solvent_element()
        self.assertEqual(expected_xml, etree.tounicode(actual_xml_element))
