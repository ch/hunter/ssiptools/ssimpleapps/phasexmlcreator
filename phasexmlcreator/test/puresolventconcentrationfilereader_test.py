# -*- coding: utf-8 -*-
#    phasexmlcreator provides utilites to generate XML input for phasetransfer calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlcreator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Tests for the puresolventconcentrationfilereader.

@author: mark
"""

import logging
import unittest
import pathlib
import phasexmlcreator.puresolventconcentrationfilereader as puresolventconcentrationfilereader

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)


class PureSolventConcentrationFileReaderTestCase(unittest.TestCase):
    """Test case for the reading of the pure solvent concentrations.
    """

    def setUp(self):
        """set up for tests.
        """
        self.maxDiff = None
        parent_directory = pathlib.Path(__file__).parents[0]
        self.solvent_filename = (
            (parent_directory / "resources" / "concentration_examples.csv")
            .absolute()
            .as_posix()
        )
        self.solvent_temperature_filename = (
            (parent_directory / "resources" / "concentration_tempexamples.csv")
            .absolute()
            .as_posix()
        )
        self.title_line = "concentration\tSolvent\tStdInChIKey\n"
        self.entry_line_list = [
            "9.3\t1-bromobutane\tMPPPKRYCTPRNTB-UHFFFAOYSA-N\n",
            "10.9\t1-butanol\tLRHPLDYGYMQRHN-UHFFFAOYSA-N\n",
        ]

    def tearDown(self):
        """clean up after tests.
        """
        del self.solvent_filename
        del self.title_line
        del self.entry_line_list

    def test_read_file_to_information_dict(self):
        """Test to see if file is read in as expected.
        """
        expected_dictionary = {
            "MPPPKRYCTPRNTB-UHFFFAOYSA-N": {
                "concentration": "9.3",
                "Solvent": "1-bromobutane",
                "StdInChIKey": "MPPPKRYCTPRNTB-UHFFFAOYSA-N",
            },
            "LRHPLDYGYMQRHN-UHFFFAOYSA-N": {
                "concentration": "10.9",
                "Solvent": "1-butanol",
                "StdInChIKey": "LRHPLDYGYMQRHN-UHFFFAOYSA-N",
            },
            "XLYOFNOQVPJJNP-UHFFFAOYSA-N": {
                "concentration": "55.35",
                "Solvent": "water",
                "StdInChIKey": "XLYOFNOQVPJJNP-UHFFFAOYSA-N",
            },
        }
        actual_dictionary = puresolventconcentrationfilereader.read_file_to_information_dict(
            self.solvent_filename
        )
        self.assertDictEqual(expected_dictionary, actual_dictionary)
        expected_temp_dict = {"XLYOFNOQVPJJNP-UHFFFAOYSA-N": {"273.1500":{
                "concentration": "55.35",
                "Solvent": "water",
                "StdInChIKey": "XLYOFNOQVPJJNP-UHFFFAOYSA-N",
                "temperature":"273.15",
            },
            "303.1500":{
                "concentration": "50.35",
                "Solvent": "water",
                "StdInChIKey": "XLYOFNOQVPJJNP-UHFFFAOYSA-N",
                "temperature":"303.15",
            },
            "313.1500":{
                "concentration": "45.35",
                "Solvent": "water",
                "StdInChIKey": "XLYOFNOQVPJJNP-UHFFFAOYSA-N",
                "temperature":"313.15",
            },
            'Solvent': 'water',
            },}
        actual_dictionary  = puresolventconcentrationfilereader.read_file_to_information_dict(self.solvent_temperature_filename, True)
        self.assertDictEqual(expected_temp_dict, actual_dictionary)
    def test_read_entry_lines_to_dictionary(self):
        """Test to see if the expected dictionary is produced.
        """
        expected_dictionary = {
            "MPPPKRYCTPRNTB-UHFFFAOYSA-N": {
                "concentration": "9.3",
                "Solvent": "1-bromobutane",
                "StdInChIKey": "MPPPKRYCTPRNTB-UHFFFAOYSA-N",
            },
            "LRHPLDYGYMQRHN-UHFFFAOYSA-N": {
                "concentration": "10.9",
                "Solvent": "1-butanol",
                "StdInChIKey": "LRHPLDYGYMQRHN-UHFFFAOYSA-N",
            },
        }
        actual_dictionary = puresolventconcentrationfilereader.read_entry_lines_to_dictionary(
            {0: "concentration", 1: "Solvent", 2: "StdInChIKey"}, self.entry_line_list, False
        )
        self.assertDictEqual(expected_dictionary, actual_dictionary)

    def test_read_entry_line_to_dict(self):
        """Test to see if the line is read to the expected dictionary.
        """
        expected_dictionary = {
            "concentration": "9.3",
            "Solvent": "1-bromobutane",
            "StdInChIKey": "MPPPKRYCTPRNTB-UHFFFAOYSA-N",
        }
        actual_dictionary = puresolventconcentrationfilereader.read_entry_line_to_dict(
            {0: "concentration", 1: "Solvent", 2: "StdInChIKey"},
            self.entry_line_list[0],
        )
        self.assertDictEqual(expected_dictionary, actual_dictionary)

    def test_read_titleline(self):
        """test to see if title line is read as expected.
        """
        expected_dictionary = {0: "concentration", 1: "Solvent", 2: "StdInChIKey"}
        actual_dictionary = puresolventconcentrationfilereader.read_titleline(
            self.title_line
        )
        self.assertDictEqual(expected_dictionary, actual_dictionary)
