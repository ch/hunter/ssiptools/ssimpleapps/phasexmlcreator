# -*- coding: utf-8 -*-
#    phasexmlcreator provides utilites to generate XML input for phasetransfer calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlcreator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
script to test the solventconcentrationreader

@author: mark
"""

import logging
import unittest
import pathlib
from lxml import etree
import phasexmlcreator.solventconcentrationreader as solventconcentrationreader

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)


class SolventConcentrationReaderTestCase(unittest.TestCase):
    """Test case for the solventconcentrationreader script.
    """

    def setUp(self):
        """set up for tests.
        """
        self.maxDiff=None
        parent_directory = pathlib.Path(__file__).parents[0]
        self.name_inchikey_map = {"Ethyl heptanoate":"TVQGDYNRXLTQAP-UHFFFAOYSA-N"}
        self.solvent_filename = (
            (parent_directory / "resources" / "concentration_examples.csv")
            .absolute()
            .as_posix()
        )
        self.ssip_filename_list = [
            (parent_directory / "resources/XLYOFNOQVPJJNP-UHFFFAOYSA-N_ssip.xml")
            .absolute()
            .as_posix()
        ]
        self.ssip_filename_list2 = [
            (parent_directory / "resources/XLYOFNOQVPJJNP-UHFFFAOYSA-N_ssip.xml")
            .absolute()
            .as_posix(),
            (parent_directory / "resources/TVQGDYNRXLTQAP-UHFFFAOYSA-N_ssip.xml")
            .absolute()
            .as_posix(),
        ]
        
        self.expected_xml = """<ssip:SSIPMolecule xmlns:cml="http://www.xml-cml.org/schema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ssip:ssipSoftwareVersion="2.2.0-SNAPSHOT" ssip:parameterVersion="1.0.0" xsi:schemaLocation="http://www.xml-cml.org/schema http://www-hunter.ch.cam.ac.uk/schema/cmlschema.xsd http://www-hunter.ch.cam.ac.uk/SSIP http://www-hunter.ch.cam.ac.uk/schema/SSIP.xsd">
    <cml:molecule ssip:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" cml:id="XLYOFNOQVPJJNP-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="O" cml:id="a1" cml:x3="0.0569236794019" cml:y3="0.53497425302" cml:z3="-9.99862395488E-5"/>
            <cml:atom cml:elementType="H" cml:id="a2" cml:x3="-0.735721920018" cml:y3="-0.0219270863225" cml:z3="-1.029173866E-4"/>
            <cml:atom cml:elementType="H" cml:id="a3" cml:x3="0.785198240616" cml:y3="-0.103847166698" cml:z3="-1.97096373822E-4"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a3" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    <ssip:SurfaceInformation>
        <ssip:TotalSurfaceArea>37.48483410829331</ssip:TotalSurfaceArea>
        <ssip:NegativeSurfaceArea>18.664358683092974</ssip:NegativeSurfaceArea>
        <ssip:PositiveSurfaceArea>18.82047542519977</ssip:PositiveSurfaceArea>
        <ssip:ElectronDensityIsosurface>0.002</ssip:ElectronDensityIsosurface>
        <ssip:NumberOFMEPSPoints>2286</ssip:NumberOFMEPSPoints>
    </ssip:SurfaceInformation>
    <ssip:SSIPs>
        <ssip:SSIP ssip:value="2.9387503903833574" ssip:nearestAtomID="a3" cml:x3="1.509794550767402" cml:y3="-0.8784982637871289" cml:z3="-1.6986589692899999E-4"/>
        <ssip:SSIP ssip:value="2.9275582734233065" ssip:nearestAtomID="a2" cml:x3="-1.4603207114038939" cml:y3="-0.790973405157027" cml:z3="0.12362427220238399"/>
        <ssip:SSIP ssip:value="-6.263074063139137" ssip:nearestAtomID="a1" cml:x3="0.156188781751346" cml:y3="1.6969169177862917" cml:z3="1.172598045109361"/>
        <ssip:SSIP ssip:value="-6.607684490829715" ssip:nearestAtomID="a1" cml:x3="0.06855491260794999" cml:y3="1.9788577934582507" cml:z3="-0.77762067563301"/>
    </ssip:SSIPs>
</ssip:SSIPMolecule>"""
        self.expected_xml2 = """<ssip:SSIPMolecule xmlns:cml="http://www.xml-cml.org/schema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ssip:ssipSoftwareVersion="6.0.1-SNAPSHOT" ssip:parameterVersion="1.0.0" xsi:schemaLocation="http://www.xml-cml.org/schema http://www-hunter.ch.cam.ac.uk/schema/cmlschema.xsd http://www-hunter.ch.cam.ac.uk/SSIP http://www-hunter.ch.cam.ac.uk/schema/SSIP.xsd">
    <cml:molecule ssip:stdInChIKey="TVQGDYNRXLTQAP-UHFFFAOYSA-N" cml:id="TVQGDYNRXLTQAP-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="C" cml:id="a1" cml:formalCharge="0" cml:x3="-4.77793657293563" cml:y3="-1.4241280534948606" cml:z3="-0.3630976931561518"/>
            <cml:atom cml:elementType="C" cml:id="a10" cml:formalCharge="0" cml:x3="4.225142123503188" cml:y3="0.33453515568287406" cml:z3="-0.5742736810267585"/>
            <cml:atom cml:elementType="C" cml:id="a11" cml:formalCharge="0" cml:x3="4.752915111137654" cml:y3="-1.0825143514329492" cml:z3="-0.4501635972674916"/>
            <cml:atom cml:elementType="H" cml:id="a12" cml:formalCharge="0" cml:x3="-4.898955882553757" cml:y3="-1.1163097063502652" cml:z3="-1.4090511324659702"/>
            <cml:atom cml:elementType="H" cml:id="a13" cml:formalCharge="0" cml:x3="-5.771704123049405" cml:y3="-1.4302453370581245" cml:z3="0.09897178617213459"/>
            <cml:atom cml:elementType="H" cml:id="a14" cml:formalCharge="0" cml:x3="-4.404400525467168" cml:y3="-2.4555568670888395" cml:z3="-0.363883585760838"/>
            <cml:atom cml:elementType="H" cml:id="a15" cml:formalCharge="0" cml:x3="-4.238761276661798" cml:y3="0.5282306187458604" cml:z3="0.3951014463604072"/>
            <cml:atom cml:elementType="H" cml:id="a16" cml:formalCharge="0" cml:x3="-3.7469085203939025" cml:y3="-0.8001510110151278" cml:z3="1.4333084764099535"/>
            <cml:atom cml:elementType="H" cml:id="a17" cml:formalCharge="0" cml:x3="-1.9967138707741572" cml:y3="-1.4646557107583962" cml:z3="-0.2489771631543417"/>
            <cml:atom cml:elementType="H" cml:id="a18" cml:formalCharge="0" cml:x3="-2.4879183015390054" cml:y3="-0.13188263194708386" cml:z3="-1.283508732120002"/>
            <cml:atom cml:elementType="H" cml:id="a19" cml:formalCharge="0" cml:x3="-1.373311730851992" cml:y3="0.16654244251848518" cml:z3="1.5619637850918429"/>
            <cml:atom cml:elementType="C" cml:id="a2" cml:formalCharge="0" cml:x3="-3.8203925594557835" cml:y3="-0.48823681576529093" cml:z3="0.3817323772707575"/>
            <cml:atom cml:elementType="H" cml:id="a20" cml:formalCharge="0" cml:x3="-1.8702159319058245" cml:y3="1.5018581340031059" cml:z3="0.5322864237244379"/>
            <cml:atom cml:elementType="H" cml:id="a21" cml:formalCharge="0" cml:x3="0.380647838839953" cml:y3="-0.48093225015259955" cml:z3="-0.13036077411943106"/>
            <cml:atom cml:elementType="H" cml:id="a22" cml:formalCharge="0" cml:x3="-0.12323769763727176" cml:y3="0.8586046709578306" cml:z3="-1.1548945211492472"/>
            <cml:atom cml:elementType="H" cml:id="a23" cml:formalCharge="0" cml:x3="0.5039118772345774" cml:y3="2.4802087006854823" cml:z3="0.6962726455185527"/>
            <cml:atom cml:elementType="H" cml:id="a24" cml:formalCharge="0" cml:x3="1.0315017769301802" cml:y3="1.1011564124916107" cml:z3="1.6798246242058448"/>
            <cml:atom cml:elementType="H" cml:id="a25" cml:formalCharge="0" cml:x3="4.870947781347329" cml:y3="1.0596044299690874" cml:z3="-0.068102978221173"/>
            <cml:atom cml:elementType="H" cml:id="a26" cml:formalCharge="0" cml:x3="4.133496369295323" cml:y3="0.650239312291525" cml:z3="-1.6186590878352158"/>
            <cml:atom cml:elementType="H" cml:id="a27" cml:formalCharge="0" cml:x3="4.836683401906629" cml:y3="-1.3784775096277864" cml:z3="0.600536299347343"/>
            <cml:atom cml:elementType="H" cml:id="a28" cml:formalCharge="0" cml:x3="5.745713893957405" cml:y3="-1.1514603034529023" cml:z3="-0.9079420879278576"/>
            <cml:atom cml:elementType="H" cml:id="a29" cml:formalCharge="0" cml:x3="4.090209431845274" cml:y3="-1.7925164289264108" cml:z3="-0.9556888006688495"/>
            <cml:atom cml:elementType="C" cml:id="a3" cml:formalCharge="0" cml:x3="-2.4149956777423665" cml:y3="-0.44725040070371097" cml:z3="-0.2320459300476804"/>
            <cml:atom cml:elementType="C" cml:id="a4" cml:formalCharge="0" cml:x3="-1.4504558585212788" cml:y3="0.48546240049236566" cml:z3="0.5118802441624547"/>
            <cml:atom cml:elementType="C" cml:id="a5" cml:formalCharge="0" cml:x3="-0.04910680601989368" cml:y3="0.5277887365813221" cml:z3="-0.1096942208664376"/>
            <cml:atom cml:elementType="C" cml:id="a6" cml:formalCharge="0" cml:x3="0.9087361668116533" cml:y3="1.4651179577988453" cml:z3="0.6516228429727999"/>
            <cml:atom cml:elementType="C" cml:id="a7" cml:formalCharge="0" cml:x3="2.276640106664835" cml:y3="1.5601577012313168" cml:z3="0.006347533208713589"/>
            <cml:atom cml:elementType="O" cml:id="a8" cml:formalCharge="0" cml:x3="2.7514872582418173" cml:y3="2.557587257644238" cml:z3="-0.4934254307084001"/>
            <cml:atom cml:elementType="O" cml:id="a9" cml:formalCharge="0" cml:x3="2.916882197793633" cml:y3="0.3670234466798665" cml:z3="0.040120932050534096"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a12" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a13" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a14" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a10 a11" cml:order="1"/>
            <cml:bond cml:atomRefs2="a10 a25" cml:order="1"/>
            <cml:bond cml:atomRefs2="a10 a26" cml:order="1"/>
            <cml:bond cml:atomRefs2="a11 a27" cml:order="1"/>
            <cml:bond cml:atomRefs2="a11 a28" cml:order="1"/>
            <cml:bond cml:atomRefs2="a11 a29" cml:order="1"/>
            <cml:bond cml:atomRefs2="a2 a15" cml:order="1"/>
            <cml:bond cml:atomRefs2="a2 a16" cml:order="1"/>
            <cml:bond cml:atomRefs2="a2 a3" cml:order="1"/>
            <cml:bond cml:atomRefs2="a3 a17" cml:order="1"/>
            <cml:bond cml:atomRefs2="a3 a18" cml:order="1"/>
            <cml:bond cml:atomRefs2="a3 a4" cml:order="1"/>
            <cml:bond cml:atomRefs2="a4 a19" cml:order="1"/>
            <cml:bond cml:atomRefs2="a4 a20" cml:order="1"/>
            <cml:bond cml:atomRefs2="a4 a5" cml:order="1"/>
            <cml:bond cml:atomRefs2="a5 a21" cml:order="1"/>
            <cml:bond cml:atomRefs2="a5 a22" cml:order="1"/>
            <cml:bond cml:atomRefs2="a5 a6" cml:order="1"/>
            <cml:bond cml:atomRefs2="a6 a23" cml:order="1"/>
            <cml:bond cml:atomRefs2="a6 a24" cml:order="1"/>
            <cml:bond cml:atomRefs2="a6 a7" cml:order="1"/>
            <cml:bond cml:atomRefs2="a7 a8" cml:order="2"/>
            <cml:bond cml:atomRefs2="a7 a9" cml:order="1"/>
            <cml:bond cml:atomRefs2="a9 a10" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    <ssip:SurfaceInformation>
        <ssip:Surfaces>
            <ssip:Surface>
                <ssip:TotalSurfaceArea ssip:unit="Å^2">225.01297702423483</ssip:TotalSurfaceArea>
                <ssip:NegativeSurfaceArea ssip:unit="Å^2">38.36229972474354</ssip:NegativeSurfaceArea>
                <ssip:PositiveSurfaceArea ssip:unit="Å^2">186.6375106328303</ssip:PositiveSurfaceArea>
                <ssip:ElectronDensityIsosurface ssip:unit="e bohr^-3">0.002</ssip:ElectronDensityIsosurface>
                <ssip:NumberOFMEPSPoints>17829</ssip:NumberOFMEPSPoints>
                <ssip:VdWVolume ssip:unit="Å^3">198.68006374732028</ssip:VdWVolume>
                <ssip:ElectrostaticPotentialMax ssip:unit="kJ mol^-1">74.78998217532</ssip:ElectrostaticPotentialMax>
                <ssip:ElectrostaticPotentialMin ssip:unit="kJ mol^-1">-172.74474749790002</ssip:ElectrostaticPotentialMin>
            </ssip:Surface>
        </ssip:Surfaces>
    </ssip:SurfaceInformation>
    <ssip:SSIPs>
        <ssip:SSIP ssip:value="0.7052382353065448" ssip:nearestAtomID="a28" cml:x3="6.835506352809266" cml:y3="-1.0920313381263598" cml:z3="-1.38902677735512"/>
        <ssip:SSIP ssip:value="0.6434223074737937" ssip:nearestAtomID="a24" cml:x3="1.129916724914017" cml:y3="0.666501920178994" cml:z3="2.7895150047463306"/>
        <ssip:SSIP ssip:value="0.593850816369865" ssip:nearestAtomID="a7" cml:x3="2.7544543661138357" cml:y3="1.721621557655857" cml:z3="1.5732074480468188"/>
        <ssip:SSIP ssip:value="0.5831168262332395" ssip:nearestAtomID="a29" cml:x3="3.503656635943799" cml:y3="-2.6316963570680647" cml:z3="-1.568809985107629"/>
        <ssip:SSIP ssip:value="0.5787300150724145" ssip:nearestAtomID="a27" cml:x3="5.118901924651178" cml:y3="-1.5975040922574049" cml:z3="1.7384933158857239"/>
        <ssip:SSIP ssip:value="0.5551329481435678" ssip:nearestAtomID="a25" cml:x3="5.87739654697358" cml:y3="1.369915011830236" cml:z3="0.5123012573521409"/>
        <ssip:SSIP ssip:value="0.5546574072668179" ssip:nearestAtomID="a26" cml:x3="4.382819546355671" cml:y3="0.5028914275104229" cml:z3="-2.7883524023302777"/>
        <ssip:SSIP ssip:value="0.5116489749462008" ssip:nearestAtomID="a7" cml:x3="1.632575843612129" cml:y3="0.954199185965575" cml:z3="-1.4145722798733458"/>
        <ssip:SSIP ssip:value="0.47342451173253985" ssip:nearestAtomID="a23" cml:x3="-0.27674382590953" cml:y3="3.3866952344835735" cml:z3="0.748312193697145"/>
        <ssip:SSIP ssip:value="0.4140225171023405" ssip:nearestAtomID="a14" cml:x3="-3.930124814330891" cml:y3="-3.5539776880829557" cml:z3="-0.22445158851784797"/>
        <ssip:SSIP ssip:value="0.4048241866213803" ssip:nearestAtomID="a13" cml:x3="-6.782549997970089" cml:y3="-1.439696028124119" cml:z3="0.7527345279670379"/>
        <ssip:SSIP ssip:value="0.4044343044287664" ssip:nearestAtomID="a12" cml:x3="-4.936307727351981" cml:y3="-0.740324792300739" cml:z3="-2.553179153570441"/>
        <ssip:SSIP ssip:value="0.388028461256277" ssip:nearestAtomID="a21" cml:x3="0.33867026429650593" cml:y3="-1.686522818261434" cml:z3="0.010440137945520999"/>
        <ssip:SSIP ssip:value="0.38612290135942556" ssip:nearestAtomID="a18" cml:x3="-2.593178074290604" cml:y3="0.31479537435337296" cml:z3="-2.4097816442819227"/>
        <ssip:SSIP ssip:value="0.3812905091620159" ssip:nearestAtomID="a16" cml:x3="-3.5095648999288835" cml:y3="-1.2832304866715458" cml:z3="2.5267594502368667"/>
        <ssip:SSIP ssip:value="0.37179112909992346" ssip:nearestAtomID="a15" cml:x3="-4.736958719994946" cml:y3="1.6336950534937638" cml:z3="0.334435787950008"/>
        <ssip:SSIP ssip:value="0.0" ssip:nearestAtomID="a5" cml:x3="-3.4482758545114126E-6" cml:y3="-6.89655174248265E-6" cml:z3="-0.06116551724138171"/>
        <ssip:SSIP ssip:value="0.0" ssip:nearestAtomID="a5" cml:x3="-3.4482758545114126E-6" cml:y3="-6.89655174248265E-6" cml:z3="-0.06116551724138171"/>
        <ssip:SSIP ssip:value="0.0" ssip:nearestAtomID="a5" cml:x3="-3.4482758545114126E-6" cml:y3="-6.89655174248265E-6" cml:z3="-0.06116551724138171"/>
        <ssip:SSIP ssip:value="0.0" ssip:nearestAtomID="a5" cml:x3="-3.4482758545114126E-6" cml:y3="-6.89655174248265E-6" cml:z3="-0.06116551724138171"/>
        <ssip:SSIP ssip:value="-0.04832232508329189" ssip:nearestAtomID="a13" cml:x3="-5.903387087558216" cml:y3="-2.489844045346627" cml:z3="-1.164482582818697"/>
        <ssip:SSIP ssip:value="-2.011946765898198" ssip:nearestAtomID="a9" cml:x3="2.3607449053260887" cml:y3="-0.965254757051683" cml:z3="0.806982604471024"/>
        <ssip:SSIP ssip:value="-5.143140662614065" ssip:nearestAtomID="a8" cml:x3="3.9432383557383597" cml:y3="3.2163747742976825" cml:z3="-1.326987095036858"/>
        <ssip:SSIP ssip:value="-5.694569321748074" ssip:nearestAtomID="a8" cml:x3="2.5365772757375638" cml:y3="4.1163909725608985" cml:z3="-0.80307410130991"/>
    </ssip:SSIPs>
</ssip:SSIPMolecule>"""

    def tearDown(self):
        """clean up after tests.
        """
        del self.solvent_filename
        del self.ssip_filename_list
        del self.expected_xml

    def test_read_and_merge_solvent_information(self):
        """Test to see if expected dictionary is produced from the information read in.
        """
        expected_key_list = ["solvent_id", "solvent_name", "ssip_info_list"]
        expected_keys = [
            "concentration_value",
            "inchikey",
            "mole_fraction",
            "ssip_molecule",
        ]
        actual_dictionary = solventconcentrationreader.read_and_merge_solvent_information(
            self.solvent_filename, self.ssip_filename_list
        )
        self.assertEqual(1, len(actual_dictionary))
        for inchikey, solvent_dict in actual_dictionary.items():
            self.assertEqual("XLYOFNOQVPJJNP-UHFFFAOYSA-N", inchikey)
            self.assertListEqual(expected_key_list, sorted(solvent_dict.keys()))
            self.assertEqual("water", solvent_dict["solvent_id"])
            self.assertEqual("water", solvent_dict["solvent_name"])
            self.assertEqual(1, len(solvent_dict["ssip_info_list"]))
            for information_dict in solvent_dict["ssip_info_list"]:
                self.assertListEqual(expected_keys, sorted(information_dict.keys()))
                self.assertEqual(
                    "XLYOFNOQVPJJNP-UHFFFAOYSA-N", information_dict["inchikey"]
                )
                self.assertAlmostEqual(55.35, information_dict["concentration_value"])
                self.assertAlmostEqual(1.0, information_dict["mole_fraction"])
                self.assertMultiLineEqual(
                    self.expected_xml,
                    etree.tounicode(information_dict["ssip_molecule"]),
                )
        
        actual_dictionary2 = solventconcentrationreader.read_and_merge_solvent_information(
            self.solvent_filename, self.ssip_filename_list2, name_inchikey_map=self.name_inchikey_map
        )
        self.assertEqual(2, len(actual_dictionary2))
        for inchikey, solvent_dict in actual_dictionary2.items():
            if inchikey == "XLYOFNOQVPJJNP-UHFFFAOYSA-N":
                self.assertListEqual(expected_key_list, sorted(solvent_dict.keys()))
                self.assertEqual("water", solvent_dict["solvent_id"])
                self.assertEqual("water", solvent_dict["solvent_name"])
                self.assertEqual(1, len(solvent_dict["ssip_info_list"]))
                for information_dict in solvent_dict["ssip_info_list"]:
                    self.assertListEqual(expected_keys, sorted(information_dict.keys()))
                    self.assertEqual(
                    "XLYOFNOQVPJJNP-UHFFFAOYSA-N", information_dict["inchikey"]
                        )
                    self.assertAlmostEqual(55.35, information_dict["concentration_value"])
                    self.assertAlmostEqual(1.0, information_dict["mole_fraction"])
                    self.assertMultiLineEqual(
                        self.expected_xml,
                        etree.tounicode(information_dict["ssip_molecule"]),
                        )
            elif inchikey == "TVQGDYNRXLTQAP-UHFFFAOYSA-N":
                self.assertListEqual(expected_key_list, sorted(solvent_dict.keys()))
                self.assertEqual("Ethyl heptanoate", solvent_dict["solvent_id"])
                self.assertEqual("Ethyl heptanoate", solvent_dict["solvent_name"])
                self.assertEqual(1, len(solvent_dict["ssip_info_list"]))
                for information_dict in solvent_dict["ssip_info_list"]:
                    self.assertListEqual(expected_keys, sorted(information_dict.keys()))
                    self.assertEqual(
                    "TVQGDYNRXLTQAP-UHFFFAOYSA-N", information_dict["inchikey"]
                        )
                    self.assertAlmostEqual(10.0, information_dict["concentration_value"])
                    self.assertAlmostEqual(1.0, information_dict["mole_fraction"])
                    self.assertMultiLineEqual(
                        self.expected_xml2,
                        etree.tounicode(information_dict["ssip_molecule"]),
                        )
        
        

    def test_append_ssip_list_to_concentrations(self):
        """Test to see if expected dictionary is produced.
        """

        expected_key_list = ["solvent_id", "solvent_name", "ssip_info_list"]
        expected_keys = [
            "concentration_value",
            "inchikey",
            "mole_fraction",
            "ssip_molecule",
        ]
        solvent_information_dict = solventconcentrationreader.read_solvent_information(
            self.solvent_filename
        )
        ssip_list_dict = solventconcentrationreader.read_ssip_lists(
            self.ssip_filename_list
        )
        actual_dictionary = solventconcentrationreader.append_ssip_list_to_concentrations(
            solvent_information_dict, ssip_list_dict
        )
        self.assertEqual(1, len(actual_dictionary))
        for inchikey, solvent_dict in actual_dictionary.items():
            self.assertEqual("XLYOFNOQVPJJNP-UHFFFAOYSA-N", inchikey)
            self.assertListEqual(expected_key_list, sorted(solvent_dict.keys()))
            self.assertEqual("water", solvent_dict["solvent_id"])
            self.assertEqual("water", solvent_dict["solvent_name"])
            self.assertEqual(1, len(solvent_dict["ssip_info_list"]))
            for information_dict in solvent_dict["ssip_info_list"]:
                self.assertListEqual(expected_keys, sorted(information_dict.keys()))
                self.assertEqual(
                    "XLYOFNOQVPJJNP-UHFFFAOYSA-N", information_dict["inchikey"]
                )
                self.assertAlmostEqual(55.35, information_dict["concentration_value"])
                self.assertAlmostEqual(1.0, information_dict["mole_fraction"])
                self.assertMultiLineEqual(
                    self.expected_xml,
                    etree.tounicode(information_dict["ssip_molecule"]),
                )

    def test_read_solvent_information(self):
        """Test to see if the expected solvent information is read in.
        """
        expected_dictionary = {
            "MPPPKRYCTPRNTB-UHFFFAOYSA-N": {
                "concentration": "9.3",
                "Solvent": "1-bromobutane",
                "StdInChIKey": "MPPPKRYCTPRNTB-UHFFFAOYSA-N",
            },
            "LRHPLDYGYMQRHN-UHFFFAOYSA-N": {
                "concentration": "10.9",
                "Solvent": "1-butanol",
                "StdInChIKey": "LRHPLDYGYMQRHN-UHFFFAOYSA-N",
            },
            "XLYOFNOQVPJJNP-UHFFFAOYSA-N": {
                "concentration": "55.35",
                "Solvent": "water",
                "StdInChIKey": "XLYOFNOQVPJJNP-UHFFFAOYSA-N",
            },
        }
        actual_dictionary = solventconcentrationreader.read_solvent_information(
            self.solvent_filename
        )
        self.assertDictEqual(expected_dictionary, actual_dictionary)

    def test_read_ssip_lists(self):
        """Test to see if expected SSIPLists are read in.
        """
        expected_keys = ["inchikey", "ssip_molecule"]
        actual_dict = solventconcentrationreader.read_ssip_lists(
            self.ssip_filename_list
        )
        self.assertEqual(1, len(actual_dict))
        for inchikey, information_dict in actual_dict.items():
            self.assertEqual("XLYOFNOQVPJJNP-UHFFFAOYSA-N", inchikey)
            self.assertListEqual(expected_keys, sorted(information_dict.keys()))
            self.assertEqual(
                "XLYOFNOQVPJJNP-UHFFFAOYSA-N", information_dict["inchikey"]
            )
            self.assertMultiLineEqual(
                self.expected_xml, etree.tounicode(information_dict["ssip_molecule"])
            )
