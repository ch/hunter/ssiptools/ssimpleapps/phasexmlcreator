# -*- coding: utf-8 -*-
#    phasexmlcreator provides utilites to generate XML input for phasetransfer calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlcreator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for running tests.

@author: mark
"""

import logging
import unittest
import sys
from phasexmlcreator.test.moleculexmlcreator_test import MoleculeXMLCreatorTestCase
from phasexmlcreator.test.solutexmlcreator_test import SoluteXMLCreatorTestCase
from phasexmlcreator.test.solventxmlcreator_test import SolventXMLCreatorTestCase
from phasexmlcreator.test.ssipfilereader_test import SSIPFileReaderTestCase
from phasexmlcreator.test.solutexmlmaker_test import SoluteXMLMakerTestCase
from phasexmlcreator.test.puresolventconcentrationfilereader_test import (
    PureSolventConcentrationFileReaderTestCase,
)
from phasexmlcreator.test.solventconcentrationreader_test import (
    SolventConcentrationReaderTestCase,
)
from phasexmlcreator.test.solventxmlmaker_test import SolventXMLMakerTestCase
from phasexmlcreator.test.phasexmlcreator_test import PhaseXMLCreatorTestCase
from phasexmlcreator.test.phasexmlmaker_test import PhaseXMLMakerTestCase
from phasexmlcreator.test.binarysolventmaker_test import BinarySolventMakerTestCase
from phasexmlcreator.test.multisolventmaker_test import MultiSolventMakerTestCase
from phasexmlcreator.test.binaryphasemaker_test import BinaryPhaseMakerTestCase
from phasexmlcreator.test.multicomponentassembler_test import MultiComponentAssemblerTestCase

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)

TESTCASES = [
    MoleculeXMLCreatorTestCase,
    SoluteXMLCreatorTestCase,
    SolventXMLCreatorTestCase,
    SSIPFileReaderTestCase,
    SoluteXMLMakerTestCase,
    PureSolventConcentrationFileReaderTestCase,
    SolventConcentrationReaderTestCase,
    SolventXMLMakerTestCase,
    PhaseXMLCreatorTestCase,
    PhaseXMLMakerTestCase,
    BinarySolventMakerTestCase,
    MultiSolventMakerTestCase,
    BinaryPhaseMakerTestCase,
    MultiComponentAssemblerTestCase,
]


def create_test_suite():
    """Function creates a test suite and then loads all the tests from the
    different test cases.
    """
    LOGGER.info("setting up loader and test suite")
    loader = unittest.TestLoader()
    suite = unittest.TestSuite()
    for test_case in TESTCASES:
        LOGGER.debug("Adding %s", test_case)
        suite.addTests(loader.loadTestsFromTestCase(test_case))
    return suite


def run_tests():
    """Runs test suite. Exits if there is a failure.

    Returns
    --------
    None
    """
    LOGGER.info("calling test suite method")
    suite = create_test_suite()
    LOGGER.info("running test suite")
    ret = (
        not unittest.TextTestRunner(verbosity=2, stream=sys.stderr)
        .run(suite)
        .wasSuccessful()
    )
    if ret:
        sys.exit(ret)


if __name__ == "__main__":
    run_tests()
