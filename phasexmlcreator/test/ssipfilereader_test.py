# -*- coding: utf-8 -*-
#    phasexmlcreator provides utilites to generate XML input for phasetransfer calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlcreator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Tests for the reading of the ssip files.

@author: mark
"""

import logging
import unittest
import pathlib
from lxml import etree
import phasexmlcreator.ssipfilereader as ssipfilereader
import xmlvalidator.xmlvalidation as xmlvalidation

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)


class SSIPFileReaderTestCase(unittest.TestCase):
    """Test case for the reading of SSIPLists.
    """

    def setUp(self):
        """set up for tests
        """
        self.maxDiff = None
        parent_directory = pathlib.Path(__file__).parents[0]
        self.ssip_filename = (
            (parent_directory / "resources/XLYOFNOQVPJJNP-UHFFFAOYSA-N_ssip.xml")
            .absolute()
            .as_posix()
        )
        self.expected_xml = """<ssip:SSIPMolecule xmlns:cml="http://www.xml-cml.org/schema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ssip:ssipSoftwareVersion="2.2.0-SNAPSHOT" ssip:parameterVersion="1.0.0" xsi:schemaLocation="http://www.xml-cml.org/schema http://www-hunter.ch.cam.ac.uk/schema/cmlschema.xsd http://www-hunter.ch.cam.ac.uk/SSIP http://www-hunter.ch.cam.ac.uk/schema/SSIP.xsd">
    <cml:molecule ssip:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" cml:id="XLYOFNOQVPJJNP-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="O" cml:id="a1" cml:x3="0.0569236794019" cml:y3="0.53497425302" cml:z3="-9.99862395488E-5"/>
            <cml:atom cml:elementType="H" cml:id="a2" cml:x3="-0.735721920018" cml:y3="-0.0219270863225" cml:z3="-1.029173866E-4"/>
            <cml:atom cml:elementType="H" cml:id="a3" cml:x3="0.785198240616" cml:y3="-0.103847166698" cml:z3="-1.97096373822E-4"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a3" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    <ssip:SurfaceInformation>
        <ssip:TotalSurfaceArea>37.48483410829331</ssip:TotalSurfaceArea>
        <ssip:NegativeSurfaceArea>18.664358683092974</ssip:NegativeSurfaceArea>
        <ssip:PositiveSurfaceArea>18.82047542519977</ssip:PositiveSurfaceArea>
        <ssip:ElectronDensityIsosurface>0.002</ssip:ElectronDensityIsosurface>
        <ssip:NumberOFMEPSPoints>2286</ssip:NumberOFMEPSPoints>
    </ssip:SurfaceInformation>
    <ssip:SSIPs>
        <ssip:SSIP ssip:value="2.9387503903833574" ssip:nearestAtomID="a3" cml:x3="1.509794550767402" cml:y3="-0.8784982637871289" cml:z3="-1.6986589692899999E-4"/>
        <ssip:SSIP ssip:value="2.9275582734233065" ssip:nearestAtomID="a2" cml:x3="-1.4603207114038939" cml:y3="-0.790973405157027" cml:z3="0.12362427220238399"/>
        <ssip:SSIP ssip:value="-6.263074063139137" ssip:nearestAtomID="a1" cml:x3="0.156188781751346" cml:y3="1.6969169177862917" cml:z3="1.172598045109361"/>
        <ssip:SSIP ssip:value="-6.607684490829715" ssip:nearestAtomID="a1" cml:x3="0.06855491260794999" cml:y3="1.9788577934582507" cml:z3="-0.77762067563301"/>
    </ssip:SSIPs>
</ssip:SSIPMolecule>"""

    def test_extract_ssip_information_from_files(self):
        """Test to see if ssip lists are extracted, along with inchikey.
        """

        expected_keys = ["inchikey", "ssip_molecule"]
        actual_dict = ssipfilereader.extract_ssip_information_from_files(
            [self.ssip_filename]
        )
        self.assertEqual(1, len(actual_dict))
        for inchikey, information_dict in actual_dict.items():
            self.assertEqual("XLYOFNOQVPJJNP-UHFFFAOYSA-N", inchikey)
            self.assertListEqual(expected_keys, sorted(information_dict.keys()))
            self.assertEqual(
                "XLYOFNOQVPJJNP-UHFFFAOYSA-N", information_dict["inchikey"]
            )
            self.assertMultiLineEqual(
                self.expected_xml, etree.tounicode(information_dict["ssip_molecule"])
            )

    def test_extract_ssip_molecule_and_inchikey(self):
        """Test to see if expected SSIPList and inchikey are extracted.
        """
        expected_keys = ["inchikey", "ssip_molecule"]
        actual_dict = ssipfilereader.extract_ssip_molecule_and_inchikey(
            self.ssip_filename
        )
        self.assertListEqual(expected_keys, sorted(actual_dict.keys()))
        self.assertEqual("XLYOFNOQVPJJNP-UHFFFAOYSA-N", actual_dict["inchikey"])
        self.assertMultiLineEqual(
            self.expected_xml, etree.tounicode(actual_dict["ssip_molecule"])
        )

    def test_extract_ssip_molecule_from_file(self):
        """Test to see if SSIPList element is extracted from file.
        """
        actual_element = ssipfilereader.extract_ssip_molecule_from_file(
            self.ssip_filename
        )
        self.assertMultiLineEqual(self.expected_xml, etree.tounicode(actual_element))

    def test_extract_ssip_molecule_in_etree(self):
        """Test to see if expected element is extracted.
        """
        actual_element_tree = xmlvalidation.validate_and_read_xml_file(
            self.ssip_filename, xmlvalidation.SSIP_SCHEMA
        )
        actual_element = ssipfilereader.extract_ssip_molecule_in_etree(
            actual_element_tree
        )
        self.assertMultiLineEqual(self.expected_xml, etree.tounicode(actual_element))

    def test_extract_inchikey_from_filename(self):
        """This extracts an inchikey from a file name.
        """
        expected_inchikey = "XLYOFNOQVPJJNP-UHFFFAOYSA-N"
        actual_inchikey = ssipfilereader.extract_inchikey_from_filename(
            self.ssip_filename
        )
        self.assertEqual(expected_inchikey, actual_inchikey)
