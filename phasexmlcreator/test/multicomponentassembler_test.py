# -*- coding: utf-8 -*-
#    phasexmlcreator provides utilites to generate XML input for phasetransfer calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlcreator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.

"""
Test case for multicomponent assembler methods.

@author: mark
"""

import logging
import unittest
import pathlib
import numpy as np
from lxml import etree
import phasexmlcreator.multicomponentassembler as multiassem

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)

class MultiComponentAssemblerTestCase(unittest.TestCase):
    """Test case for multicomponent assembler methods."""
    
    def setUp(self):
        """Set up variables before tests.

        Returns
        -------
        None.

        """
        self.maxDiff = None
        parent_directory = pathlib.Path(__file__).parents[0]
        self.solvent_filename = (
            (parent_directory / "resources" / "concentration_examples.csv")
            .absolute()
            .as_posix()
        )
        self.ssip_filename_list = [
            (parent_directory / "resources/LRHPLDYGYMQRHN-UHFFFAOYSA-N_ssip.xml")
            .absolute()
            .as_posix(),
            (parent_directory / "resources/XLYOFNOQVPJJNP-UHFFFAOYSA-N_ssip.xml")
            .absolute()
            .as_posix(),
            (parent_directory / "resources/TVQGDYNRXLTQAP-UHFFFAOYSA-N_ssip.xml")
            .absolute()
            .as_posix(),
        ]
        self.name_inchikey_map = {"water":"XLYOFNOQVPJJNP-UHFFFAOYSA-N",
                                  "1-octanol":"LRHPLDYGYMQRHN-UHFFFAOYSA-N",
                                  "Ethyl heptanoate":"TVQGDYNRXLTQAP-UHFFFAOYSA-N"}
        self.solvent_information = multiassem.read_and_merge_solvent_information(
            self.solvent_filename, self.ssip_filename_list, self.name_inchikey_map
        )
        self.expected_water_xml = """<ssip:SSIPMolecule xmlns:cml="http://www.xml-cml.org/schema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ssip:ssipSoftwareVersion="2.2.0-SNAPSHOT" ssip:parameterVersion="1.0.0" xsi:schemaLocation="http://www.xml-cml.org/schema http://www-hunter.ch.cam.ac.uk/schema/cmlschema.xsd http://www-hunter.ch.cam.ac.uk/SSIP http://www-hunter.ch.cam.ac.uk/schema/SSIP.xsd">
    <cml:molecule ssip:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" cml:id="XLYOFNOQVPJJNP-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="O" cml:id="a1" cml:x3="0.0569236794019" cml:y3="0.53497425302" cml:z3="-9.99862395488E-5"/>
            <cml:atom cml:elementType="H" cml:id="a2" cml:x3="-0.735721920018" cml:y3="-0.0219270863225" cml:z3="-1.029173866E-4"/>
            <cml:atom cml:elementType="H" cml:id="a3" cml:x3="0.785198240616" cml:y3="-0.103847166698" cml:z3="-1.97096373822E-4"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a3" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    <ssip:SurfaceInformation>
        <ssip:TotalSurfaceArea>37.48483410829331</ssip:TotalSurfaceArea>
        <ssip:NegativeSurfaceArea>18.664358683092974</ssip:NegativeSurfaceArea>
        <ssip:PositiveSurfaceArea>18.82047542519977</ssip:PositiveSurfaceArea>
        <ssip:ElectronDensityIsosurface>0.002</ssip:ElectronDensityIsosurface>
        <ssip:NumberOFMEPSPoints>2286</ssip:NumberOFMEPSPoints>
    </ssip:SurfaceInformation>
    <ssip:SSIPs>
        <ssip:SSIP ssip:value="2.9387503903833574" ssip:nearestAtomID="a3" cml:x3="1.509794550767402" cml:y3="-0.8784982637871289" cml:z3="-1.6986589692899999E-4"/>
        <ssip:SSIP ssip:value="2.9275582734233065" ssip:nearestAtomID="a2" cml:x3="-1.4603207114038939" cml:y3="-0.790973405157027" cml:z3="0.12362427220238399"/>
        <ssip:SSIP ssip:value="-6.263074063139137" ssip:nearestAtomID="a1" cml:x3="0.156188781751346" cml:y3="1.6969169177862917" cml:z3="1.172598045109361"/>
        <ssip:SSIP ssip:value="-6.607684490829715" ssip:nearestAtomID="a1" cml:x3="0.06855491260794999" cml:y3="1.9788577934582507" cml:z3="-0.77762067563301"/>
    </ssip:SSIPs>
</ssip:SSIPMolecule>"""
        self.expected_butanol_xml = """<ssip:SSIPMolecule xmlns:cml="http://www.xml-cml.org/schema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ssip:ssipSoftwareVersion="3.2.0" ssip:parameterVersion="1.0.0" xsi:schemaLocation="http://www.xml-cml.org/schema http://www-hunter.ch.cam.ac.uk/schema/cmlschema.xsd http://www-hunter.ch.cam.ac.uk/SSIP http://www-hunter.ch.cam.ac.uk/schema/SSIP.xsd">
    <cml:molecule ssip:stdInChIKey="LRHPLDYGYMQRHN-UHFFFAOYSA-N" cml:id="LRHPLDYGYMQRHN-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="C" cml:id="a1" cml:x3="0.453161473082" cml:y3="-1.03446801266" cml:z3="-0.0905306598548"/>
            <cml:atom cml:elementType="H" cml:id="a10" cml:x3="-0.711314642651" cml:y3="-1.77936986861" cml:z3="1.57182493824"/>
            <cml:atom cml:elementType="H" cml:id="a11" cml:x3="-2.35918900768" cml:y3="-0.976902587046" cml:z3="-0.14772459368"/>
            <cml:atom cml:elementType="H" cml:id="a12" cml:x3="-1.77474726333" cml:y3="-2.19616982792" cml:z3="-1.27745306344"/>
            <cml:atom cml:elementType="H" cml:id="a13" cml:x3="-2.519145554" cml:y3="-4.00559237167" cml:z3="0.302707946329"/>
            <cml:atom cml:elementType="H" cml:id="a14" cml:x3="-3.10209800172" cml:y3="-2.77368664521" cml:z3="1.42866821719"/>
            <cml:atom cml:elementType="H" cml:id="a15" cml:x3="-4.0901443513" cml:y3="-3.10664597908" cml:z3="-1.18904146578"/>
            <cml:atom cml:elementType="C" cml:id="a2" cml:x3="-0.564202390594" cml:y3="-2.0116665412" cml:z3="0.507612692817"/>
            <cml:atom cml:elementType="C" cml:id="a3" cml:x3="-1.92163762747" cml:y3="-1.98241261328" cml:z3="-0.206572876019"/>
            <cml:atom cml:elementType="C" cml:id="a4" cml:x3="-2.92693672032" cml:y3="-2.98225012861" cml:z3="0.367318952425"/>
            <cml:atom cml:elementType="O" cml:id="a5" cml:x3="-4.208010389" cml:y3="-2.9017666367" cml:z3="-0.248445606999"/>
            <cml:atom cml:elementType="H" cml:id="a6" cml:x3="0.655827908609" cml:y3="-1.26752859656" cml:z3="-1.14312376632"/>
            <cml:atom cml:elementType="H" cml:id="a7" cml:x3="0.0848553777946" cml:y3="-0.00239243828223" cml:z3="-0.0456709489122"/>
            <cml:atom cml:elementType="H" cml:id="a8" cml:x3="1.40666917559" cml:y3="-1.07214682201" cml:z3="0.448356319391"/>
            <cml:atom cml:elementType="H" cml:id="a9" cml:x3="-0.154987987004" cml:y3="-3.03150093117" cml:z3="0.472873914607"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a6" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a7" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a8" cml:order="1"/>
            <cml:bond cml:atomRefs2="a2 a10" cml:order="1"/>
            <cml:bond cml:atomRefs2="a2 a3" cml:order="1"/>
            <cml:bond cml:atomRefs2="a2 a9" cml:order="1"/>
            <cml:bond cml:atomRefs2="a3 a11" cml:order="1"/>
            <cml:bond cml:atomRefs2="a3 a12" cml:order="1"/>
            <cml:bond cml:atomRefs2="a3 a4" cml:order="1"/>
            <cml:bond cml:atomRefs2="a4 a13" cml:order="1"/>
            <cml:bond cml:atomRefs2="a4 a14" cml:order="1"/>
            <cml:bond cml:atomRefs2="a4 a5" cml:order="1"/>
            <cml:bond cml:atomRefs2="a5 a15" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    <ssip:SurfaceInformation>
        <ssip:TotalSurfaceArea>122.2095781704891</ssip:TotalSurfaceArea>
        <ssip:NegativeSurfaceArea>20.900319182402814</ssip:NegativeSurfaceArea>
        <ssip:PositiveSurfaceArea>101.30925898808572</ssip:PositiveSurfaceArea>
        <ssip:ElectronDensityIsosurface>0.002</ssip:ElectronDensityIsosurface>
        <ssip:NumberOFMEPSPoints>9580</ssip:NumberOFMEPSPoints>
        <ssip:VdWVolume>99.3920085615431</ssip:VdWVolume>
    </ssip:SurfaceInformation>
    <ssip:SSIPs>
        <ssip:SSIP ssip:value="2.784040024744081" ssip:nearestAtomID="a15" cml:x3="-3.8161781356582187" cml:y3="-3.342557618498982" cml:z3="-2.192649106294994"/>
        <ssip:SSIP ssip:value="0.773639545237379" ssip:nearestAtomID="a12" cml:x3="-2.239105576984704" cml:y3="-3.099174713898159" cml:z3="-2.0252301249653697"/>
        <ssip:SSIP ssip:value="0.5071846345423165" ssip:nearestAtomID="a13" cml:x3="-2.8499750929763277" cml:y3="-4.524230524251444" cml:z3="-0.8462517897648159"/>
        <ssip:SSIP ssip:value="0.4784372492163057" ssip:nearestAtomID="a6" cml:x3="0.9270005337214728" cml:y3="-1.649107340870889" cml:z3="-2.251805301783455"/>
        <ssip:SSIP ssip:value="0.46383776537503163" ssip:nearestAtomID="a9" cml:x3="0.136470579099108" cml:y3="-4.205322913496092" cml:z3="0.352187567944962"/>
        <ssip:SSIP ssip:value="0.4557099985213939" ssip:nearestAtomID="a8" cml:x3="2.420223369759191" cml:y3="-1.188501408973558" cml:z3="1.0796321860050409"/>
        <ssip:SSIP ssip:value="0.4355872186992693" ssip:nearestAtomID="a7" cml:x3="-0.30271267022695597" cml:y3="1.1313523827905538" cml:z3="0.031343697635518995"/>
        <ssip:SSIP ssip:value="0.43429343831228867" ssip:nearestAtomID="a14" cml:x3="-2.937811637006091" cml:y3="-2.45801773806002" cml:z3="2.5781764284587028"/>
        <ssip:SSIP ssip:value="0.33435822085360595" ssip:nearestAtomID="a11" cml:x3="-2.5158938537831497" cml:y3="0.207568717565752" cml:z3="0.059691722864448994"/>
        <ssip:SSIP ssip:value="0.0" ssip:nearestAtomID="a3" cml:x3="-1.4487933333328935" cml:y3="-2.074966666667215" cml:z3="0.05005333333293337"/>
        <ssip:SSIP ssip:value="0.0" ssip:nearestAtomID="a3" cml:x3="-1.4487933333328935" cml:y3="-2.074966666667215" cml:z3="0.05005333333293337"/>
        <ssip:SSIP ssip:value="-5.3386085001748205" ssip:nearestAtomID="a5" cml:x3="-5.4850745889327115" cml:y3="-3.640825199834338" cml:z3="0.48092898414517593"/>
        <ssip:SSIP ssip:value="-5.365217010700665" ssip:nearestAtomID="a5" cml:x3="-5.353424285394745" cml:y3="-1.7849561367024218" cml:z3="0.11271581239149799"/>
    </ssip:SSIPs>
</ssip:SSIPMolecule>"""
        self.ethylheptanoate_xml = """<ssip:SSIPMolecule xmlns:cml="http://www.xml-cml.org/schema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ssip:ssipSoftwareVersion="6.0.1-SNAPSHOT" ssip:parameterVersion="1.0.0" xsi:schemaLocation="http://www.xml-cml.org/schema http://www-hunter.ch.cam.ac.uk/schema/cmlschema.xsd http://www-hunter.ch.cam.ac.uk/SSIP http://www-hunter.ch.cam.ac.uk/schema/SSIP.xsd">
    <cml:molecule ssip:stdInChIKey="TVQGDYNRXLTQAP-UHFFFAOYSA-N" cml:id="TVQGDYNRXLTQAP-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="C" cml:id="a1" cml:formalCharge="0" cml:x3="-4.77793657293563" cml:y3="-1.4241280534948606" cml:z3="-0.3630976931561518"/>
            <cml:atom cml:elementType="C" cml:id="a10" cml:formalCharge="0" cml:x3="4.225142123503188" cml:y3="0.33453515568287406" cml:z3="-0.5742736810267585"/>
            <cml:atom cml:elementType="C" cml:id="a11" cml:formalCharge="0" cml:x3="4.752915111137654" cml:y3="-1.0825143514329492" cml:z3="-0.4501635972674916"/>
            <cml:atom cml:elementType="H" cml:id="a12" cml:formalCharge="0" cml:x3="-4.898955882553757" cml:y3="-1.1163097063502652" cml:z3="-1.4090511324659702"/>
            <cml:atom cml:elementType="H" cml:id="a13" cml:formalCharge="0" cml:x3="-5.771704123049405" cml:y3="-1.4302453370581245" cml:z3="0.09897178617213459"/>
            <cml:atom cml:elementType="H" cml:id="a14" cml:formalCharge="0" cml:x3="-4.404400525467168" cml:y3="-2.4555568670888395" cml:z3="-0.363883585760838"/>
            <cml:atom cml:elementType="H" cml:id="a15" cml:formalCharge="0" cml:x3="-4.238761276661798" cml:y3="0.5282306187458604" cml:z3="0.3951014463604072"/>
            <cml:atom cml:elementType="H" cml:id="a16" cml:formalCharge="0" cml:x3="-3.7469085203939025" cml:y3="-0.8001510110151278" cml:z3="1.4333084764099535"/>
            <cml:atom cml:elementType="H" cml:id="a17" cml:formalCharge="0" cml:x3="-1.9967138707741572" cml:y3="-1.4646557107583962" cml:z3="-0.2489771631543417"/>
            <cml:atom cml:elementType="H" cml:id="a18" cml:formalCharge="0" cml:x3="-2.4879183015390054" cml:y3="-0.13188263194708386" cml:z3="-1.283508732120002"/>
            <cml:atom cml:elementType="H" cml:id="a19" cml:formalCharge="0" cml:x3="-1.373311730851992" cml:y3="0.16654244251848518" cml:z3="1.5619637850918429"/>
            <cml:atom cml:elementType="C" cml:id="a2" cml:formalCharge="0" cml:x3="-3.8203925594557835" cml:y3="-0.48823681576529093" cml:z3="0.3817323772707575"/>
            <cml:atom cml:elementType="H" cml:id="a20" cml:formalCharge="0" cml:x3="-1.8702159319058245" cml:y3="1.5018581340031059" cml:z3="0.5322864237244379"/>
            <cml:atom cml:elementType="H" cml:id="a21" cml:formalCharge="0" cml:x3="0.380647838839953" cml:y3="-0.48093225015259955" cml:z3="-0.13036077411943106"/>
            <cml:atom cml:elementType="H" cml:id="a22" cml:formalCharge="0" cml:x3="-0.12323769763727176" cml:y3="0.8586046709578306" cml:z3="-1.1548945211492472"/>
            <cml:atom cml:elementType="H" cml:id="a23" cml:formalCharge="0" cml:x3="0.5039118772345774" cml:y3="2.4802087006854823" cml:z3="0.6962726455185527"/>
            <cml:atom cml:elementType="H" cml:id="a24" cml:formalCharge="0" cml:x3="1.0315017769301802" cml:y3="1.1011564124916107" cml:z3="1.6798246242058448"/>
            <cml:atom cml:elementType="H" cml:id="a25" cml:formalCharge="0" cml:x3="4.870947781347329" cml:y3="1.0596044299690874" cml:z3="-0.068102978221173"/>
            <cml:atom cml:elementType="H" cml:id="a26" cml:formalCharge="0" cml:x3="4.133496369295323" cml:y3="0.650239312291525" cml:z3="-1.6186590878352158"/>
            <cml:atom cml:elementType="H" cml:id="a27" cml:formalCharge="0" cml:x3="4.836683401906629" cml:y3="-1.3784775096277864" cml:z3="0.600536299347343"/>
            <cml:atom cml:elementType="H" cml:id="a28" cml:formalCharge="0" cml:x3="5.745713893957405" cml:y3="-1.1514603034529023" cml:z3="-0.9079420879278576"/>
            <cml:atom cml:elementType="H" cml:id="a29" cml:formalCharge="0" cml:x3="4.090209431845274" cml:y3="-1.7925164289264108" cml:z3="-0.9556888006688495"/>
            <cml:atom cml:elementType="C" cml:id="a3" cml:formalCharge="0" cml:x3="-2.4149956777423665" cml:y3="-0.44725040070371097" cml:z3="-0.2320459300476804"/>
            <cml:atom cml:elementType="C" cml:id="a4" cml:formalCharge="0" cml:x3="-1.4504558585212788" cml:y3="0.48546240049236566" cml:z3="0.5118802441624547"/>
            <cml:atom cml:elementType="C" cml:id="a5" cml:formalCharge="0" cml:x3="-0.04910680601989368" cml:y3="0.5277887365813221" cml:z3="-0.1096942208664376"/>
            <cml:atom cml:elementType="C" cml:id="a6" cml:formalCharge="0" cml:x3="0.9087361668116533" cml:y3="1.4651179577988453" cml:z3="0.6516228429727999"/>
            <cml:atom cml:elementType="C" cml:id="a7" cml:formalCharge="0" cml:x3="2.276640106664835" cml:y3="1.5601577012313168" cml:z3="0.006347533208713589"/>
            <cml:atom cml:elementType="O" cml:id="a8" cml:formalCharge="0" cml:x3="2.7514872582418173" cml:y3="2.557587257644238" cml:z3="-0.4934254307084001"/>
            <cml:atom cml:elementType="O" cml:id="a9" cml:formalCharge="0" cml:x3="2.916882197793633" cml:y3="0.3670234466798665" cml:z3="0.040120932050534096"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a12" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a13" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a14" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a10 a11" cml:order="1"/>
            <cml:bond cml:atomRefs2="a10 a25" cml:order="1"/>
            <cml:bond cml:atomRefs2="a10 a26" cml:order="1"/>
            <cml:bond cml:atomRefs2="a11 a27" cml:order="1"/>
            <cml:bond cml:atomRefs2="a11 a28" cml:order="1"/>
            <cml:bond cml:atomRefs2="a11 a29" cml:order="1"/>
            <cml:bond cml:atomRefs2="a2 a15" cml:order="1"/>
            <cml:bond cml:atomRefs2="a2 a16" cml:order="1"/>
            <cml:bond cml:atomRefs2="a2 a3" cml:order="1"/>
            <cml:bond cml:atomRefs2="a3 a17" cml:order="1"/>
            <cml:bond cml:atomRefs2="a3 a18" cml:order="1"/>
            <cml:bond cml:atomRefs2="a3 a4" cml:order="1"/>
            <cml:bond cml:atomRefs2="a4 a19" cml:order="1"/>
            <cml:bond cml:atomRefs2="a4 a20" cml:order="1"/>
            <cml:bond cml:atomRefs2="a4 a5" cml:order="1"/>
            <cml:bond cml:atomRefs2="a5 a21" cml:order="1"/>
            <cml:bond cml:atomRefs2="a5 a22" cml:order="1"/>
            <cml:bond cml:atomRefs2="a5 a6" cml:order="1"/>
            <cml:bond cml:atomRefs2="a6 a23" cml:order="1"/>
            <cml:bond cml:atomRefs2="a6 a24" cml:order="1"/>
            <cml:bond cml:atomRefs2="a6 a7" cml:order="1"/>
            <cml:bond cml:atomRefs2="a7 a8" cml:order="2"/>
            <cml:bond cml:atomRefs2="a7 a9" cml:order="1"/>
            <cml:bond cml:atomRefs2="a9 a10" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    <ssip:SurfaceInformation>
        <ssip:Surfaces>
            <ssip:Surface>
                <ssip:TotalSurfaceArea ssip:unit="Å^2">225.01297702423483</ssip:TotalSurfaceArea>
                <ssip:NegativeSurfaceArea ssip:unit="Å^2">38.36229972474354</ssip:NegativeSurfaceArea>
                <ssip:PositiveSurfaceArea ssip:unit="Å^2">186.6375106328303</ssip:PositiveSurfaceArea>
                <ssip:ElectronDensityIsosurface ssip:unit="e bohr^-3">0.002</ssip:ElectronDensityIsosurface>
                <ssip:NumberOFMEPSPoints>17829</ssip:NumberOFMEPSPoints>
                <ssip:VdWVolume ssip:unit="Å^3">198.68006374732028</ssip:VdWVolume>
                <ssip:ElectrostaticPotentialMax ssip:unit="kJ mol^-1">74.78998217532</ssip:ElectrostaticPotentialMax>
                <ssip:ElectrostaticPotentialMin ssip:unit="kJ mol^-1">-172.74474749790002</ssip:ElectrostaticPotentialMin>
            </ssip:Surface>
        </ssip:Surfaces>
    </ssip:SurfaceInformation>
    <ssip:SSIPs>
        <ssip:SSIP ssip:value="0.7052382353065448" ssip:nearestAtomID="a28" cml:x3="6.835506352809266" cml:y3="-1.0920313381263598" cml:z3="-1.38902677735512"/>
        <ssip:SSIP ssip:value="0.6434223074737937" ssip:nearestAtomID="a24" cml:x3="1.129916724914017" cml:y3="0.666501920178994" cml:z3="2.7895150047463306"/>
        <ssip:SSIP ssip:value="0.593850816369865" ssip:nearestAtomID="a7" cml:x3="2.7544543661138357" cml:y3="1.721621557655857" cml:z3="1.5732074480468188"/>
        <ssip:SSIP ssip:value="0.5831168262332395" ssip:nearestAtomID="a29" cml:x3="3.503656635943799" cml:y3="-2.6316963570680647" cml:z3="-1.568809985107629"/>
        <ssip:SSIP ssip:value="0.5787300150724145" ssip:nearestAtomID="a27" cml:x3="5.118901924651178" cml:y3="-1.5975040922574049" cml:z3="1.7384933158857239"/>
        <ssip:SSIP ssip:value="0.5551329481435678" ssip:nearestAtomID="a25" cml:x3="5.87739654697358" cml:y3="1.369915011830236" cml:z3="0.5123012573521409"/>
        <ssip:SSIP ssip:value="0.5546574072668179" ssip:nearestAtomID="a26" cml:x3="4.382819546355671" cml:y3="0.5028914275104229" cml:z3="-2.7883524023302777"/>
        <ssip:SSIP ssip:value="0.5116489749462008" ssip:nearestAtomID="a7" cml:x3="1.632575843612129" cml:y3="0.954199185965575" cml:z3="-1.4145722798733458"/>
        <ssip:SSIP ssip:value="0.47342451173253985" ssip:nearestAtomID="a23" cml:x3="-0.27674382590953" cml:y3="3.3866952344835735" cml:z3="0.748312193697145"/>
        <ssip:SSIP ssip:value="0.4140225171023405" ssip:nearestAtomID="a14" cml:x3="-3.930124814330891" cml:y3="-3.5539776880829557" cml:z3="-0.22445158851784797"/>
        <ssip:SSIP ssip:value="0.4048241866213803" ssip:nearestAtomID="a13" cml:x3="-6.782549997970089" cml:y3="-1.439696028124119" cml:z3="0.7527345279670379"/>
        <ssip:SSIP ssip:value="0.4044343044287664" ssip:nearestAtomID="a12" cml:x3="-4.936307727351981" cml:y3="-0.740324792300739" cml:z3="-2.553179153570441"/>
        <ssip:SSIP ssip:value="0.388028461256277" ssip:nearestAtomID="a21" cml:x3="0.33867026429650593" cml:y3="-1.686522818261434" cml:z3="0.010440137945520999"/>
        <ssip:SSIP ssip:value="0.38612290135942556" ssip:nearestAtomID="a18" cml:x3="-2.593178074290604" cml:y3="0.31479537435337296" cml:z3="-2.4097816442819227"/>
        <ssip:SSIP ssip:value="0.3812905091620159" ssip:nearestAtomID="a16" cml:x3="-3.5095648999288835" cml:y3="-1.2832304866715458" cml:z3="2.5267594502368667"/>
        <ssip:SSIP ssip:value="0.37179112909992346" ssip:nearestAtomID="a15" cml:x3="-4.736958719994946" cml:y3="1.6336950534937638" cml:z3="0.334435787950008"/>
        <ssip:SSIP ssip:value="0.0" ssip:nearestAtomID="a5" cml:x3="-3.4482758545114126E-6" cml:y3="-6.89655174248265E-6" cml:z3="-0.06116551724138171"/>
        <ssip:SSIP ssip:value="0.0" ssip:nearestAtomID="a5" cml:x3="-3.4482758545114126E-6" cml:y3="-6.89655174248265E-6" cml:z3="-0.06116551724138171"/>
        <ssip:SSIP ssip:value="0.0" ssip:nearestAtomID="a5" cml:x3="-3.4482758545114126E-6" cml:y3="-6.89655174248265E-6" cml:z3="-0.06116551724138171"/>
        <ssip:SSIP ssip:value="0.0" ssip:nearestAtomID="a5" cml:x3="-3.4482758545114126E-6" cml:y3="-6.89655174248265E-6" cml:z3="-0.06116551724138171"/>
        <ssip:SSIP ssip:value="-0.04832232508329189" ssip:nearestAtomID="a13" cml:x3="-5.903387087558216" cml:y3="-2.489844045346627" cml:z3="-1.164482582818697"/>
        <ssip:SSIP ssip:value="-2.011946765898198" ssip:nearestAtomID="a9" cml:x3="2.3607449053260887" cml:y3="-0.965254757051683" cml:z3="0.806982604471024"/>
        <ssip:SSIP ssip:value="-5.143140662614065" ssip:nearestAtomID="a8" cml:x3="3.9432383557383597" cml:y3="3.2163747742976825" cml:z3="-1.326987095036858"/>
        <ssip:SSIP ssip:value="-5.694569321748074" ssip:nearestAtomID="a8" cml:x3="2.5365772757375638" cml:y3="4.1163909725608985" cml:z3="-0.80307410130991"/>
    </ssip:SSIPs>
</ssip:SSIPMolecule>"""
        self.solvent_dict = {"XLYOFNOQVPJJNP-UHFFFAOYSA-N":{"solvent_id":"water",
                                                              "solvent_name":"water",
                                                              "ssip_info_list":{"concentration_value":55.35,
            "inchikey":"XLYOFNOQVPJJNP-UHFFFAOYSA-N",
            "mole_fraction":1.0,
            "ssip_molecule":self.expected_water_xml,}},
                               "LRHPLDYGYMQRHN-UHFFFAOYSA-N":{"solvent_id":"1-butanol",
                                                              "solvent_name":"1-butanol",
                                                              "ssip_info_list":{"concentration_value":10.9,
            "inchikey":"LRHPLDYGYMQRHN-UHFFFAOYSA-N",
            "mole_fraction":1.0,
            "ssip_molecule":self.expected_butanol_xml}},
                               "TVQGDYNRXLTQAP-UHFFFAOYSA-N":{"solvent_id":"Ethyl heptanoate",
                                                              "solvent_name":"Ethyl heptanoate",
                                                              "ssip_info_list":{"concentration_value":10.0,
            "inchikey":"TVQGDYNRXLTQAP-UHFFFAOYSA-N",
            "mole_fraction":1.0,
            "ssip_molecule":self.ethylheptanoate_xml}},
                }
        self.mole_fraction_dict = {"water":0.8354716981132075,
                                  "1-octanol":0.16452830188679246}
        
    def test_generate_solv_info_from_files(self):
        """Test expected information is generated.

        Returns
        -------
        None.

        """
        expected_list = [{"concentration_value":10.9,
            "inchikey":"LRHPLDYGYMQRHN-UHFFFAOYSA-N",
            "mole_fraction":0.16452830188679246,
            "ssip_molecule":self.expected_butanol_xml},
            {"concentration_value":55.35,
            "inchikey":"XLYOFNOQVPJJNP-UHFFFAOYSA-N",
            "mole_fraction":0.8354716981132075,
            "ssip_molecule":self.expected_water_xml,},
            ]
        expected_keys = [
            "concentration_value",
            "inchikey",
            "mole_fraction",
            "ssip_molecule",
        ]
        actual_info_list = multiassem.generate_solv_info_from_files([self.mole_fraction_dict], self.solvent_filename, self.ssip_filename_list, self.name_inchikey_map)
        self.assertEqual(1, len(actual_info_list))
        actual_information = actual_info_list[0]
        self.assertEqual("1-octanol0.164528302water0.835471698", actual_information["solvent_name"])
        self.assertEqual("1-octanol0.164528302water0.835471698", actual_information["solvent_id"])
        actual_list = actual_information["ssip_info_list"]
        for entry, information_dict in enumerate(actual_list):
            with self.subTest(entry=entry):
                expected_dict = expected_list[entry]
                self.assertListEqual(expected_keys, sorted(information_dict.keys()))
                self.assertEqual(
                    expected_dict["inchikey"], information_dict["inchikey"]
                )
                self.assertAlmostEqual(expected_dict["concentration_value"], information_dict["concentration_value"])
                self.assertAlmostEqual(expected_dict["mole_fraction"], information_dict["mole_fraction"])
                self.assertMultiLineEqual(
                    expected_dict["ssip_molecule"],
                    etree.tounicode(information_dict["ssip_molecule"]),
                )
    def test_generate_solv_info_list(self):
        """Test expected information is generated.

        Returns
        -------
        None.

        """
        expected_list = [{"concentration_value":10.9,
            "inchikey":"LRHPLDYGYMQRHN-UHFFFAOYSA-N",
            "mole_fraction":0.16452830188679246,
            "ssip_molecule":self.expected_butanol_xml},
            {"concentration_value":55.35,
            "inchikey":"XLYOFNOQVPJJNP-UHFFFAOYSA-N",
            "mole_fraction":0.8354716981132075,
            "ssip_molecule":self.expected_water_xml,},
            ]
        expected_keys = [
            "concentration_value",
            "inchikey",
            "mole_fraction",
            "ssip_molecule",
        ]
        actual_info_list = multiassem.generate_solv_info_list([self.mole_fraction_dict], self.solvent_information, self.name_inchikey_map)
        self.assertEqual(1, len(actual_info_list))
        actual_information = actual_info_list[0]
        self.assertEqual("1-octanol0.164528302water0.835471698", actual_information["solvent_name"])
        self.assertEqual("1-octanol0.164528302water0.835471698", actual_information["solvent_id"])
        actual_list = actual_information["ssip_info_list"]
        for entry, information_dict in enumerate(actual_list):
            with self.subTest(entry=entry):
                expected_dict = expected_list[entry]
                self.assertListEqual(expected_keys, sorted(information_dict.keys()))
                self.assertEqual(
                    expected_dict["inchikey"], information_dict["inchikey"]
                )
                self.assertAlmostEqual(expected_dict["concentration_value"], information_dict["concentration_value"])
                self.assertAlmostEqual(expected_dict["mole_fraction"], information_dict["mole_fraction"])
                self.assertMultiLineEqual(
                    expected_dict["ssip_molecule"],
                    etree.tounicode(information_dict["ssip_molecule"]),
                )
    def test_generate_solvent_information(self):
        """Test expected solvent information is assembled.

        Returns
        -------
        None.

        """
        expected_list = [{"concentration_value":10.9,
            "inchikey":"LRHPLDYGYMQRHN-UHFFFAOYSA-N",
            "mole_fraction":0.16452830188679246,
            "ssip_molecule":self.expected_butanol_xml},
            {"concentration_value":55.35,
            "inchikey":"XLYOFNOQVPJJNP-UHFFFAOYSA-N",
            "mole_fraction":0.8354716981132075,
            "ssip_molecule":self.expected_water_xml,},
            ]
        expected_keys = [
            "concentration_value",
            "inchikey",
            "mole_fraction",
            "ssip_molecule",
        ]
        actual_information = multiassem.generate_solvent_information(self.mole_fraction_dict, self.solvent_information, self.name_inchikey_map)
        self.assertEqual("1-octanol0.164528302water0.835471698", actual_information["solvent_name"])
        self.assertEqual("1-octanol0.164528302water0.835471698", actual_information["solvent_id"])
        actual_list = actual_information["ssip_info_list"]
        for entry, information_dict in enumerate(actual_list):
            with self.subTest(entry=entry):
                expected_dict = expected_list[entry]
                self.assertListEqual(expected_keys, sorted(information_dict.keys()))
                self.assertEqual(
                    expected_dict["inchikey"], information_dict["inchikey"]
                )
                self.assertAlmostEqual(expected_dict["concentration_value"], information_dict["concentration_value"])
                self.assertAlmostEqual(expected_dict["mole_fraction"], information_dict["mole_fraction"])
                self.assertMultiLineEqual(
                    expected_dict["ssip_molecule"],
                    etree.tounicode(information_dict["ssip_molecule"]),
                )
    def test_create_solvent_name(self):
        """Test expected name is created.

        Returns
        -------
        None.

        """
        expected_name = "1-octanol0.164528302water0.835471698"
        actual_name = multiassem.create_solvent_name({"water":0.8354716981132075,
                                                      "1-octanol":0.16452830188679246})
        self.assertEqual(expected_name, actual_name)
    def test_convert_molfrac_names_to_inchikeys(self):
        """Test if molefraction names are converted to inchikeys.

        Returns
        -------
        None.

        """
        expected_molefractions = {"XLYOFNOQVPJJNP-UHFFFAOYSA-N": 0.8354716981132075,
                                  "LRHPLDYGYMQRHN-UHFFFAOYSA-N": 0.16452830188679246}
        actual_mole_fractions = multiassem.convert_molfrac_names_to_inchikeys(self.mole_fraction_dict,
                                                                              self.name_inchikey_map)
        for inchikey, mole_fraction in actual_mole_fractions.items():
            with self.subTest(inchikey=inchikey):
                self.assertAlmostEqual(expected_molefractions[inchikey], mole_fraction)
    def test_append_molefractions(self):
        """Test if expected information is returned.

        Returns
        -------
        None.

        """
        expected_list = [{"concentration_value":10.9,
            "inchikey":"LRHPLDYGYMQRHN-UHFFFAOYSA-N",
            "mole_fraction":0.16452830188679246,
            "ssip_molecule":self.expected_butanol_xml},
            {"concentration_value":55.35,
            "inchikey":"XLYOFNOQVPJJNP-UHFFFAOYSA-N",
            "mole_fraction":0.8354716981132075,
            "ssip_molecule":self.expected_water_xml,},
            ]
        expected_keys = [
            "concentration_value",
            "inchikey",
            "mole_fraction",
            "ssip_molecule",
        ]
        actual_list = multiassem.append_molefractions(self.solvent_information,
                                                      {"XLYOFNOQVPJJNP-UHFFFAOYSA-N": 0.8354716981132075,
                                                       "LRHPLDYGYMQRHN-UHFFFAOYSA-N": 0.16452830188679246})
        
        for entry, information_dict in enumerate(actual_list):
            with self.subTest(entry=entry):
                expected_dict = expected_list[entry]
                self.assertListEqual(expected_keys, sorted(information_dict.keys()))
                self.assertEqual(
                    expected_dict["inchikey"], information_dict["inchikey"]
                )
                self.assertAlmostEqual(expected_dict["concentration_value"], information_dict["concentration_value"])
                self.assertAlmostEqual(expected_dict["mole_fraction"], information_dict["mole_fraction"])
                self.assertMultiLineEqual(
                    expected_dict["ssip_molecule"],
                    etree.tounicode(information_dict["ssip_molecule"]),
                )
    def test_calculate_mole_fractions(self):
        """Test mole fractions are calculated as expected.

        Returns
        -------
        None.

        """
        expected_array = {"XLYOFNOQVPJJNP-UHFFFAOYSA-N": 0.8354716981132075,
                          "LRHPLDYGYMQRHN-UHFFFAOYSA-N": 0.16452830188679246}
        actual_array = multiassem.calculate_mole_fractions({"XLYOFNOQVPJJNP-UHFFFAOYSA-N":55.35,
                                                            "LRHPLDYGYMQRHN-UHFFFAOYSA-N":10.9},
                                                           {"XLYOFNOQVPJJNP-UHFFFAOYSA-N":0.5,
                                                            "LRHPLDYGYMQRHN-UHFFFAOYSA-N":0.5})
        for inchikey, value in actual_array.items():
            with self.subTest(inchikey=inchikey):
                self.assertAlmostEqual(expected_array[inchikey], value)
    def test_read_and_merge_solvent_information(self):
        """Test expected information is read in.

        Returns
        -------
        None.

        """
        expected_key_list = ["solvent_id", "solvent_name", "ssip_info_list"]
        expected_keys = [
            "concentration_value",
            "inchikey",
            "mole_fraction",
            "ssip_molecule",
        ]
        expected_dictionary = self.solvent_dict
        actual_dictionary = self.solvent_information
        self.assertEqual(3, len(actual_dictionary))
        for inchikey, solvent_dict in actual_dictionary.items():
            self.assertListEqual(expected_key_list, sorted(solvent_dict.keys()))
            
            self.assertEqual(expected_dictionary[inchikey]["solvent_id"], solvent_dict["solvent_id"])
            self.assertEqual(expected_dictionary[inchikey]["solvent_name"], solvent_dict["solvent_name"])
            self.assertEqual(1, len(solvent_dict["ssip_info_list"]))
            for information_dict in solvent_dict["ssip_info_list"]:
                expected_dict = expected_dictionary[inchikey]["ssip_info_list"]
                self.assertListEqual(expected_keys, sorted(information_dict.keys()))
                self.assertEqual(
                    expected_dict["inchikey"], information_dict["inchikey"]
                )
                self.assertAlmostEqual(expected_dict["concentration_value"], information_dict["concentration_value"])
                self.assertAlmostEqual(expected_dict["mole_fraction"], information_dict["mole_fraction"])
                self.assertMultiLineEqual(
                    expected_dict["ssip_molecule"],
                    etree.tounicode(information_dict["ssip_molecule"]),
                )
        

