# -*- coding: utf-8 -*-
#    phasexmlcreator provides utilites to generate XML input for phasetransfer calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlcreator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for tests of the solutefilemaker script.

@author: mark
"""

import logging
import unittest
import os
import pathlib
from lxml import etree
import phasexmlcreator.solutexmlmaker as solutexmlmaker

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)


class SoluteXMLMakerTestCase(unittest.TestCase):
    """Test case for the solutefilemaker methods.
    """

    def setUp(self):
        """Set up before tests.
        """
        self.maxDiff = None
        parent_directory = pathlib.Path(__file__).parents[0]
        self.ssip_filename = (
            (parent_directory / "resources/XLYOFNOQVPJJNP-UHFFFAOYSA-N_ssip.xml")
            .absolute()
            .as_posix()
        )
        self.water_solute_file = (
            (parent_directory / "resources/watersolute2.xml").absolute().as_posix()
        )
        self.expected_ssip_list_xml = """<ssip:SSIPMolecule xmlns:cml="http://www.xml-cml.org/schema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ssip:ssipSoftwareVersion="2.2.0-SNAPSHOT" ssip:parameterVersion="1.0.0" xsi:schemaLocation="http://www.xml-cml.org/schema http://www-hunter.ch.cam.ac.uk/schema/cmlschema.xsd http://www-hunter.ch.cam.ac.uk/SSIP http://www-hunter.ch.cam.ac.uk/schema/SSIP.xsd">
    <cml:molecule ssip:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" cml:id="XLYOFNOQVPJJNP-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="O" cml:id="a1" cml:x3="0.0569236794019" cml:y3="0.53497425302" cml:z3="-9.99862395488E-5"/>
            <cml:atom cml:elementType="H" cml:id="a2" cml:x3="-0.735721920018" cml:y3="-0.0219270863225" cml:z3="-1.029173866E-4"/>
            <cml:atom cml:elementType="H" cml:id="a3" cml:x3="0.785198240616" cml:y3="-0.103847166698" cml:z3="-1.97096373822E-4"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a3" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    <ssip:SurfaceInformation>
        <ssip:TotalSurfaceArea>37.48483410829331</ssip:TotalSurfaceArea>
        <ssip:NegativeSurfaceArea>18.664358683092974</ssip:NegativeSurfaceArea>
        <ssip:PositiveSurfaceArea>18.82047542519977</ssip:PositiveSurfaceArea>
        <ssip:ElectronDensityIsosurface>0.002</ssip:ElectronDensityIsosurface>
        <ssip:NumberOFMEPSPoints>2286</ssip:NumberOFMEPSPoints>
    </ssip:SurfaceInformation>
    <ssip:SSIPs>
        <ssip:SSIP ssip:value="2.9387503903833574" ssip:nearestAtomID="a3" cml:x3="1.509794550767402" cml:y3="-0.8784982637871289" cml:z3="-1.6986589692899999E-4"/>
        <ssip:SSIP ssip:value="2.9275582734233065" ssip:nearestAtomID="a2" cml:x3="-1.4603207114038939" cml:y3="-0.790973405157027" cml:z3="0.12362427220238399"/>
        <ssip:SSIP ssip:value="-6.263074063139137" ssip:nearestAtomID="a1" cml:x3="0.156188781751346" cml:y3="1.6969169177862917" cml:z3="1.172598045109361"/>
        <ssip:SSIP ssip:value="-6.607684490829715" ssip:nearestAtomID="a1" cml:x3="0.06855491260794999" cml:y3="1.9788577934582507" cml:z3="-0.77762067563301"/>
    </ssip:SSIPs>
</ssip:SSIPMolecule>"""
        self.ssip_info_dict_dict = solutexmlmaker.extract_ssip_information_from_files(
            [self.ssip_filename]
        )
        self.actual_filename = "actual_solute2.xml"

    def tearDown(self):
        """Clean up after tests.
        """
        del self.ssip_filename
        del self.expected_ssip_list_xml
        if os.path.isfile(self.actual_filename):
            os.remove(self.actual_filename)
        del self.actual_filename

    def test_write_ssip_lists_to_solute(self):
        """Test to see if ssip lists are read in and written to solute lists.
        """
        solutexmlmaker.write_ssip_lists_to_solute(
            [self.ssip_filename], 0.001, self.actual_filename
        )
        with open(self.actual_filename, "r") as actual_file:
            actual_xml = actual_file.read()
            with open(self.water_solute_file) as expected_file:
                expected_xml = expected_file.read()
                self.assertMultiLineEqual(expected_xml, actual_xml)

    def test_write_solute_list_element_to_file(self):
        """Test to see if solute list element is written to file.
        """
        solute_list_element = solutexmlmaker.generate_solute_list_element_from_filename_list(
            [self.ssip_filename], 0.001
        )
        solutexmlmaker.write_solute_list_element_to_file(
            solute_list_element, self.actual_filename
        )
        with open(self.actual_filename, "r") as actual_file:
            actual_xml = actual_file.read()
            with open(self.water_solute_file) as expected_file:
                expected_xml = expected_file.read()
                self.assertMultiLineEqual(expected_xml, actual_xml)

    def test_generate_solute_list_element_from_filename_list(self):
        """Test to see if solute list element is generated from a filename list.
        """
        expected_xml = """<phase:SoluteList xmlns:phase="http://www-hunter.ch.cam.ac.uk/PhaseSchema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:cml="http://www.xml-cml.org/schema">
  <phase:Solutes>
    <phase:Solute phase:soluteID="XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute">
      <phase:Molecule phase:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute"><cml:molecule ssip:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" cml:id="XLYOFNOQVPJJNP-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="O" cml:id="a1" cml:x3="0.0569236794019" cml:y3="0.53497425302" cml:z3="-9.99862395488E-5"/>
            <cml:atom cml:elementType="H" cml:id="a2" cml:x3="-0.735721920018" cml:y3="-0.0219270863225" cml:z3="-1.029173866E-4"/>
            <cml:atom cml:elementType="H" cml:id="a3" cml:x3="0.785198240616" cml:y3="-0.103847166698" cml:z3="-1.97096373822E-4"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a3" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    <ssip:SurfaceInformation>
        <ssip:TotalSurfaceArea>37.48483410829331</ssip:TotalSurfaceArea>
        <ssip:NegativeSurfaceArea>18.664358683092974</ssip:NegativeSurfaceArea>
        <ssip:PositiveSurfaceArea>18.82047542519977</ssip:PositiveSurfaceArea>
        <ssip:ElectronDensityIsosurface>0.002</ssip:ElectronDensityIsosurface>
        <ssip:NumberOFMEPSPoints>2286</ssip:NumberOFMEPSPoints>
    </ssip:SurfaceInformation>
    <phase:SSIPs phase:units="MOLAR"><phase:SSIP ssip:value="2.9387503903833574" ssip:nearestAtomID="a3" cml:x3="1.509794550767402" cml:y3="-0.8784982637871289" cml:z3="-1.6986589692899999E-4" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">0.0010000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="2.9275582734233065" ssip:nearestAtomID="a2" cml:x3="-1.4603207114038939" cml:y3="-0.790973405157027" cml:z3="0.12362427220238399" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">0.0010000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.263074063139137" ssip:nearestAtomID="a1" cml:x3="0.156188781751346" cml:y3="1.6969169177862917" cml:z3="1.172598045109361" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">0.0010000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.607684490829715" ssip:nearestAtomID="a1" cml:x3="0.06855491260794999" cml:y3="1.9788577934582507" cml:z3="-0.77762067563301" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">0.0010000000</phase:TotalConcentration></phase:SSIP></phase:SSIPs></phase:Molecule>
    </phase:Solute>
  </phase:Solutes>
</phase:SoluteList>
"""
        actual_xml_element = solutexmlmaker.generate_solute_list_element_from_filename_list(
            [self.ssip_filename], 0.001
        )
        self.assertMultiLineEqual(
            expected_xml, etree.tounicode(actual_xml_element, pretty_print=True)
        )

    def test_generate_solute_list_element_from_dict(self):
        """test to see if expected element is generated from dictionary.
        """
        expected_xml = """<phase:SoluteList xmlns:phase="http://www-hunter.ch.cam.ac.uk/PhaseSchema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:cml="http://www.xml-cml.org/schema">
  <phase:Solutes>
    <phase:Solute phase:soluteID="XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute">
      <phase:Molecule phase:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute"><cml:molecule ssip:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" cml:id="XLYOFNOQVPJJNP-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="O" cml:id="a1" cml:x3="0.0569236794019" cml:y3="0.53497425302" cml:z3="-9.99862395488E-5"/>
            <cml:atom cml:elementType="H" cml:id="a2" cml:x3="-0.735721920018" cml:y3="-0.0219270863225" cml:z3="-1.029173866E-4"/>
            <cml:atom cml:elementType="H" cml:id="a3" cml:x3="0.785198240616" cml:y3="-0.103847166698" cml:z3="-1.97096373822E-4"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a3" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    <ssip:SurfaceInformation>
        <ssip:TotalSurfaceArea>37.48483410829331</ssip:TotalSurfaceArea>
        <ssip:NegativeSurfaceArea>18.664358683092974</ssip:NegativeSurfaceArea>
        <ssip:PositiveSurfaceArea>18.82047542519977</ssip:PositiveSurfaceArea>
        <ssip:ElectronDensityIsosurface>0.002</ssip:ElectronDensityIsosurface>
        <ssip:NumberOFMEPSPoints>2286</ssip:NumberOFMEPSPoints>
    </ssip:SurfaceInformation>
    <phase:SSIPs phase:units="MOLAR"><phase:SSIP ssip:value="2.9387503903833574" ssip:nearestAtomID="a3" cml:x3="1.509794550767402" cml:y3="-0.8784982637871289" cml:z3="-1.6986589692899999E-4" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">0.0010000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="2.9275582734233065" ssip:nearestAtomID="a2" cml:x3="-1.4603207114038939" cml:y3="-0.790973405157027" cml:z3="0.12362427220238399" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">0.0010000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.263074063139137" ssip:nearestAtomID="a1" cml:x3="0.156188781751346" cml:y3="1.6969169177862917" cml:z3="1.172598045109361" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">0.0010000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.607684490829715" ssip:nearestAtomID="a1" cml:x3="0.06855491260794999" cml:y3="1.9788577934582507" cml:z3="-0.77762067563301" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">0.0010000000</phase:TotalConcentration></phase:SSIP></phase:SSIPs></phase:Molecule>
    </phase:Solute>
  </phase:Solutes>
</phase:SoluteList>
"""
        ssip_info_dict_dict = solutexmlmaker.extract_ssip_information_append_concentration(
            [self.ssip_filename], 0.001
        )
        actual_xml_element = solutexmlmaker.generate_solute_list_element_from_dict(
            ssip_info_dict_dict
        )
        self.assertMultiLineEqual(
            expected_xml, etree.tounicode(actual_xml_element, pretty_print=True)
        )

    def test_generate_solute_list_element(self):
        """Test to see if expected solute list element is generated.
        """
        expected_xml = """<phase:SoluteList xmlns:phase="http://www-hunter.ch.cam.ac.uk/PhaseSchema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:cml="http://www.xml-cml.org/schema">
  <phase:Solutes>
    <phase:Solute phase:soluteID="XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute">
      <phase:Molecule phase:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute"><cml:molecule ssip:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" cml:id="XLYOFNOQVPJJNP-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="O" cml:id="a1" cml:x3="0.0569236794019" cml:y3="0.53497425302" cml:z3="-9.99862395488E-5"/>
            <cml:atom cml:elementType="H" cml:id="a2" cml:x3="-0.735721920018" cml:y3="-0.0219270863225" cml:z3="-1.029173866E-4"/>
            <cml:atom cml:elementType="H" cml:id="a3" cml:x3="0.785198240616" cml:y3="-0.103847166698" cml:z3="-1.97096373822E-4"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a3" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    <ssip:SurfaceInformation>
        <ssip:TotalSurfaceArea>37.48483410829331</ssip:TotalSurfaceArea>
        <ssip:NegativeSurfaceArea>18.664358683092974</ssip:NegativeSurfaceArea>
        <ssip:PositiveSurfaceArea>18.82047542519977</ssip:PositiveSurfaceArea>
        <ssip:ElectronDensityIsosurface>0.002</ssip:ElectronDensityIsosurface>
        <ssip:NumberOFMEPSPoints>2286</ssip:NumberOFMEPSPoints>
    </ssip:SurfaceInformation>
    <phase:SSIPs phase:units="MOLAR"><phase:SSIP ssip:value="2.9387503903833574" ssip:nearestAtomID="a3" cml:x3="1.509794550767402" cml:y3="-0.8784982637871289" cml:z3="-1.6986589692899999E-4" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">0.0010000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="2.9275582734233065" ssip:nearestAtomID="a2" cml:x3="-1.4603207114038939" cml:y3="-0.790973405157027" cml:z3="0.12362427220238399" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">0.0010000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.263074063139137" ssip:nearestAtomID="a1" cml:x3="0.156188781751346" cml:y3="1.6969169177862917" cml:z3="1.172598045109361" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">0.0010000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.607684490829715" ssip:nearestAtomID="a1" cml:x3="0.06855491260794999" cml:y3="1.9788577934582507" cml:z3="-0.77762067563301" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-Nsolute" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">0.0010000000</phase:TotalConcentration></phase:SSIP></phase:SSIPs></phase:Molecule>
    </phase:Solute>
  </phase:Solutes>
</phase:SoluteList>
"""
        ssip_info_dict_dict = solutexmlmaker.extract_ssip_information_append_concentration(
            [self.ssip_filename], 0.001
        )
        ssip_info_dict_list = solutexmlmaker.get_sorted_ssip_info_list(
            ssip_info_dict_dict
        )
        actual_xml_element = solutexmlmaker.generate_solute_list_element(
            ssip_info_dict_list
        )
        self.assertMultiLineEqual(
            expected_xml, etree.tounicode(actual_xml_element, pretty_print=True)
        )

    def test_get_sorted_ssip_info_list(self):
        """test to see if expecte dlist is returned.
        """
        ssip_info_dict_dict = solutexmlmaker.extract_ssip_information_append_concentration(
            [self.ssip_filename], 0.001
        )
        ssip_info_dict_list = solutexmlmaker.get_sorted_ssip_info_list(
            ssip_info_dict_dict
        )
        expected_keys = ["concentration_value", "inchikey", "ssip_molecule"]
        for information_dict in ssip_info_dict_list:
            self.assertListEqual(expected_keys, sorted(information_dict.keys()))
            self.assertAlmostEqual(0.001, information_dict["concentration_value"])
            self.assertEqual(
                "XLYOFNOQVPJJNP-UHFFFAOYSA-N", information_dict["inchikey"]
            )
            self.assertMultiLineEqual(
                self.expected_ssip_list_xml,
                etree.tounicode(information_dict["ssip_molecule"]),
            )

    def test_extract_ssip_information_append_concentration(self):
        """Test to see if the ssip information is extracted and the concentration appended as expected.
        """
        ssip_info_dict_dict = solutexmlmaker.extract_ssip_information_append_concentration(
            [self.ssip_filename], 0.001
        )
        self.assertEqual(1, len(ssip_info_dict_dict))
        expected_keys = ["concentration_value", "inchikey", "ssip_molecule"]
        for inchikey, information_dict in ssip_info_dict_dict.items():
            self.assertEqual("XLYOFNOQVPJJNP-UHFFFAOYSA-N", inchikey)
            self.assertListEqual(expected_keys, sorted(information_dict.keys()))
            self.assertAlmostEqual(0.001, information_dict["concentration_value"])
            self.assertEqual(
                "XLYOFNOQVPJJNP-UHFFFAOYSA-N", information_dict["inchikey"]
            )
            self.assertMultiLineEqual(
                self.expected_ssip_list_xml,
                etree.tounicode(information_dict["ssip_molecule"]),
            )

    def test_append_concentrations_to_all_ssip_list_info(self):
        """Test to see if concentration is appended to all values in dictionary.
        """
        solutexmlmaker.append_concentrations_to_all_ssip_list_info(
            self.ssip_info_dict_dict, 0.001
        )
        self.assertEqual(1, len(self.ssip_info_dict_dict))
        expected_keys = ["concentration_value", "inchikey", "ssip_molecule"]
        for inchikey, information_dict in self.ssip_info_dict_dict.items():
            self.assertEqual("XLYOFNOQVPJJNP-UHFFFAOYSA-N", inchikey)
            self.assertListEqual(expected_keys, sorted(information_dict.keys()))
            self.assertAlmostEqual(0.001, information_dict["concentration_value"])
            self.assertEqual(
                "XLYOFNOQVPJJNP-UHFFFAOYSA-N", information_dict["inchikey"]
            )
            self.assertMultiLineEqual(
                self.expected_ssip_list_xml,
                etree.tounicode(information_dict["ssip_molecule"]),
            )

    def test_append_concentration_to_ssip_list_info(self):
        """Test to see if expected information is appended the element.
        """
        information_dict = self.ssip_info_dict_dict["XLYOFNOQVPJJNP-UHFFFAOYSA-N"]
        solutexmlmaker.append_concentration_to_ssip_list_info(information_dict, 0.001)
        expected_keys = ["concentration_value", "inchikey", "ssip_molecule"]
        self.assertListEqual(expected_keys, sorted(information_dict.keys()))
        self.assertAlmostEqual(0.001, information_dict["concentration_value"])
        self.assertEqual("XLYOFNOQVPJJNP-UHFFFAOYSA-N", information_dict["inchikey"])
        self.assertMultiLineEqual(
            self.expected_ssip_list_xml,
            etree.tounicode(information_dict["ssip_molecule"]),
        )

    def test_extract_ssip_information_from_files(self):
        """Test to see if information is extracted as expected.
        """
        expected_keys = ["inchikey", "ssip_molecule"]
        actual_dict = solutexmlmaker.extract_ssip_information_from_files(
            [self.ssip_filename]
        )
        self.assertEqual(1, len(actual_dict))
        for inchikey, information_dict in actual_dict.items():
            self.assertEqual("XLYOFNOQVPJJNP-UHFFFAOYSA-N", inchikey)
            self.assertListEqual(expected_keys, sorted(information_dict.keys()))
            self.assertEqual(
                "XLYOFNOQVPJJNP-UHFFFAOYSA-N", information_dict["inchikey"]
            )
            self.assertMultiLineEqual(
                self.expected_ssip_list_xml + "\n",
                etree.tounicode(information_dict["ssip_molecule"], pretty_print=True),
            )
