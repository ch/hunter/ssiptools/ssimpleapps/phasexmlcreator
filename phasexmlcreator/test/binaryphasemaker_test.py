# -*- coding: utf-8 -*-
#    phasexmlcreator provides utilites to generate XML input for phasetransfer calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlcreator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Created on Thu Sep 19 10:28:42 2019

@author: mark
"""

import logging
import unittest
import os
import pathlib
from lxml import etree
import phasexmlcreator.binaryphasemaker as binphasemaker


logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


class BinaryPhaseMakerTestCase(unittest.TestCase):
    """Test case for binary phase maker tests.
    """

    def setUp(self):
        """Set up before tests.
        """
        self.maxDiff = None
        parent_directory = pathlib.Path(__file__).parents[0]
        self.solvent_filename = (
            (parent_directory / "resources" / "concentration_examples.csv")
            .absolute()
            .as_posix()
        )
        self.ssip_filename_list = [
            (parent_directory / "resources/LRHPLDYGYMQRHN-UHFFFAOYSA-N_ssip.xml")
            .absolute()
            .as_posix(),
            (parent_directory / "resources/XLYOFNOQVPJJNP-UHFFFAOYSA-N_ssip.xml")
            .absolute()
            .as_posix(),
        ]
        self.binary_phase_file = (
            (parent_directory / "resources/binary_phase.xml").absolute().as_posix()
        )
        self.pure_solvent_info = binphasemaker.read_and_merge_solvent_information(
            self.solvent_filename, self.ssip_filename_list
        )
        self.actual_filename = "actual_binary_phase.xml"
        self.temperature_info = [
            {"temperature_value": 298.0, "temperature_units": "KELVIN"}
        ]
        self.expected_water_xml = """<ssip:SSIPMolecule xmlns:cml="http://www.xml-cml.org/schema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ssip:ssipSoftwareVersion="2.2.0-SNAPSHOT" ssip:parameterVersion="1.0.0" xsi:schemaLocation="http://www.xml-cml.org/schema http://www-hunter.ch.cam.ac.uk/schema/cmlschema.xsd http://www-hunter.ch.cam.ac.uk/SSIP http://www-hunter.ch.cam.ac.uk/schema/SSIP.xsd">
    <cml:molecule ssip:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" cml:id="XLYOFNOQVPJJNP-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="O" cml:id="a1" cml:x3="0.0569236794019" cml:y3="0.53497425302" cml:z3="-9.99862395488E-5"/>
            <cml:atom cml:elementType="H" cml:id="a2" cml:x3="-0.735721920018" cml:y3="-0.0219270863225" cml:z3="-1.029173866E-4"/>
            <cml:atom cml:elementType="H" cml:id="a3" cml:x3="0.785198240616" cml:y3="-0.103847166698" cml:z3="-1.97096373822E-4"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a3" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    <ssip:SurfaceInformation>
        <ssip:TotalSurfaceArea>37.48483410829331</ssip:TotalSurfaceArea>
        <ssip:NegativeSurfaceArea>18.664358683092974</ssip:NegativeSurfaceArea>
        <ssip:PositiveSurfaceArea>18.82047542519977</ssip:PositiveSurfaceArea>
        <ssip:ElectronDensityIsosurface>0.002</ssip:ElectronDensityIsosurface>
        <ssip:NumberOFMEPSPoints>2286</ssip:NumberOFMEPSPoints>
    </ssip:SurfaceInformation>
    <ssip:SSIPs>
        <ssip:SSIP ssip:value="2.9387503903833574" ssip:nearestAtomID="a3" cml:x3="1.509794550767402" cml:y3="-0.8784982637871289" cml:z3="-1.6986589692899999E-4"/>
        <ssip:SSIP ssip:value="2.9275582734233065" ssip:nearestAtomID="a2" cml:x3="-1.4603207114038939" cml:y3="-0.790973405157027" cml:z3="0.12362427220238399"/>
        <ssip:SSIP ssip:value="-6.263074063139137" ssip:nearestAtomID="a1" cml:x3="0.156188781751346" cml:y3="1.6969169177862917" cml:z3="1.172598045109361"/>
        <ssip:SSIP ssip:value="-6.607684490829715" ssip:nearestAtomID="a1" cml:x3="0.06855491260794999" cml:y3="1.9788577934582507" cml:z3="-0.77762067563301"/>
    </ssip:SSIPs>
</ssip:SSIPMolecule>"""
        self.expected_butanol_xml = """<ssip:SSIPMolecule xmlns:cml="http://www.xml-cml.org/schema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" ssip:ssipSoftwareVersion="3.2.0" ssip:parameterVersion="1.0.0" xsi:schemaLocation="http://www.xml-cml.org/schema http://www-hunter.ch.cam.ac.uk/schema/cmlschema.xsd http://www-hunter.ch.cam.ac.uk/SSIP http://www-hunter.ch.cam.ac.uk/schema/SSIP.xsd">
    <cml:molecule ssip:stdInChIKey="LRHPLDYGYMQRHN-UHFFFAOYSA-N" cml:id="LRHPLDYGYMQRHN-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="C" cml:id="a1" cml:x3="0.453161473082" cml:y3="-1.03446801266" cml:z3="-0.0905306598548"/>
            <cml:atom cml:elementType="H" cml:id="a10" cml:x3="-0.711314642651" cml:y3="-1.77936986861" cml:z3="1.57182493824"/>
            <cml:atom cml:elementType="H" cml:id="a11" cml:x3="-2.35918900768" cml:y3="-0.976902587046" cml:z3="-0.14772459368"/>
            <cml:atom cml:elementType="H" cml:id="a12" cml:x3="-1.77474726333" cml:y3="-2.19616982792" cml:z3="-1.27745306344"/>
            <cml:atom cml:elementType="H" cml:id="a13" cml:x3="-2.519145554" cml:y3="-4.00559237167" cml:z3="0.302707946329"/>
            <cml:atom cml:elementType="H" cml:id="a14" cml:x3="-3.10209800172" cml:y3="-2.77368664521" cml:z3="1.42866821719"/>
            <cml:atom cml:elementType="H" cml:id="a15" cml:x3="-4.0901443513" cml:y3="-3.10664597908" cml:z3="-1.18904146578"/>
            <cml:atom cml:elementType="C" cml:id="a2" cml:x3="-0.564202390594" cml:y3="-2.0116665412" cml:z3="0.507612692817"/>
            <cml:atom cml:elementType="C" cml:id="a3" cml:x3="-1.92163762747" cml:y3="-1.98241261328" cml:z3="-0.206572876019"/>
            <cml:atom cml:elementType="C" cml:id="a4" cml:x3="-2.92693672032" cml:y3="-2.98225012861" cml:z3="0.367318952425"/>
            <cml:atom cml:elementType="O" cml:id="a5" cml:x3="-4.208010389" cml:y3="-2.9017666367" cml:z3="-0.248445606999"/>
            <cml:atom cml:elementType="H" cml:id="a6" cml:x3="0.655827908609" cml:y3="-1.26752859656" cml:z3="-1.14312376632"/>
            <cml:atom cml:elementType="H" cml:id="a7" cml:x3="0.0848553777946" cml:y3="-0.00239243828223" cml:z3="-0.0456709489122"/>
            <cml:atom cml:elementType="H" cml:id="a8" cml:x3="1.40666917559" cml:y3="-1.07214682201" cml:z3="0.448356319391"/>
            <cml:atom cml:elementType="H" cml:id="a9" cml:x3="-0.154987987004" cml:y3="-3.03150093117" cml:z3="0.472873914607"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a6" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a7" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a8" cml:order="1"/>
            <cml:bond cml:atomRefs2="a2 a10" cml:order="1"/>
            <cml:bond cml:atomRefs2="a2 a3" cml:order="1"/>
            <cml:bond cml:atomRefs2="a2 a9" cml:order="1"/>
            <cml:bond cml:atomRefs2="a3 a11" cml:order="1"/>
            <cml:bond cml:atomRefs2="a3 a12" cml:order="1"/>
            <cml:bond cml:atomRefs2="a3 a4" cml:order="1"/>
            <cml:bond cml:atomRefs2="a4 a13" cml:order="1"/>
            <cml:bond cml:atomRefs2="a4 a14" cml:order="1"/>
            <cml:bond cml:atomRefs2="a4 a5" cml:order="1"/>
            <cml:bond cml:atomRefs2="a5 a15" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    <ssip:SurfaceInformation>
        <ssip:TotalSurfaceArea>122.2095781704891</ssip:TotalSurfaceArea>
        <ssip:NegativeSurfaceArea>20.900319182402814</ssip:NegativeSurfaceArea>
        <ssip:PositiveSurfaceArea>101.30925898808572</ssip:PositiveSurfaceArea>
        <ssip:ElectronDensityIsosurface>0.002</ssip:ElectronDensityIsosurface>
        <ssip:NumberOFMEPSPoints>9580</ssip:NumberOFMEPSPoints>
        <ssip:VdWVolume>99.3920085615431</ssip:VdWVolume>
    </ssip:SurfaceInformation>
    <ssip:SSIPs>
        <ssip:SSIP ssip:value="2.784040024744081" ssip:nearestAtomID="a15" cml:x3="-3.8161781356582187" cml:y3="-3.342557618498982" cml:z3="-2.192649106294994"/>
        <ssip:SSIP ssip:value="0.773639545237379" ssip:nearestAtomID="a12" cml:x3="-2.239105576984704" cml:y3="-3.099174713898159" cml:z3="-2.0252301249653697"/>
        <ssip:SSIP ssip:value="0.5071846345423165" ssip:nearestAtomID="a13" cml:x3="-2.8499750929763277" cml:y3="-4.524230524251444" cml:z3="-0.8462517897648159"/>
        <ssip:SSIP ssip:value="0.4784372492163057" ssip:nearestAtomID="a6" cml:x3="0.9270005337214728" cml:y3="-1.649107340870889" cml:z3="-2.251805301783455"/>
        <ssip:SSIP ssip:value="0.46383776537503163" ssip:nearestAtomID="a9" cml:x3="0.136470579099108" cml:y3="-4.205322913496092" cml:z3="0.352187567944962"/>
        <ssip:SSIP ssip:value="0.4557099985213939" ssip:nearestAtomID="a8" cml:x3="2.420223369759191" cml:y3="-1.188501408973558" cml:z3="1.0796321860050409"/>
        <ssip:SSIP ssip:value="0.4355872186992693" ssip:nearestAtomID="a7" cml:x3="-0.30271267022695597" cml:y3="1.1313523827905538" cml:z3="0.031343697635518995"/>
        <ssip:SSIP ssip:value="0.43429343831228867" ssip:nearestAtomID="a14" cml:x3="-2.937811637006091" cml:y3="-2.45801773806002" cml:z3="2.5781764284587028"/>
        <ssip:SSIP ssip:value="0.33435822085360595" ssip:nearestAtomID="a11" cml:x3="-2.5158938537831497" cml:y3="0.207568717565752" cml:z3="0.059691722864448994"/>
        <ssip:SSIP ssip:value="0.0" ssip:nearestAtomID="a3" cml:x3="-1.4487933333328935" cml:y3="-2.074966666667215" cml:z3="0.05005333333293337"/>
        <ssip:SSIP ssip:value="0.0" ssip:nearestAtomID="a3" cml:x3="-1.4487933333328935" cml:y3="-2.074966666667215" cml:z3="0.05005333333293337"/>
        <ssip:SSIP ssip:value="-5.3386085001748205" ssip:nearestAtomID="a5" cml:x3="-5.4850745889327115" cml:y3="-3.640825199834338" cml:z3="0.48092898414517593"/>
        <ssip:SSIP ssip:value="-5.365217010700665" ssip:nearestAtomID="a5" cml:x3="-5.353424285394745" cml:y3="-1.7849561367024218" cml:z3="0.11271581239149799"/>
    </ssip:SSIPs>
</ssip:SSIPMolecule>"""
        self.expected_mixturecollection_xml = """<phase:MixtureCollection xmlns:phase="http://www-hunter.ch.cam.ac.uk/PhaseSchema" xmlns:ssip="http://www-hunter.ch.cam.ac.uk/SSIP" xmlns:cml="http://www.xml-cml.org/schema">
  <phase:Mixtures>
    <phase:Mixture phase:soluteID="298.000KELVIN">
      <phase:Phases>
        <phase:Phase phase:solventID="1-butanol0.500000000water298.000KELVIN" phase:units="MOLAR">
          <phase:Molecules>
            <phase:Molecule phase:stdInChIKey="LRHPLDYGYMQRHN-UHFFFAOYSA-N" phase:moleculeID="LRHPLDYGYMQRHN-UHFFFAOYSA-N" phase:moleFraction="0.500000000"><cml:molecule ssip:stdInChIKey="LRHPLDYGYMQRHN-UHFFFAOYSA-N" cml:id="LRHPLDYGYMQRHN-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="C" cml:id="a1" cml:x3="0.453161473082" cml:y3="-1.03446801266" cml:z3="-0.0905306598548"/>
            <cml:atom cml:elementType="H" cml:id="a10" cml:x3="-0.711314642651" cml:y3="-1.77936986861" cml:z3="1.57182493824"/>
            <cml:atom cml:elementType="H" cml:id="a11" cml:x3="-2.35918900768" cml:y3="-0.976902587046" cml:z3="-0.14772459368"/>
            <cml:atom cml:elementType="H" cml:id="a12" cml:x3="-1.77474726333" cml:y3="-2.19616982792" cml:z3="-1.27745306344"/>
            <cml:atom cml:elementType="H" cml:id="a13" cml:x3="-2.519145554" cml:y3="-4.00559237167" cml:z3="0.302707946329"/>
            <cml:atom cml:elementType="H" cml:id="a14" cml:x3="-3.10209800172" cml:y3="-2.77368664521" cml:z3="1.42866821719"/>
            <cml:atom cml:elementType="H" cml:id="a15" cml:x3="-4.0901443513" cml:y3="-3.10664597908" cml:z3="-1.18904146578"/>
            <cml:atom cml:elementType="C" cml:id="a2" cml:x3="-0.564202390594" cml:y3="-2.0116665412" cml:z3="0.507612692817"/>
            <cml:atom cml:elementType="C" cml:id="a3" cml:x3="-1.92163762747" cml:y3="-1.98241261328" cml:z3="-0.206572876019"/>
            <cml:atom cml:elementType="C" cml:id="a4" cml:x3="-2.92693672032" cml:y3="-2.98225012861" cml:z3="0.367318952425"/>
            <cml:atom cml:elementType="O" cml:id="a5" cml:x3="-4.208010389" cml:y3="-2.9017666367" cml:z3="-0.248445606999"/>
            <cml:atom cml:elementType="H" cml:id="a6" cml:x3="0.655827908609" cml:y3="-1.26752859656" cml:z3="-1.14312376632"/>
            <cml:atom cml:elementType="H" cml:id="a7" cml:x3="0.0848553777946" cml:y3="-0.00239243828223" cml:z3="-0.0456709489122"/>
            <cml:atom cml:elementType="H" cml:id="a8" cml:x3="1.40666917559" cml:y3="-1.07214682201" cml:z3="0.448356319391"/>
            <cml:atom cml:elementType="H" cml:id="a9" cml:x3="-0.154987987004" cml:y3="-3.03150093117" cml:z3="0.472873914607"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a6" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a7" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a8" cml:order="1"/>
            <cml:bond cml:atomRefs2="a2 a10" cml:order="1"/>
            <cml:bond cml:atomRefs2="a2 a3" cml:order="1"/>
            <cml:bond cml:atomRefs2="a2 a9" cml:order="1"/>
            <cml:bond cml:atomRefs2="a3 a11" cml:order="1"/>
            <cml:bond cml:atomRefs2="a3 a12" cml:order="1"/>
            <cml:bond cml:atomRefs2="a3 a4" cml:order="1"/>
            <cml:bond cml:atomRefs2="a4 a13" cml:order="1"/>
            <cml:bond cml:atomRefs2="a4 a14" cml:order="1"/>
            <cml:bond cml:atomRefs2="a4 a5" cml:order="1"/>
            <cml:bond cml:atomRefs2="a5 a15" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    <ssip:SurfaceInformation>
        <ssip:TotalSurfaceArea>122.2095781704891</ssip:TotalSurfaceArea>
        <ssip:NegativeSurfaceArea>20.900319182402814</ssip:NegativeSurfaceArea>
        <ssip:PositiveSurfaceArea>101.30925898808572</ssip:PositiveSurfaceArea>
        <ssip:ElectronDensityIsosurface>0.002</ssip:ElectronDensityIsosurface>
        <ssip:NumberOFMEPSPoints>9580</ssip:NumberOFMEPSPoints>
        <ssip:VdWVolume>99.3920085615431</ssip:VdWVolume>
    </ssip:SurfaceInformation>
    <phase:SSIPs phase:units="MOLAR"><phase:SSIP ssip:value="2.784040024744081" ssip:nearestAtomID="a15" cml:x3="-3.8161781356582187" cml:y3="-3.342557618498982" cml:z3="-2.192649106294994" phase:moleculeID="LRHPLDYGYMQRHN-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">10.9000000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="0.773639545237379" ssip:nearestAtomID="a12" cml:x3="-2.239105576984704" cml:y3="-3.099174713898159" cml:z3="-2.0252301249653697" phase:moleculeID="LRHPLDYGYMQRHN-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">10.9000000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="0.5071846345423165" ssip:nearestAtomID="a13" cml:x3="-2.8499750929763277" cml:y3="-4.524230524251444" cml:z3="-0.8462517897648159" phase:moleculeID="LRHPLDYGYMQRHN-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">10.9000000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="0.4784372492163057" ssip:nearestAtomID="a6" cml:x3="0.9270005337214728" cml:y3="-1.649107340870889" cml:z3="-2.251805301783455" phase:moleculeID="LRHPLDYGYMQRHN-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">10.9000000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="0.46383776537503163" ssip:nearestAtomID="a9" cml:x3="0.136470579099108" cml:y3="-4.205322913496092" cml:z3="0.352187567944962" phase:moleculeID="LRHPLDYGYMQRHN-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">10.9000000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="0.4557099985213939" ssip:nearestAtomID="a8" cml:x3="2.420223369759191" cml:y3="-1.188501408973558" cml:z3="1.0796321860050409" phase:moleculeID="LRHPLDYGYMQRHN-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">10.9000000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="0.4355872186992693" ssip:nearestAtomID="a7" cml:x3="-0.30271267022695597" cml:y3="1.1313523827905538" cml:z3="0.031343697635518995" phase:moleculeID="LRHPLDYGYMQRHN-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">10.9000000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="0.43429343831228867" ssip:nearestAtomID="a14" cml:x3="-2.937811637006091" cml:y3="-2.45801773806002" cml:z3="2.5781764284587028" phase:moleculeID="LRHPLDYGYMQRHN-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">10.9000000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="0.33435822085360595" ssip:nearestAtomID="a11" cml:x3="-2.5158938537831497" cml:y3="0.207568717565752" cml:z3="0.059691722864448994" phase:moleculeID="LRHPLDYGYMQRHN-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">10.9000000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="0.0" ssip:nearestAtomID="a3" cml:x3="-1.4487933333328935" cml:y3="-2.074966666667215" cml:z3="0.05005333333293337" phase:moleculeID="LRHPLDYGYMQRHN-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">10.9000000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="0.0" ssip:nearestAtomID="a3" cml:x3="-1.4487933333328935" cml:y3="-2.074966666667215" cml:z3="0.05005333333293337" phase:moleculeID="LRHPLDYGYMQRHN-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">10.9000000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-5.3386085001748205" ssip:nearestAtomID="a5" cml:x3="-5.4850745889327115" cml:y3="-3.640825199834338" cml:z3="0.48092898414517593" phase:moleculeID="LRHPLDYGYMQRHN-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">10.9000000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-5.365217010700665" ssip:nearestAtomID="a5" cml:x3="-5.353424285394745" cml:y3="-1.7849561367024218" cml:z3="0.11271581239149799" phase:moleculeID="LRHPLDYGYMQRHN-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">10.9000000000</phase:TotalConcentration></phase:SSIP></phase:SSIPs></phase:Molecule>
            <phase:Molecule phase:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:moleFraction="0.500000000"><cml:molecule ssip:stdInChIKey="XLYOFNOQVPJJNP-UHFFFAOYSA-N" cml:id="XLYOFNOQVPJJNP-UHFFFAOYSA-N">
        <cml:atomArray>
            <cml:atom cml:elementType="O" cml:id="a1" cml:x3="0.0569236794019" cml:y3="0.53497425302" cml:z3="-9.99862395488E-5"/>
            <cml:atom cml:elementType="H" cml:id="a2" cml:x3="-0.735721920018" cml:y3="-0.0219270863225" cml:z3="-1.029173866E-4"/>
            <cml:atom cml:elementType="H" cml:id="a3" cml:x3="0.785198240616" cml:y3="-0.103847166698" cml:z3="-1.97096373822E-4"/>
        </cml:atomArray>
        <cml:bondArray>
            <cml:bond cml:atomRefs2="a1 a2" cml:order="1"/>
            <cml:bond cml:atomRefs2="a1 a3" cml:order="1"/>
        </cml:bondArray>
    </cml:molecule>
    <ssip:SurfaceInformation>
        <ssip:TotalSurfaceArea>37.48483410829331</ssip:TotalSurfaceArea>
        <ssip:NegativeSurfaceArea>18.664358683092974</ssip:NegativeSurfaceArea>
        <ssip:PositiveSurfaceArea>18.82047542519977</ssip:PositiveSurfaceArea>
        <ssip:ElectronDensityIsosurface>0.002</ssip:ElectronDensityIsosurface>
        <ssip:NumberOFMEPSPoints>2286</ssip:NumberOFMEPSPoints>
    </ssip:SurfaceInformation>
    <phase:SSIPs phase:units="MOLAR"><phase:SSIP ssip:value="2.9387503903833574" ssip:nearestAtomID="a3" cml:x3="1.509794550767402" cml:y3="-0.8784982637871289" cml:z3="-1.6986589692899999E-4" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="2.9275582734233065" ssip:nearestAtomID="a2" cml:x3="-1.4603207114038939" cml:y3="-0.790973405157027" cml:z3="0.12362427220238399" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.263074063139137" ssip:nearestAtomID="a1" cml:x3="0.156188781751346" cml:y3="1.6969169177862917" cml:z3="1.172598045109361" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP><phase:SSIP ssip:value="-6.607684490829715" ssip:nearestAtomID="a1" cml:x3="0.06855491260794999" cml:y3="1.9788577934582507" cml:z3="-0.77762067563301" phase:moleculeID="XLYOFNOQVPJJNP-UHFFFAOYSA-N" phase:units="MOLAR"><phase:TotalConcentration phase:units="MOLAR">55.3500000000</phase:TotalConcentration></phase:SSIP></phase:SSIPs></phase:Molecule>
          </phase:Molecules>
          <phase:Temperature phase:units="KELVIN">298.000</phase:Temperature>
        </phase:Phase>
      </phase:Phases>
    </phase:Mixture>
  </phase:Mixtures>
</phase:MixtureCollection>
"""

    def tearDown(self):
        """
        """
        del self.solvent_filename
        del self.ssip_filename_list
        del self.expected_butanol_xml
        del self.expected_mixturecollection_xml
        del self.expected_water_xml
        if os.path.isfile(self.actual_filename):
            os.remove(self.actual_filename)
        del self.actual_filename

    def test_write_mixture_collection_element_to_file(self):
        """Test to see if collection element writeen to file as expected.
        """
        input_xml = binphasemaker.read_and_generate_mixture_collection_element(
            self.solvent_filename, self.ssip_filename_list, [0.5], self.temperature_info
        )
        binphasemaker.write_mixture_collection_element_to_file(
            input_xml, self.actual_filename
        )
        with open(self.actual_filename, "r") as actual_file:
            actual_xml = actual_file.read()
            with open(self.binary_phase_file) as expected_file:
                expected_xml = expected_file.read()
                self.assertMultiLineEqual(expected_xml, actual_xml)

    def test_read_and_generate_mixture_collection_element(self):
        """Test to see if expected collection element created.
        """
        actual_xml = binphasemaker.read_and_generate_mixture_collection_element(
            self.solvent_filename, self.ssip_filename_list, [0.5], self.temperature_info
        )
        self.assertMultiLineEqual(
            self.expected_mixturecollection_xml,
            etree.tounicode(actual_xml, pretty_print=True),
        )

    def test_generate_mixture_collection_element(self):
        """Test to see if expected element created.
        """
        actual_information_list = binphasemaker.generate_binary_phase_information(
            self.pure_solvent_info[0], self.pure_solvent_info[1], [0.5]
        )
        actual_xml = binphasemaker.generate_mixture_collection_element(
            actual_information_list, self.temperature_info
        )
        self.assertMultiLineEqual(
            self.expected_mixturecollection_xml,
            etree.tounicode(actual_xml, pretty_print=True),
        )

    def test_read_and_merge_solvent_information(self):
        """Test to see if information read and merged correctly
        """
        self.assertEqual(2, len(self.pure_solvent_info))
        LOGGER.debug(self.pure_solvent_info)
        expected_key_list = ["solvent_id", "solvent_name", "ssip_info_list"]
        expected_keys = [
            "concentration_value",
            "inchikey",
            "mole_fraction",
            "ssip_molecule",
        ]
        for solvent_info in self.pure_solvent_info:
            self.assertListEqual(expected_key_list, sorted(solvent_info.keys()))
            if solvent_info["solvent_id"] == "water":
                self.assertEqual("water", solvent_info["solvent_name"])
                for information_dict in solvent_info["ssip_info_list"]:
                    self.assertListEqual(expected_keys, sorted(information_dict.keys()))
                    self.assertEqual(
                        "XLYOFNOQVPJJNP-UHFFFAOYSA-N", information_dict["inchikey"]
                    )
                    self.assertAlmostEqual(
                        55.35, information_dict["concentration_value"]
                    )
                    self.assertAlmostEqual(1.0, information_dict["mole_fraction"])
                    self.assertMultiLineEqual(
                        self.expected_water_xml,
                        etree.tounicode(information_dict["ssip_molecule"]),
                    )
            else:
                self.assertEqual("1-butanol", solvent_info["solvent_name"])
                self.assertEqual("1-butanol", solvent_info["solvent_id"])
                for information_dict in solvent_info["ssip_info_list"]:
                    self.assertListEqual(expected_keys, sorted(information_dict.keys()))
                    self.assertEqual(
                        "LRHPLDYGYMQRHN-UHFFFAOYSA-N", information_dict["inchikey"]
                    )
                    self.assertAlmostEqual(
                        10.9, information_dict["concentration_value"]
                    )
                    self.assertAlmostEqual(1.0, information_dict["mole_fraction"])
                    self.assertMultiLineEqual(
                        self.expected_butanol_xml,
                        etree.tounicode(information_dict["ssip_molecule"]),
                    )

    def test_generate_binary_phase_information(self):
        """Test to see if expected information generated from list of compositions.
        """
        expected_key_list = ["solvent_id", "solvent_name", "ssip_info_list"]
        expected_keys = [
            "concentration_value",
            "inchikey",
            "mole_fraction",
            "ssip_molecule",
        ]
        actual_information_list = binphasemaker.generate_binary_phase_information(
            self.pure_solvent_info[0], self.pure_solvent_info[1], [0.5]
        )
        self.assertEqual(1, len(actual_information_list))
        actual_information = actual_information_list[0]
        self.assertListEqual(expected_key_list, sorted(actual_information.keys()))
        self.assertEqual("1-butanol0.500000000water", actual_information["solvent_id"])
        self.assertEqual("1-butanol0.500000000water", actual_information["solvent_name"])
        actual_list = actual_information["ssip_info_list"]
        self.assertEqual(2, len(actual_list))
        information_dict = actual_list[0]
        self.assertListEqual(expected_keys, sorted(information_dict.keys()))
        self.assertEqual("LRHPLDYGYMQRHN-UHFFFAOYSA-N", information_dict["inchikey"])
        self.assertAlmostEqual(10.9, information_dict["concentration_value"])
        self.assertAlmostEqual(0.5, information_dict["mole_fraction"])
        self.assertMultiLineEqual(
            self.expected_butanol_xml,
            etree.tounicode(information_dict["ssip_molecule"]),
        )
        information_dict = actual_list[1]
        self.assertListEqual(expected_keys, sorted(information_dict.keys()))
        self.assertEqual("XLYOFNOQVPJJNP-UHFFFAOYSA-N", information_dict["inchikey"])
        self.assertAlmostEqual(55.35, information_dict["concentration_value"])
        self.assertAlmostEqual(0.5, information_dict["mole_fraction"])
        self.assertMultiLineEqual(
            self.expected_water_xml, etree.tounicode(information_dict["ssip_molecule"])
        )

    def test_create_binary_phase_information(self):
        """Test to see if expected binary information created.
        """
        expected_key_list = ["solvent_id", "solvent_name", "ssip_info_list"]
        expected_keys = [
            "concentration_value",
            "inchikey",
            "mole_fraction",
            "ssip_molecule",
        ]
        actual_information = binphasemaker.create_binary_phase_information(
            self.pure_solvent_info[0], self.pure_solvent_info[1], 0.5
        )
        self.assertListEqual(expected_key_list, sorted(actual_information.keys()))
        self.assertEqual("1-butanol0.500000000water", actual_information["solvent_id"])
        self.assertEqual("1-butanol0.500000000water", actual_information["solvent_name"])
        actual_list = actual_information["ssip_info_list"]
        self.assertEqual(2, len(actual_list))
        information_dict = actual_list[0]
        self.assertListEqual(expected_keys, sorted(information_dict.keys()))
        self.assertEqual("LRHPLDYGYMQRHN-UHFFFAOYSA-N", information_dict["inchikey"])
        self.assertAlmostEqual(10.9, information_dict["concentration_value"])
        self.assertAlmostEqual(0.5, information_dict["mole_fraction"])
        self.assertMultiLineEqual(
            self.expected_butanol_xml,
            etree.tounicode(information_dict["ssip_molecule"]),
        )
        information_dict = actual_list[1]
        self.assertListEqual(expected_keys, sorted(information_dict.keys()))
        self.assertEqual("XLYOFNOQVPJJNP-UHFFFAOYSA-N", information_dict["inchikey"])
        self.assertAlmostEqual(55.35, information_dict["concentration_value"])
        self.assertAlmostEqual(0.5, information_dict["mole_fraction"])
        self.assertMultiLineEqual(
            self.expected_water_xml, etree.tounicode(information_dict["ssip_molecule"])
        )

    def test_generate_solvent_ssip_information(self):
        """Test to see if expeccted information is generated.
        """
        expected_keys = [
            "concentration_value",
            "inchikey",
            "mole_fraction",
            "ssip_molecule",
        ]
        actual_list = binphasemaker.generate_solvent_ssip_information(
            self.pure_solvent_info[0]["ssip_info_list"][0],
            self.pure_solvent_info[1]["ssip_info_list"][0],
            0.5,
            0.5,
        )
        self.assertEqual(2, len(actual_list))
        information_dict = actual_list[0]
        self.assertListEqual(expected_keys, sorted(information_dict.keys()))
        self.assertEqual("LRHPLDYGYMQRHN-UHFFFAOYSA-N", information_dict["inchikey"])
        self.assertAlmostEqual(10.9, information_dict["concentration_value"])
        self.assertAlmostEqual(0.5, information_dict["mole_fraction"])
        self.assertMultiLineEqual(
            self.expected_butanol_xml,
            etree.tounicode(information_dict["ssip_molecule"]),
        )
        information_dict = actual_list[1]
        self.assertListEqual(expected_keys, sorted(information_dict.keys()))
        self.assertEqual("XLYOFNOQVPJJNP-UHFFFAOYSA-N", information_dict["inchikey"])
        self.assertAlmostEqual(55.35, information_dict["concentration_value"])
        self.assertAlmostEqual(0.5, information_dict["mole_fraction"])
        self.assertMultiLineEqual(
            self.expected_water_xml, etree.tounicode(information_dict["ssip_molecule"])
        )
