# -*- coding: utf-8 -*-
#    phasexmlcreator provides utilites to generate XML input for phasetransfer calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlcreator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
script for generating solute entries, and outputting a file.
default concentration of 0.001M is used for all.

@author: mark
"""

import logging
import phasexmlcreator.ssipfilereader as ssipfilereader
import phasexmlcreator.solutexmlcreator as solutexmlcreator

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)


def write_ssip_lists_to_solute(ssip_filename_list, concentration_value, filename):
    """This reads in SSIPLists, and then outputs the SoluteList.
    """
    solute_list_element = generate_solute_list_element_from_filename_list(
        ssip_filename_list, concentration_value
    )
    write_solute_list_element_to_file(solute_list_element, filename)


def write_solute_list_element_to_file(solute_list_element, filename):
    """Function to write the element list to file.
    """
    element_tree = solutexmlcreator.create_element_tree(solute_list_element)
    solutexmlcreator.write_element_tree_to_file(element_tree, filename)


def generate_solute_list_element_from_filename_list(filename_list, concentration_value):
    """This generates solute list element.
    """
    ssip_info_dict_dict = extract_ssip_information_append_concentration(
        filename_list, concentration_value
    )
    return generate_solute_list_element_from_dict(ssip_info_dict_dict)


def generate_solute_list_element_from_dict(ssip_info_dict_dict):
    """This generates the solute list element.
    """
    ssip_info_dict_list = get_sorted_ssip_info_list(ssip_info_dict_dict)
    return generate_solute_list_element(ssip_info_dict_list)


def generate_solute_list_element(ssip_info_dict_list):
    """This generates solute list element.
    """
    return solutexmlcreator.generate_solute_list_element(ssip_info_dict_list)


def get_sorted_ssip_info_list(ssip_info_dict_dict):
    """This returns a list of the SSIP infor dictionaries, with the order based on the InChIKey..
    """
    sorted_inchikeys = sorted(ssip_info_dict_dict.keys())
    ssip_info_dict_list = []
    for inchikey in sorted_inchikeys:
        ssip_info_dict_list.append(ssip_info_dict_dict[inchikey])
    return ssip_info_dict_list


def extract_ssip_information_append_concentration(filename_list, concentration_value):
    """This returns the ssip_info_dict_dict with the concentrations appended.
    """
    ssip_info_dict_dict = extract_ssip_information_from_files(filename_list)
    append_concentrations_to_all_ssip_list_info(
        ssip_info_dict_dict, concentration_value
    )
    return ssip_info_dict_dict


def append_concentrations_to_all_ssip_list_info(
    ssip_info_dict_dict, concentration_value
):
    """This appends the same concentration to each info dict.
    """
    for inchikey, ssip_info_dict in ssip_info_dict_dict.items():
        append_concentration_to_ssip_list_info(ssip_info_dict, concentration_value)


def append_concentration_to_ssip_list_info(ssip_info_dict, concentration_value):
    """This appends concentration to a ssip_info_dict.
    """
    ssip_info_dict["concentration_value"] = concentration_value


def extract_ssip_information_from_files(filename_list):
    """Read in the ssip lists and inchikeys.
    """
    return ssipfilereader.extract_ssip_information_from_files(filename_list)
