# -*- coding: utf-8 -*-
#    phasexmlcreator provides utilites to generate XML input for phasetransfer calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlcreator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
CLI for phase XML creation. This works for solute creation, and also pure solvents.
For phase or higher order solvent systems, you need to write a small script.

@author: mark
"""

import logging
import argparse
import glob
import phasexmlcreator.solutexmlmaker as solutecreator
import phasexmlcreator.solventxmlmaker as solventcreator

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


def process_args(args):
    """This processes the arguments. The function has been set using defaults
    for the subparsers, so should be called automatically.
    """
    return args.func(args)


def process_solutemaking(args):
    """This processes arguments for solute file creation.
    """
    ssip_filename_list = glob.glob(args.ssip_files)
    solute_concentration = args.conc
    solute_output_file = args.output
    return solutecreator.write_ssip_lists_to_solute(
        ssip_filename_list, solute_concentration, solute_output_file
    )


def process_solventmaking(args):
    """This processes arguments for solvent file creation.
    """
    ssip_filename_list = glob.glob(args.ssip_files)
    solvent_information = args.solventinfo
    solvent_output_file = args.output
    return solventcreator.read_solvent_information_write_to_xml(
        solvent_information, ssip_filename_list, solvent_output_file
    )


def create_argparser():
    """This creates the arg parser for the command line.
    """
    description = """This CLI allows for simple creation of solute or solvent XML input.

"""
    parser = argparse.ArgumentParser(description=description)
    sub_description = """There are two CLI options:
        1) Solute creation
        2) Solvent creation (for pure solvents)"""
    subparsers = parser.add_subparsers(
        title="Sub methods", description=sub_description, help=""
    )
    # sub parser for solvent making.
    solvent_description = """This generates solvent input for pure solvents."""
    solvent_parser = subparsers.add_parser(
        "solvent", description=solvent_description, help="used to call solventmaker."
    )
    solvent_parser.add_argument("--ssip_files", type=str, help="regex for ssip files.")
    solvent_parser.add_argument(
        "--solventinfo",
        type=str,
        default=solventcreator.DEFAULT_SOLVENT_INFORMATION_FILE,
        help="solvent information, defaults to values in file in repository.",
    )
    solvent_parser.add_argument(
        "-o", "--output", type=str, help="output filename for file."
    )
    solvent_parser.set_defaults(func=process_solventmaking)
    # sub parser for solute making
    solute_description = """This generates solute XML input"""
    solute_parser = subparsers.add_parser(
        "solute", description=solute_description, help="Used to call solutemalker."
    )
    solute_parser.add_argument("--ssip_files", type=str, help="regex for ssip files.")
    solute_parser.add_argument(
        "-c", "--conc", type=float, default=0.001, help="concentration (DEFAULT: 0.001)"
    )
    solute_parser.add_argument(
        "-o", "--output", type=str, help="output filename for file."
    )
    solute_parser.set_defaults(func=process_solutemaking)
    return parser


def main():
    """Main function of script. Creates parser, reads args and then processes them.
    """
    # Create parser
    parser = create_argparser()
    # parse args
    args = parser.parse_args()
    return process_args(args)
