# -*- coding: utf-8 -*-
#    phasexmlcreator provides utilites to generate XML input for phasetransfer calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlcreator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for setting up solvent mixtures with more than two solvent molecules.

This requires all volume fractions of molecules to be explicitly stated in input.

@author: mark
"""

import logging
import numpy as np
import phasexmlcreator.solventxmlmaker as solventxmlmaker
from phasexmlcreator.formattingconstants import VOLUME_FRACTION_FORMAT

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


def write_solvent_list_element_to_file(solvent_list_element, filename):
    """Write the solvent list element to file.

    Parameters
    ----------
    solvent_list_element : lxml.etree.Etree
        element tree to be written.
    filename : str
        filename for output location.

    Returns
    -------
    None.

    """
    solventxmlmaker.write_solvent_list_element_to_file(solvent_list_element, filename)


def read_and_generate_solvent_list_element(
    solvent_filename, ssip_filename_list, volume_frac_dict_list
):
    """Read pure information and generate SoluteList element for mixtures.

    Parameters
    ----------
    solvent_filename : str
        filename for pure concentration information.
    ssip_filename_list : list of str
        list of SSIP filenames.
    volume_frac_dict_list : list of dicts of str:float pairs.
        list of different volume fraction lists.

    Returns
    -------
    lxml.etree.Etree
        element tree of the SolventList element.

    """
    pure_solvent_information = read_and_merge_solvent_information(
        solvent_filename, ssip_filename_list
    )
    solvent_information_list = create_solvent_information_list(
        pure_solvent_information, volume_frac_dict_list
    )
    return generate_solvent_list_element(solvent_information_list)


def generate_solvent_list_element(solvent_information_list):
    """This generates a SolventList element.
    """
    return solventxmlmaker.generate_solvent_list_element(solvent_information_list)


def create_solvent_information_list(solvent_info_dict, volume_frac_dict_list):
    """This creates solvent information entries for each set of volume fractions.
    """
    solvent_information_list = []
    for volume_frac_dict in volume_frac_dict_list:
        solvent_information_list.append(
            create_solvent_information(solvent_info_dict, volume_frac_dict)
        )
    return solvent_information_list


def create_solvent_information(solvent_info_dict, volume_frac_dict):
    """This creates the solvent information entry for the given volume fractions and input data.
    """
    (
        solvent_ssip_info_list,
        solvent_name_list,
    ) = get_ordered_solvent_ssip_list_and_solvent_names(solvent_info_dict)
    volume_frac_array = create_volume_fraction_array(
        volume_frac_dict, solvent_name_list
    )
    solvent_ssip_info_list = generate_solvent_ssip_information(
        solvent_ssip_info_list, volume_frac_array
    )
    solvent_name = create_solvent_name(solvent_name_list, volume_frac_array)
    return {
        "solvent_id": solvent_name,
        "solvent_name": solvent_name,
        "ssip_info_list": solvent_ssip_info_list,
    }


def create_solvent_name(solvent_list, volume_fraction_list):
    """This creates a solvent name following the pattern "[solvent][volume fraction]"
    for all solvents in mixture.
    """
    composite_name = ""
    for i in range(len(solvent_list)):
        composite_name += ("{:s}" + VOLUME_FRACTION_FORMAT).format(solvent_list[i], volume_fraction_list[i])
    return composite_name


def create_volume_fraction_array(volume_frac_dict, solvent_list):
    """This creates the volume fraction array required.
    """
    volume_list = []
    for solvent in solvent_list:
        volume_list.append(volume_frac_dict[solvent])
    return np.array(volume_list)


def read_and_merge_solvent_information(solvent_filename, ssip_filename_list):
    """This reads in the information about the solvents that make up the
    system.
    """
    return solventxmlmaker.read_and_merge_solvent_information(
        solvent_filename, ssip_filename_list
    )


def generate_solvent_ssip_information(solvent_info_list, volume_fractions):
    """This generates the molecule entries for a solvent for the two solvents
    using the volume fraction.
    """
    pure_concentrations = get_concentration_array(solvent_info_list)
    concentrations = calculate_concentrations(pure_concentrations, volume_fractions)
    molefractions = calculate_molefractions(pure_concentrations, volume_fractions)
    set_mole_fractions(solvent_info_list, molefractions)
    set_concentrations(solvent_info_list, concentrations)
    # this removes any zero entries from the list.
    elements_to_remove = []
    # assign to temporary list, before poping to avoid any indexing errors.
    for i in range(len(solvent_info_list)):
        if solvent_info_list[i]["mole_fraction"] <= 0.00001:
            elements_to_remove.append(i)
    elements_to_remove = sorted(elements_to_remove)
    for i in range(len(elements_to_remove)):
        solvent_info_list.pop(elements_to_remove.pop())
    return solvent_info_list


def set_mole_fractions(solvent_info_list, molefractions):
    """This sets the mole fractions.
    """
    for i in range(len(molefractions)):
        solvent_info_list[i]["mole_fraction"] = molefractions[i]


def set_concentrations(solvent_info_list, concentrations):
    """This sets the concentrations
    """
    for i in range(len(concentrations)):
        solvent_info_list[i]["concentration_value"] = concentrations[i]


def calculate_concentrations(pure_concentrations, volume_fractions):
    """This calculates the concentrations for the components in the solvent
    mixture.
    """
    return np.multiply(pure_concentrations, volume_fractions)


def get_concentration_array(solvent_info_list):
    """Get concentrations from the molecules.

    Parameters
    ----------
    solvent_info_list : list of dicts
        Solvent information list.

    Returns
    -------
    np.array
        concentration array.

    """
    concentration_list = []
    for solvent_info in solvent_info_list:
        concentration_list.append(solvent_info["concentration_value"])
    return np.array(concentration_list)


def get_ordered_solvent_ssip_list_and_solvent_names(solvent_info_dict):
    """Get ordered solvent ssip information and also the solvent names.

    Parameters
    ----------
    solvent_info_dict : dict
        dictionary of solvent info inchikey:solventinfo pairs.

    Returns
    -------
    solvent_info_list : list
        solvent info list.
    solvent_name_list : list
        solvent name.

    """
    solvent_info_list = []
    solvent_name_list = []
    solvent_inchikeys = sorted(solvent_info_dict.keys())
    for i in range(len(solvent_inchikeys)):
        solvent_info_list.append(
            solvent_info_dict[solvent_inchikeys[i]]["ssip_info_list"][0]
        )
        solvent_name_list.append(
            solvent_info_dict[solvent_inchikeys[i]]["solvent_name"]
        )
    return solvent_info_list, solvent_name_list


def calculate_molefractions(concentrations, volume_fractions):
    """Calculate the molefractions for each species.
    
    Based on knowing concentrations and volume fractions of each component.

    Parameters
    ----------
    concentrations : np.array
        concentrations of species in phase.
    volume_fractions : np.array
        volume fractions of species in phase.

    Returns
    -------
    np.array
        Array of mole fractions.

    """
    total_concentrations = concentrations.dot(volume_fractions)
    return np.divide(
        np.multiply(concentrations, volume_fractions), total_concentrations
    )
