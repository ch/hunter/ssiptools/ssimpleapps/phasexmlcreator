# -*- coding: utf-8 -*-
#    phasexmlcreator provides utilites to generate XML input for phasetransfer calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlcreator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for assembling multi component information.

Used to create either phase or solvent XML.

@author: mark
"""

import logging
import copy
import numpy as np
import phasexmlcreator.solventconcentrationreader as solvconcreader
from phasexmlcreator.formattingconstants import MOLEFRACTION_FORMAT, MOLEFRACTION_ERROR

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.WARN)


def generate_solv_info_from_files(mole_fraction_dict_list, solvent_filename, ssip_filename_list, name_inchikey_map=None):
    """Generate solvent info from read in SSIP files and solvent information.

    Parameters
    ----------
    mole_fraction_dict_list : list of dicts
        List of dictionaries of name: mole fraction pairs per phase/solvent to
        create.
    solvent_filename : str
        Filename for file containing solvent information.
    ssip_filename_list : list of str
        Filenames for SSIP XML files.
    name_inchikey_map : dict
        inchikey: name pairs

    Returns
    -------
    solv_info_list : list of dicts
        List of dictionaries of information to create XML for given solvent
        compositions.

    """
    solvent_molecule_info = read_and_merge_solvent_information(solvent_filename,
                                                               ssip_filename_list,name_inchikey_map)
    return generate_solv_info_list(mole_fraction_dict_list, solvent_molecule_info, name_inchikey_map)

def generate_solv_info_list(mole_fraction_dict_list, solvent_molecule_info, name_inchikey_map):
    """Generate list of solvent information to create phase/solvent XML for.

    Parameters
    ----------
    mole_fraction_dict_list : list of dicts
        List of dictionaries of name: mole fraction pairs per phase/solvent to
        create.
    solvent_molecule_info : dict of dicts
        Dictionary containing information about pure solvents.
    name_inchikey_map : dict
        inchikey: name pairs

    Returns
    -------
    solv_info_list : list of dicts
        List of dictionaries of information to create XML for given solvent
        compositions.

    """
    solv_info_list = []
    for mole_fraction_dict in mole_fraction_dict_list:
        solv_info_list.append(generate_solvent_information(mole_fraction_dict, solvent_molecule_info, name_inchikey_map))
    return solv_info_list
    

def generate_solvent_information(mole_fraction_dict, solvent_molecule_info, name_inchikey_map):
    """Create information dictionary for a solvent.
    
    This contains information required for generating the solvent XML or phase XML.

    Parameters
    ----------
    mole_fraction_dict : dict
        Dictionary of name: mole fraction pairs.
    solvent_molecule_info : dict of dicts
        Dictionary containing information about pure solvents.
    name_inchikey_map : dict
        inchikey: name pairs.

    Returns
    -------
    dict
        Dictionary of information to create XML for given solvent composition.

    """
    mole_fract_dict_inchikey = convert_molfrac_names_to_inchikeys(mole_fraction_dict, name_inchikey_map)
    solvent_name = create_solvent_name(mole_fraction_dict)
    return {"solvent_name":solvent_name,
            "solvent_id":solvent_name,
            "ssip_info_list":append_molefractions(solvent_molecule_info, mole_fract_dict_inchikey)}


def create_solvent_name(mole_fraction_dict):
    """Create a solvent name from mole fraction information.
    
    The following pattern is used for each component: "[solvent][mole fraction]". Solvents are ordered by lexicographic index.

    Parameters
    ----------
    mole_fraction_dict : dict
        dictionary containing solvent name: mole fraction pairs.

    Returns
    -------
    composite_name : str
        solvent name.

    """
    composite_name = ""
    for name in sorted(mole_fraction_dict.keys()):
        composite_name += ("{:s}"+ MOLEFRACTION_FORMAT).format(name, mole_fraction_dict[name])
    return composite_name

def convert_molfrac_names_to_inchikeys(mole_fraction_dict, name_inchikey_map):
    """Convert nmolecule names to inchikeys in mole fraction dictionary.

    Parameters
    ----------
    mole_fraction_dict : dict
        molecule name: mole fraction pairs.
    name_inchikey_map : dict
        Dictionary of molecule name: molecule inchikey pairs.

    Returns
    -------
    dict
        molecule inchikey:mole fraction pairs.

    """
    return {name_inchikey_map[name]: mole_fraction for  name, mole_fraction in mole_fraction_dict.items()}

def append_molefractions(ssip_information_dict, mole_fraction_dict):
    """Create list of SSIP descriptions of molecules to be in phase.
    
    This appends with mole fraction information.

    Parameters
    ----------
    ssip_information_dict : dict of dicts
        DESCRIPTION.
    mole_fraction_dict : dict
        inchikey: mole fraction pairs.

    Returns
    -------
    ssip_information_list : 
        List of SSIP description information.

    """
    ssip_information_list = []
    for inchikey, molefrac in mole_fraction_dict.items():
        if molefrac > 0.0:
            information_dict = copy.deepcopy(ssip_information_dict[inchikey]['ssip_info_list'][0])
            information_dict['mole_fraction'] = molefrac
            ssip_information_list.append(information_dict)
    ssip_information_list = sorted(ssip_information_list, key=lambda ssip_info: ssip_info['inchikey'])
    return ssip_information_list


def calculate_mole_fractions(conc_dict, volume_frac_dict):
    """Calculate the moel fractions of the given solvents.

    Parameters
    ----------
    conc_dict : dict
        inchikey: concentration pair.
    volume_frac_dict : dict
        inchikey: volume fraction pair.

    Returns
    -------
    dict
        Dictionary of inchikey:mole fraction pairs.

    """
    name_list = list(sorted(volume_frac_dict.keys()))
    vol_frac_array = np.array([volume_frac_dict[name] for name in name_list]) 
    conc_array = np.array([conc_dict[name] for name in name_list]) 
    total_concentrations = conc_array.dot(vol_frac_array)
    molefractions = np.divide(
        np.multiply(conc_array, vol_frac_array), total_concentrations
    )
    return {name_list[pos]:molefractions[pos] for pos in range(len(name_list))}
    

def read_and_merge_solvent_information(solvent_filename, ssip_filename_list, name_inchikey_map=None):
    """Read solvent concentrations and merges with SSIP descriptions.

    Parameters
    ----------
    solvent_filename : str
        Filename for file containing 
    ssip_filename_list : list of str
        Solvent files

    Returns
    -------
    dict
        Dictionary containing solvent information including SSIPMolecule element.

    """
    solvent_information = solvconcreader.read_and_merge_solvent_information(
        solvent_filename, ssip_filename_list, name_inchikey_map=name_inchikey_map
    )
    return solvent_information