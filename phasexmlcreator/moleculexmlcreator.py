# -*- coding: utf-8 -*-
#    phasexmlcreator provides utilites to generate XML input for phasetransfer calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlcreator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for generating the phase:Molecule from an SSIPList and concentration
value in M.

@author: mark
"""

import logging
import copy
import re
from lxml import etree
from phasexmlcreator.formattingconstants import MOLEFRACTION_FORMAT

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)

PHASE_NAMESPACE_DICT = {
    "phase": "http://www-hunter.ch.cam.ac.uk/PhaseSchema",
    "ssip": "http://www-hunter.ch.cam.ac.uk/SSIP",
    "cml": "http://www.xml-cml.org/schema",
}
PHASE = "{{{}}}".format(PHASE_NAMESPACE_DICT["phase"])


def generate_phase_molecule(
    inchikey,
    molecule_id,
    ssip_molecule_element,
    concentration_value,
    mole_fraction=None,
):
    """This generates a phase Molecule element.
    """
    phase_molecule_element = create_phase_molecule_element()
    inchikey_pattern = re.compile(r"[A-Z]{14}\-[A-Z]{10}\-[A-Z]")
    if inchikey_pattern.match(inchikey):
        append_inchikey_attribute(phase_molecule_element, inchikey)
    else:
        dummy_inchikey = "A" * 14 + "-" + "A" * 8 + "NZ-N"
        append_inchikey_attribute(phase_molecule_element, dummy_inchikey)
    append_molecule_id(phase_molecule_element, molecule_id)
    if mole_fraction is not None:
        phase_molecule_element.set(
            PHASE + "moleFraction", MOLEFRACTION_FORMAT.format(mole_fraction)
        )
    cml_molecule = extract_cml_molecule(ssip_molecule_element)
    append_to_element(phase_molecule_element, cml_molecule)
    surface_information = extract_surface_information(ssip_molecule_element)
    append_to_element(phase_molecule_element, surface_information)
    ssip_ssips_element = extract_ssips(ssip_molecule_element)
    phase_ssips_element = create_phase_ssips(
        ssip_ssips_element, concentration_value, molecule_id
    )
    append_to_element(phase_molecule_element, phase_ssips_element)
    return phase_molecule_element


def extract_cml_molecule(ssip_molecule_element):
    """This returns the cml molecule
    """
    cml_molecule = ssip_molecule_element.xpath(
        "cml:molecule", namespaces=PHASE_NAMESPACE_DICT
    )[0]
    return copy.deepcopy(cml_molecule)


def extract_surface_information(ssip_molecule_element):
    """This returns the surface information element.
    """
    surface_information = ssip_molecule_element.xpath(
        "ssip:SurfaceInformation", namespaces=PHASE_NAMESPACE_DICT
    )[0]
    return copy.deepcopy(surface_information)


def extract_ssips(ssip_molecule_element):
    """This returns the SSIPs element.
    """
    ssips_element = ssip_molecule_element.xpath(
        "ssip:SSIPs", namespaces=PHASE_NAMESPACE_DICT
    )[0]
    return copy.deepcopy(ssips_element)


def create_phase_ssips(ssip_ssips_element, concentration_value, molecule_id):
    """This creates an SSIPs element in the phase namespace.
    """
    phase_ssips_element = etree.Element(PHASE + "SSIPs", nsmap=PHASE_NAMESPACE_DICT)
    phase_ssips_element.set(PHASE + "units", "MOLAR")
    ssip_element_list = ssip_ssips_element.xpath(
        "ssip:SSIP", namespaces=PHASE_NAMESPACE_DICT
    )
    for ssip_ssip_element in ssip_element_list:
        phase_ssip_element = create_phase_ssip_with_concentration(
            ssip_ssip_element, concentration_value, molecule_id
        )
        append_to_element(phase_ssips_element, phase_ssip_element)
    return phase_ssips_element


def create_phase_ssip_with_concentration(
    ssip_ssip_element, concentration_value, molecule_id
):
    """This creates a phase SSIP and appends a Total Concentration.
    """
    phase_ssip_element = create_phase_ssip(ssip_ssip_element, molecule_id)
    concentration_element = create_total_concentration_element(concentration_value)
    append_to_element(phase_ssip_element, concentration_element)
    return phase_ssip_element


def create_phase_ssip(ssip_ssip_element, molecule_id):
    """This creates a phase:SSIP element based on an ssip:SSIP element.
    It also adds a moleculeID
    """
    phase_ssip_element = etree.Element(PHASE + "SSIP", nsmap=PHASE_NAMESPACE_DICT)
    for items in ssip_ssip_element.items():
        phase_ssip_element.set(items[0], items[1])
    phase_ssip_element.set(PHASE + "moleculeID", molecule_id)
    phase_ssip_element.set(PHASE + "units", "MOLAR")
    return phase_ssip_element


def append_to_element(parent_element, child_element_to_append):
    """This appends the child element to the parent element given.
    """
    parent_element.append(copy.deepcopy(child_element_to_append))


def append_molecule_id(phase_molecule_element, molecule_id):
    """This appends a molecule ID.
    """
    phase_molecule_element.set(PHASE + "moleculeID", molecule_id)


def append_inchikey_attribute(phase_molecule_element, inchikey):
    """This appends the StdInChIKey attribute to the molecule element.
    """
    phase_molecule_element.set(PHASE + "stdInChIKey", inchikey)
    return phase_molecule_element


def create_phase_molecule_element():
    """This creates the Molecule element.
    """
    phase_molecule_element = etree.Element(
        PHASE + "Molecule", nsmap=PHASE_NAMESPACE_DICT
    )
    return phase_molecule_element


def create_total_concentration_element(concentration_value):
    """This creates a concentration element, with the value given being Molar.
    """
    concentration_element = etree.Element(
        PHASE + "TotalConcentration", nsmap=PHASE_NAMESPACE_DICT
    )
    concentration_element.set(PHASE + "units", "MOLAR")
    concentration_element.text = "{:.10f}".format(concentration_value)
    return concentration_element
