# -*- coding: utf-8 -*-
#    phasexmlcreator provides utilites to generate XML input for phasetransfer calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlcreator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for creating solute xml elements.

@author: mark
"""

import logging
import copy
from lxml import etree
import phasexmlcreator.moleculexmlcreator as moleculexmlcreator
import xmlvalidator.xmlvalidation as xmlvalidation

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.DEBUG)

PHASE_NAMESPACE_DICT = {
    "phase": "http://www-hunter.ch.cam.ac.uk/PhaseSchema",
    "ssip": "http://www-hunter.ch.cam.ac.uk/SSIP",
    "cml": "http://www.xml-cml.org/schema",
}
PHASE = "{{{}}}".format(PHASE_NAMESPACE_DICT["phase"])


def write_element_tree_to_file(element_tree, filename):
    """Writes element tree to file.
    """
    xmlvalidation.validate_xml(element_tree, xmlvalidation.PHASE_SCHEMA)
    element_tree.write(
        filename, encoding="UTF-8", xml_declaration=True, pretty_print=True
    )


def create_element_tree(element):
    """Returns the element tree for the element.
    """
    return etree.ElementTree(element)


def generate_solute_list_element(ssip_list_info_dict_list):
    """this creates a solute list element, with the solute elements appended as
    children for all the information given.
    """
    solute_list_element = create_solute_list_element()
    solute_element_list = generate_solute_element_list(ssip_list_info_dict_list)
    append_solute_elements_to_solute_list_element(
        solute_list_element, solute_element_list
    )

    return solute_list_element


def append_solute_elements_to_solute_list_element(
    solute_list_element, solute_element_list
):
    """This appends each solute element in the solute_element_list to the
    solute_list_element.
    """
    solutes_element = etree.Element(PHASE + "Solutes", nsmap=PHASE_NAMESPACE_DICT)
    for solute_element in solute_element_list:
        append_child_element_to_parent_element(solutes_element, solute_element)
    append_child_element_to_parent_element(solute_list_element, solutes_element)


def append_child_element_to_parent_element(parent_element, child_element):
    """This appends the child element to the parent.
    """
    parent_element.append(copy.deepcopy(child_element))


def create_solute_list_element():
    """This creates a solute_list element.
    """
    return etree.Element(PHASE + "SoluteList", nsmap=PHASE_NAMESPACE_DICT)


def generate_solute_element_list(ssip_list_info_dict_list):
    """This creates a list of solute elements
    """
    solute_element_list = []
    for ssip_list_info_dict in ssip_list_info_dict_list:
        solute_element_list.append(generate_solute_from_dictionary(ssip_list_info_dict))
    return solute_element_list


def generate_solute_from_dictionary(ssip_list_info_dict):
    """This creates the solute element from a dictionary containing the
    information.
    """
    return generate_solute_from_ssip_list(
        ssip_list_info_dict["ssip_molecule"],
        ssip_list_info_dict["inchikey"],
        ssip_list_info_dict["concentration_value"],
    )


def generate_solute_from_ssip_list(ssip_list_element, inchikey, concentration_value):
    """This creates a solute element.
    """
    molecule_id = inchikey + "solute"
    molecule_element = moleculexmlcreator.generate_phase_molecule(
        inchikey, molecule_id, ssip_list_element, concentration_value
    )
    solute_element = generate_solute_from_molecule_element(molecule_element)
    solute_element.set(PHASE + "soluteID", molecule_id)
    return solute_element


def generate_solute_from_molecule_element(molecule_element):
    """This creates a solute element using the given molecule element.
    """
    solute_element = create_solute_element()
    append_molecule_element_to_solute(solute_element, molecule_element)
    return solute_element


def append_molecule_element_to_solute(solute_element, molecule_element):
    """This appends the molecule element to the solute.
    """
    solute_element.append(copy.deepcopy(molecule_element))


def create_solute_element():
    """This creates an empty solute element.
    """
    return etree.Element(PHASE + "Solute", nsmap=PHASE_NAMESPACE_DICT)
