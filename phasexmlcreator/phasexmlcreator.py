# -*- coding: utf-8 -*-
#    phasexmlcreator provides utilites to generate XML input for phasetransfer calculations.
#    Copyright (C) 2019  Mark D. Driver
#
#    phasexmlcreator is free software: you can redistribute it and/or modify
#    it under the terms of the GNU Affero General Public License as published by
#    the Free Software Foundation, either version 3 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU Affero General Public License for more details.
#
#    You should have received a copy of the GNU Affero General Public License
#    along with this program.  If not, see <https://www.gnu.org/licenses/>.
"""
Script for producing phase XML from solvent SSIP files. This is to generate
phases with SSIP concentrations not set so concentration calculation is required.

@author: mark
"""

import logging
import copy
from lxml import etree
import phasexmlcreator.moleculexmlcreator as moleculexmlcreator
import xmlvalidator.xmlvalidation as xmlvalidation
from phasexmlcreator.formattingconstants import TEMPERATURE_FORMAT

logging.basicConfig()
LOGGER = logging.getLogger(__name__)
LOGGER.setLevel(logging.INFO)

PHASE_NAMESPACE_DICT = {
    "phase": "http://www-hunter.ch.cam.ac.uk/PhaseSchema",
    "ssip": "http://www-hunter.ch.cam.ac.uk/SSIP",
    "cml": "http://www.xml-cml.org/schema",
}
PHASE = "{{{}}}".format(PHASE_NAMESPACE_DICT["phase"])


def write_element_tree_to_file(element_tree, filename):
    """Writes element tree to file.
    """
    xmlvalidation.validate_xml(element_tree, xmlvalidation.PHASE_SCHEMA)
    element_tree.write(
        filename, encoding="UTF-8", xml_declaration=True, pretty_print=True
    )


def create_element_tree(element):
    """Returns the element tree for the element.
    """
    return etree.ElementTree(element)


def generate_mixture_collection_element_diff_temp(phase_info_by_temp_list):
    """Generate mixture collection Element.
    
    Only 

    Parameters
    ----------
    phase_info_by_temp_list : list
        List containing temperature information and list of correponding phase
        information for that temperature.

    Returns
    -------
    mixture_collection_element : lxml.etree.Element
        MixtureCollection XML element representation.

    """
    mixture_collection_element = create_mixture_collection_element()
    mixture_element_list = []
    for phase_temp_pair in phase_info_by_temp_list:
        phase_info_dict_list = phase_temp_pair[1]
        temperature_info = phase_temp_pair[0]
        mixture_element_list.extend(generate_mixture_element_list_from_information(
        phase_info_dict_list, temperature_info
    ))
    append_mixture_list_to_mixture_collection(
        mixture_collection_element, mixture_element_list
    )
    return mixture_collection_element
def generate_mixture_collection_element(phase_info_dict_list, temperature_info):
    """This generates a mixture collection element.
    """
    mixture_collection_element = create_mixture_collection_element()
    mixture_element_list = generate_mixture_element_list_from_information(
        phase_info_dict_list, temperature_info
    )
    append_mixture_list_to_mixture_collection(
        mixture_collection_element, mixture_element_list
    )
    return mixture_collection_element


def append_mixture_list_to_mixture_collection(
    mixture_collection_element, mixture_element_list
):
    """This appends the mixture elements to the MixtureCollection contained in a Mixtures Element.
    """
    mixtures_element = etree.Element(PHASE + "Mixtures", nsmap=PHASE_NAMESPACE_DICT)
    for mixture_element in mixture_element_list:
        append_child_element_to_parent_element(mixtures_element, mixture_element)
    append_child_element_to_parent_element(mixture_collection_element, mixtures_element)


def generate_mixture_element_list_from_information(
    phase_info_dict_list, temperature_info
):
    """This generates the mixture elements for the phases at the given temperatures.
    """
    mixture_element_list = []
    for temperature_entry in temperature_info:
        mixture_element_list.append(
            generate_mixture_element(
                phase_info_dict_list,
                temperature_entry["temperature_value"],
                temperature_entry["temperature_units"],
            )
        )
    return mixture_element_list


def create_mixture_collection_element():
    """This creates a MixtureCollection element.
    """
    return etree.Element(PHASE + "MixtureCollection", nsmap=PHASE_NAMESPACE_DICT)


def generate_mixture_element(
    phase_info_dict_list, temperature_value, temperature_units
):
    """This generates a Mixture element with contained phases.
    """
    mixture_element = create_mixture_element()
    phase_element_list = generate_phase_element_list_from_information(
        phase_info_dict_list, temperature_value, temperature_units
    )
    LOGGER.info("phase element list length: %d", len(phase_element_list))
    append_phases_to_mixture_element(mixture_element, phase_element_list)
    mixture_element.set(
        PHASE + "soluteID", (TEMPERATURE_FORMAT + "{}").format(temperature_value, temperature_units)
    )
    return mixture_element


def append_phases_to_mixture_element(mixture_element, phase_element_list):
    """This appends the phases as a Phases Element.
    """
    phases_element = etree.Element(PHASE + "Phases", nsmap=PHASE_NAMESPACE_DICT)
    for phase_element in phase_element_list:
        append_child_element_to_parent_element(phases_element, phase_element)
    append_child_element_to_parent_element(mixture_element, phases_element)


def create_mixture_element():
    """This creates a mixture element.
    """
    return etree.Element(PHASE + "Mixture", nsmap=PHASE_NAMESPACE_DICT)


def generate_phase_element_list_from_information(
    phase_info_dict_list, temperature_value, temperature_units
):
    """This creates a list of phase elements, from the information supplied, all at the same temperature.
    """
    phase_element_list = []
    for phase_info_dict in phase_info_dict_list:
        LOGGER.debug("phase name: %s", phase_info_dict["solvent_id"])
        phase_element_list.append(
            generate_phase_element_from_value_dictionary(
                phase_info_dict, temperature_value, temperature_units
            )
        )
    return phase_element_list


def generate_phase_element_from_value_dictionary(
    solvent_info_dict, temperature_value, temperature_units
):
    """This generates a phase entry from the given information.
    """
    return generate_phase_element_from_ssip_lists(
        solvent_info_dict["solvent_id"],
        temperature_value,
        temperature_units,
        solvent_info_dict["ssip_info_list"],
    )


def generate_phase_element_from_ssip_lists(
    solvent_id, temperature_value, temperature_units, ssip_info_list
):
    """This generates the phase information from the given information,
    first creating the molecule elements.
    """
    molecule_element_list = generate_molecule_element_list(ssip_info_list, temperature_value)
    solv_id_value = solvent_id + TEMPERATURE_FORMAT.format(temperature_value) + temperature_units
    return generate_phase_element(
        solv_id_value, temperature_value, temperature_units, molecule_element_list
    )


def generate_phase_element(
    solvent_id, temperature_value, temperature_units, molecule_element_list
):
    """This generates a phase element.
    """
    phase_element = create_phase_element()
    append_solvent_id(phase_element, solvent_id)
    phase_element.set(PHASE + "units", "MOLAR")
    append_molecule_element_list_to_phase(phase_element, molecule_element_list)
    temperature_element = create_temperature_element(
        temperature_value, temperature_units
    )
    append_child_element_to_parent_element(phase_element, temperature_element)
    return phase_element


def generate_molecule_element_list(ssip_info_list, temperature_value):
    """This generates a list of molecule elements from the given information
    about the ssip lists and concentrations.
    """
    molecule_element_list = []
    for ssip_info in ssip_info_list:
        
        molecule_element = None
        if isinstance(ssip_info["concentration_value"], float):
            molecule_element = moleculexmlcreator.generate_phase_molecule(
                    ssip_info["inchikey"],
                    ssip_info["inchikey"],
                    ssip_info["ssip_molecule"],
                    ssip_info["concentration_value"],
                    ssip_info.get("mole_fraction", None),
                    )
        else:
            molecule_element = moleculexmlcreator.generate_phase_molecule(
                    ssip_info["inchikey"],
                    ssip_info["inchikey"],
                    ssip_info["ssip_molecule"],
                    float(ssip_info["concentration_value"]["{:.4f}".format(temperature_value)]),
                    ssip_info.get("mole_fraction", None),
                    )
        molecule_element_list.append(
            molecule_element
        )
    return molecule_element_list


def append_molecule_element_list_to_phase(phase_element, molecule_element_list):
    """This appends the molecule elements in the list to the solvent as a Molecules element.
    """
    molecules_element = etree.Element(PHASE + "Molecules", nsmap=PHASE_NAMESPACE_DICT)
    for molecule_element in molecule_element_list:
        append_child_element_to_parent_element(molecules_element, molecule_element)
    append_child_element_to_parent_element(phase_element, molecules_element)


def append_child_element_to_parent_element(parent_element, child_element):
    """This appends the child element to the parent.
    """
    parent_element.append(copy.deepcopy(child_element))


def append_solvent_id(phase_element, solvent_id):
    """This appends the solvent id to the phase element.
    """
    phase_element.set(PHASE + "solventID", solvent_id)


def create_phase_element():
    """This creates a phase element
    """
    return etree.Element(PHASE + "Phase", nsmap=PHASE_NAMESPACE_DICT)


def create_temperature_element(temperature_value, temperature_units):
    """This creates a phase element
    """
    temperature_element = etree.Element(
        PHASE + "Temperature", nsmap=PHASE_NAMESPACE_DICT
    )
    temperature_element.set(PHASE + "units", temperature_units)
    temperature_element.text = TEMPERATURE_FORMAT.format(temperature_value)
    return temperature_element
