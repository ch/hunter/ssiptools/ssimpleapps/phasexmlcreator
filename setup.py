'''
    phasexmlcreator provides utilites to generate XML input for phasetransfer calculations.
    Copyright (C) 2019  Mark D. Driver

    phasexmlcreator is free software: you can redistribute it and/or modify
    it under the terms of the GNU Affero General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU Affero General Public License for more details.

    You should have received a copy of the GNU Affero General Public License
    along with this program.  If not, see <https://www.gnu.org/licenses/>.
'''

from setuptools import setup

setup(name='phasexmlcreator',
      use_scm_version=True,
      setup_requires=['setuptools_scm'],
      description='python scripts for generating solvent and solute xml from SSIPLists.',
      url='https://gitlab.developers.cam.ac.uk/ch/hunter/ssiptools/ssimpleapps/phasexmlcreator',
      author='Mark Driver',
      author_email='mdd31@cantab.ac.uk',
      license='AGPLv3',
      packages=setuptools.find_packages(),
      package_data={'':['solvent_information/solvent_information.csv', "test/resources/*"]},
      install_requires=['lxml','numpy','xmlvalidator'],
      zip_safe=False)
