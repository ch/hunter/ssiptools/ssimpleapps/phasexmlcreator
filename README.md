# PhaseXMLCreator #

This is for converting the SSIP description of molecules to solvent,
solute or phase XML entries for use with the phasetransfer code of the SSIP code.
This contains a CLI for the creation of simple solute and solvent systems.
For the creation of more complex mixtures please see the documentation.


## How do I get set up? ##

To build and install this module from source please follow the below instructions.

### Required modules ###

The other required modules produced by Mark Driver as part of this work are:
    
- xmlvalidator [bitbucket](https://bitbucket.org/mdd31/xmlvalidator) [CamGitlab](https://gitlab.developers.cam.ac.uk/ch/hunter/ssiptools/utils/xmlvalidator)

### Clone the repository ###

*From bitbucket:*

    git clone https://bitbucket.org/mdd31/phasexmlcreator.git
    

*From University of Cambridge gitlab: (using ssh)*

    git clone git@gitlab.developers.cam.ac.uk:ch/hunter/ssiptools/ssimpleapps/phasexmlcreator.git    

Change to the repository: 

    cd phasexmlcreator

### Setting up the python environment ###

Details of an anaconda environment is provided in the repository with the required dependencies for this package.

	conda env create -f environment.yml

This creates an environment called 'nwchempy' which can be loaded using:

	conda activate nwchempy


### Using pip to install module ###

To install in your environment using pip run:

	pip install .

This installs it in your current python environment.
Test that it has been installed:

    ~$ ipython
    Python 3.7.5 (default, Oct 25 2019, 15:51:11) 
    Type 'copyright', 'credits' or 'license' for more information
    IPython 7.10.2 -- An enhanced Interactive Python. Type '?' for help.

    In [1]: import phasexmlcreator.solvent_information                  

### Expected usage ###

The CLI provides simple access to production of Solute XML and pure Solvent XML.
For mixed solvents and phases, then this requires the use of importing the correct method from the appropriate script.
Please see sphinx documentation for more information.
The expected CLI output is shown below.

    ~$ python -m phasexmlcreator -h
    usage: __main__.py [-h] {solvent,solute} ...

    This CLI allows for simple creation of solute or solvent XML input.

    optional arguments:
      -h, --help        show this help message and exit

    Sub methods:
      There are two CLI options: 1) Solute creation 2) Solvent creation (for
      pure solvents)

      {solvent,solute}
        solvent         used to call solventmaker.
        solute          Used to call solutemaker.


### Documentation ###

Documentation can be rendered using sphinx.

	cd docs
	make html

The rendered HTML can then be found in the build sub folder.

In progress: display of this documentation in a project wiki.

### Contribution guidelines ###

This code has been released under the AGPLv3 license.
If you find any bugs please file an issue ticket.
Submission of pull requests for open issues or improvements are welcomed.

### Who do I talk to? ###

Any queries please contact Mark Driver.
